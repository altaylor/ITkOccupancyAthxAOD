#ifndef ITKOCCUPANCY_ITKOCCUPANCYALG_H
#define ITKOCCUPANCY_ITKOCCUPANCYALG_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"



class ITkOccupancyAlg: public ::AthAnalysisAlgorithm { 
 public: 
  ITkOccupancyAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~ITkOccupancyAlg(); 

  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();
  
  virtual StatusCode beginInputFile();

 private: 

}; 

#endif //> !ITKOCCUPANCY_ITKOCCUPANCYALG_H
