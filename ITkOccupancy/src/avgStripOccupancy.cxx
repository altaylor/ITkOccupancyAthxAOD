// MyPackage includes
#include "avgStripOccupancy.h"

#include <math.h>

// MyPackage includes
#include "xAODJet/JetContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetAuxContainer.h"

#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "xAODTracking/TrackMeasurementValidationAuxContainer.h"

#include "xAODTracking/TrackStateValidationContainer.h"
#include "xAODTracking/TrackStateValidationAuxContainer.h"

//#include "xAODJet/JetAuxContainer.h"

#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackParticleAuxContainer.h"

#include "TVector.h"

#include "GaudiKernel/ITHistSvc.h"
#include "Counters.h"


#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TVector3.h"
#include "TH2D.h"

#include <math.h>




avgStripOccupancy::avgStripOccupancy( const std::string& name, ISvcLocator* pSvcLocator ) : AthAlgorithm( name, pSvcLocator ){

  //declareProperty( "Property", m_nProperty ); //example property declaration

}


avgStripOccupancy::~avgStripOccupancy() {}


StatusCode avgStripOccupancy::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");

  ServiceHandle<ITHistSvc> histSvc("THistSvc",name()); 
  CHECK( histSvc.retrieve() );

  //TGraph *occ_z = new TGraph(occupancy.size(), &(occupancy[1]), &(zPos[1]));
  //TGraph *occ_z = new TGraph();
  //occ_z->SetName("hello_world");    
  //histSvc->regGraph("/MYSTREAM/occ_z",occ_z).ignore();

   myTree = new TTree("myTree","myTree");
  CHECK( histSvc->regTree("/MYSTREAM/myTree",myTree) );

  myHist = new TH1D("myHist","myHist",100,0,100);
  histSvc->regHist("/MYSTREAM/myHist",myHist).ignore();

  // plots for the barrel
  // each number represents a layer in the SCT barrel 

  occ_z_0 = new TGraph();
  occ_z_0->SetName("occ_z_0");

  hit_dens_z_0 = new TGraph();
  hit_dens_z_0->SetName("hit_dens_z_0");

  
  clus_size_z_0 = new TGraph();
  clus_size_z_0->SetName("clus_size_z_0");

  occ_z_1 = new TGraph();
  occ_z_1->SetName("occ_z_1");
  
  hit_dens_z_1 = new TGraph();
  hit_dens_z_1->SetName("hit_dens_z_1");

  
  clus_size_z_1 = new TGraph();
  clus_size_z_1->SetName("clus_size_z_1");
 
    
  occ_z_2 = new TGraph();
  occ_z_2->SetName("occ_z_2");

  hit_dens_z_2 = new TGraph();
  hit_dens_z_2->SetName("hit_dens_z_2");

  
  clus_size_z_2 = new TGraph();
  clus_size_z_2->SetName("clus_size_z_2");
  
  
  occ_z_3 = new TGraph();
  occ_z_3->SetName("occ_z_3");

  hit_dens_z_3 = new TGraph();
  hit_dens_z_3->SetName("hit_dens_z_3");

  
  clus_size_z_3 = new TGraph();
  clus_size_z_3->SetName("clus_size_z_3");



  histSvc->regGraph("/MYSTREAM/occ_z_0",occ_z_0).ignore();
  histSvc->regGraph("/MYSTREAM/occ_z_1",occ_z_1).ignore();
  histSvc->regGraph("/MYSTREAM/occ_z_2",occ_z_2).ignore();
  histSvc->regGraph("/MYSTREAM/occ_z_3",occ_z_3).ignore();
  
  histSvc->regGraph("/MYSTREAM/hit_dens_z_0",hit_dens_z_0).ignore();
  histSvc->regGraph("/MYSTREAM/hit_dens_z_1",hit_dens_z_1).ignore();
  histSvc->regGraph("/MYSTREAM/hit_dens_z_2",hit_dens_z_2).ignore();
  histSvc->regGraph("/MYSTREAM/hit_dens_z_3",hit_dens_z_3).ignore();

  
  histSvc->regGraph("/MYSTREAM/clus_size_z_0",clus_size_z_0).ignore();
  histSvc->regGraph("/MYSTREAM/clus_size_z_1",clus_size_z_1).ignore();
  histSvc->regGraph("/MYSTREAM/clus_size_z_2",clus_size_z_2).ignore();
  histSvc->regGraph("/MYSTREAM/clus_size_z_3",clus_size_z_3).ignore();


   // plots for the end cap
  // each number is for a disk in the endcap

  occ_r_0 = new TGraph();
  occ_r_0->SetName("occ_r_0");

  hit_dens_r_0 = new TGraph();
  hit_dens_r_0->SetName("hit_dens_r_0");

  
  clus_size_r_0 = new TGraph();
  clus_size_r_0->SetName("clus_size_r_0");


  occ_r_1 = new TGraph();
  occ_r_1->SetName("occ_r_1");
  
  hit_dens_r_1 = new TGraph();
  hit_dens_r_1->SetName("hit_dens_r_1");

  
  clus_size_r_1 = new TGraph();
  clus_size_r_1->SetName("clus_size_r_1");
    

    
  occ_r_2 = new TGraph();
  occ_r_2->SetName("occ_r_2");

  hit_dens_r_2 = new TGraph();
  hit_dens_r_2->SetName("hit_dens_r_2");

  
  clus_size_r_2 = new TGraph();
  clus_size_r_2->SetName("clus_size_r_2");
  
  
  occ_r_3 = new TGraph();
  occ_r_3->SetName("occ_r_3");

  hit_dens_r_3 = new TGraph();
  hit_dens_r_3->SetName("hit_dens_r_3");

  
  clus_size_r_3 = new TGraph();
  clus_size_r_3->SetName("clus_size_r_3");
    
  occ_r_4 = new TGraph();
  occ_r_4->SetName("occ_r_4");

  hit_dens_r_4 = new TGraph();
  hit_dens_r_4->SetName("hit_dens_r_4");

  
  clus_size_r_4 = new TGraph();
  clus_size_r_4->SetName("clus_size_r_4");
      
  occ_r_5 = new TGraph();
  occ_r_5->SetName("occ_r_5");

  hit_dens_r_5 = new TGraph();
  hit_dens_r_5->SetName("hit_dens_r_5");

  
  clus_size_r_5 = new TGraph();
  clus_size_r_5->SetName("clus_size_r_5");

  //myHist = new TH1D("myHist","myHist",10,0,10);
  //anotherHist = new TH1D("anotherHist","anotherHist",10,0,10);
  //histSvc->regHist("/MYSTREAM/myHist",myHist).ignore(); //or check the statuscode

  // barrel histograms..

  
  sct_barrel_occ_map2D = new TH2D("sct_barrel_occ_map2D","occupancies in percent (200 pileup)",(240/4),-3100,3100,(160/4),0.,1100.);
  sct_barrel_occ_map2D_norm = new TH2D("sct_barrel_occ_map2D_norm","occupancies in percent (200 pileup)",(240/4),-3100.,3100.,(160/4),0.,1100.);
  sct_barrel_hitmap2D = new TH2D("sct_barrel_hitmap2D","Hits / mm^{2} ",(240/4),-3100,3100,(160/4),0.,1100.);
  sct_barrel_hitmap2D_norm = new TH2D("sct_barrel_hitmap2D_norm"," Hits / mm^{2} ",(240/4),-3100.,3100.,(160/4),0.,1100.);
  
  sct_barrel_occ_map2DV2 = new TH2D("sct_barrel_occ_map2DV2","occupancies in percent (200 pileup)",(240/2),-3100,3100,(160/2),0.,1100.);
  sct_barrel_occ_map2DV2_norm = new TH2D("sct_barrel_occ_map2DV2_norm","occupancies in percent (200 pileup)",(240/2),-3100.,3100.,(160/2),0.,1100.);
  sct_barrel_hitmap2DV2 = new TH2D("sct_barrel_hitmap2DV2","Hits / mm^{2} ",(240/2),-3100,3100,(160/2),0.,1100.);
  sct_barrel_hitmap2DV2_norm = new TH2D("sct_barrel_hitmap2DV2_norm"," Hits / mm^{2} ",(240/2),-3100.,3100.,(160/2),0.,1100.);

  // disk histograms..

  sct_disk_occ_map2D = new TH2D("sct_disk_occ_map2D","occupancies in percent (200 pileup)",(240/4),-3100,3100,(160/4),0.,1100.);
  sct_disk_occ_map2D_norm = new TH2D("sct_disk_occ_map2D_norm","occupancies in percent (200 pileup)",(240/4),-3100.,3100.,(160/4),0.,1100.);
  sct_disk_hitmap2D = new TH2D("sct_disk_hitmap2D","Hits / mm^{2} ",(240/4),-3100,3100,(160/4),0.,1100.);
  sct_disk_hitmap2D_norm = new TH2D("sct_disk_hitmap2D_norm"," Hits / mm^{2} ",(240/4),-3100.,3100.,(160/4),0.,1100.);
  
  sct_disk_occ_map2DV2 = new TH2D("sct_disk_occ_map2DV2","occupancies in percent (200 pileup)",(240/2),-3100,3100,(160/2),0.,1100.);
  sct_disk_occ_map2DV2_norm = new TH2D("sct_disk_occ_map2DV2_norm","occupancies in percent (200 pileup)",(240/2),-3100.,3100.,(160/2),0.,1100.);
  sct_disk_hitmap2DV2 = new TH2D("sct_disk_hitmap2DV2","Hits / mm^{2} ",(240/2),-3100,3100,(160/2),0.,1100.);
  sct_disk_hitmap2DV2_norm = new TH2D("sct_disk_hitmap2DV2_norm"," Hits / mm^{2} ",(240/2),-3100.,3100.,(160/2),0.,1100.);

        

  histSvc->regGraph("/MYSTREAM/occ_r_0",occ_r_0).ignore();
  histSvc->regGraph("/MYSTREAM/occ_r_1",occ_r_1).ignore();
  histSvc->regGraph("/MYSTREAM/occ_r_2",occ_r_2).ignore();
  histSvc->regGraph("/MYSTREAM/occ_r_3",occ_r_3).ignore();
  histSvc->regGraph("/MYSTREAM/occ_r_4",occ_r_4).ignore();
  histSvc->regGraph("/MYSTREAM/occ_r_5",occ_r_5).ignore();
  
  histSvc->regGraph("/MYSTREAM/hit_dens_r_0",hit_dens_r_0).ignore();
  histSvc->regGraph("/MYSTREAM/hit_dens_r_1",hit_dens_r_1).ignore();
  histSvc->regGraph("/MYSTREAM/hit_dens_r_2",hit_dens_r_2).ignore();
  histSvc->regGraph("/MYSTREAM/hit_dens_r_3",hit_dens_r_3).ignore();
  histSvc->regGraph("/MYSTREAM/hit_dens_r_4",hit_dens_r_4).ignore();
  histSvc->regGraph("/MYSTREAM/hit_dens_r_5",hit_dens_r_5).ignore();

  histSvc->regGraph("/MYSTREAM/clus_size_r_0",clus_size_r_0).ignore();
  histSvc->regGraph("/MYSTREAM/clus_size_r_1",clus_size_r_1).ignore();
  histSvc->regGraph("/MYSTREAM/clus_size_r_2",clus_size_r_2).ignore();
  histSvc->regGraph("/MYSTREAM/clus_size_r_3",clus_size_r_3).ignore();
  histSvc->regGraph("/MYSTREAM/clus_size_r_4",clus_size_r_4).ignore();
  histSvc->regGraph("/MYSTREAM/clus_size_r_5",clus_size_r_5).ignore();

  

  // barrel 
  histSvc->regHist("/MYSTREAM/sct_barrel_occ_map2D",sct_barrel_occ_map2D).ignore();
  histSvc->regHist("/MYSTREAM/sct_barrel_occ_map2D_norm",sct_barrel_occ_map2D_norm).ignore();
  histSvc->regHist("/MYSTREAM/sct_barrel_hitmap2D",sct_barrel_hitmap2D).ignore();
  histSvc->regHist("/MYSTREAM/sct_barrel_hitmap2D_norm",sct_barrel_hitmap2D_norm).ignore();
    
  histSvc->regHist("/MYSTREAM/sct_barrel_occ_map2DV2",sct_barrel_occ_map2DV2).ignore();
  histSvc->regHist("/MYSTREAM/sct_barrel_occ_map2DV2_norm",sct_barrel_occ_map2DV2_norm).ignore();
  histSvc->regHist("/MYSTREAM/sct_barrel_hitmap2DV2",sct_barrel_hitmap2DV2).ignore();
  histSvc->regHist("/MYSTREAM/sct_barrel_hitmap2DV2_norm",sct_barrel_hitmap2DV2_norm).ignore();
  
  // disk registration
  histSvc->regHist("/MYSTREAM/sct_disk_occ_map2D",sct_disk_occ_map2D).ignore();
  histSvc->regHist("/MYSTREAM/sct_disk_occ_map2D_norm",sct_disk_occ_map2D_norm).ignore();
  histSvc->regHist("/MYSTREAM/sct_disk_hitmap2D",sct_disk_hitmap2D).ignore();
  histSvc->regHist("/MYSTREAM/sct_disk_hitmap2D_norm",sct_disk_hitmap2D_norm).ignore();

    
  histSvc->regHist("/MYSTREAM/sct_disk_occ_map2DV2",sct_disk_occ_map2DV2).ignore();
  histSvc->regHist("/MYSTREAM/sct_disk_occ_map2DV2_norm",sct_disk_occ_map2DV2_norm).ignore();
  histSvc->regHist("/MYSTREAM/sct_disk_hitmap2DV2",sct_disk_hitmap2DV2).ignore();
  histSvc->regHist("/MYSTREAM/sct_disk_hitmap2DV2_norm",sct_disk_hitmap2DV2_norm).ignore();


  //histSvc->regHist("/MYSTREAM/myHist",myHist).ignore();
  /*
  
    sct_disk_map2D = TH2D("sct_disk_occ_map2d","occupancies in percent (200 pileup)",(240/4),-3100,3100,(160/4),0.,1100.)
    sct_disk_hitmap2D = TH2D("sct_disk_hit_map2d","Hits / mm^{2} ",(240/4),-3100,3100,(160/4),0.,1100.)
    sct_disk_map2D_norm = TH2D("sct_disk_occ_map2d_norm","occupancies in percent (200 pileup)",(240/4),-3100.,3100.,(160/4),0.,1100.)
    sct_disk_hitmap2D_norm = TH2D("sct_disk_hit_map2d_norm"," Hits / mm^{2} ",(240/4),-3100.,3100.,(160/4),0.,1100.)
        
    self.hsvc.regHist("/MYSTREAM/sct_disk_occ_map2d",sct_disk_occ_map2d)
    self.hsvc.regHist("/MYSTREAM/sct_disk_hit_map2d",sct_disk_hit_map2d)
    self.hsvc.regHist("/MYSTREAM/sct_disk_occ_map2d_norm",sct_disk_occ_map2d_norm)
    self.hsvc.regHist("/MYSTREAM/sct_disk_hit_map2d_norm",sct_disk_hit_map2d_norm)




    sct_disk_map2DV2 = TH2D("sct_disk_occ_map2dV2","occupancies in percent (200 pileup)",(240/2),-3100,3100,(160/2),0.,1100.)
    sct_disk_hitmap2DV2 = TH2D("sct_disk_hit_map2dV2","Hits / mm^{2} ",(240/2),-3100,3100,(160/2),0.,1100.)
    sct_disk_map2D_normV2 = TH2D("sct_disk_occ_map2d_normV2","occupancies in percent (200 pileup)",(240/2),-3100.,3100.,(160/2),0.,1100.)
    sct_disk_hitmap2D_normV2 = TH2D("sct_disk_hit_map2d_normV2"," Hits / mm^{2} ",(240/2),-3100.,3100.,(160/2),0.,1100.)

    self.hsvc.regHist("/MYSTREAM/sct_disk_occ_map2dV2",sct_disk_occ_map2dV2)
    self.hsvc.regHist("/MYSTREAM/sct_disk_hit_map2dV2",sct_disk_hit_map2dV2)
    self.hsvc.regHist("/MYSTREAM/sct_disk_occ_map2d_normV2",sct_disk_occ_map2d_normV2)
    self.hsvc.regHist("/MYSTREAM/sct_disk_hit_map2d_normV2",sct_disk_hit_map2d_normV2)

  */




  
  

  // there are 4 layers in the strip barrel
  int nLayers = 4; 
  // nmodule is the number of modules along z in the x aod, it is eta_module(min) -> eta_module(max)
  // modules with zero occupancy are thrown away at the end.  
  
  int nmodule = 60; 
  int nmax = nmodule;
  int nmin = -nmodule;

  // number of staves as we go along the barrel

  std::vector<float> nStaves {28.0,40.0,56.0,72.0};

  // in layers 0 + 1, the number of strips is 5120. 
  // in layers 2 + 3, the nuber of strips is 2560
  float nShortStrips = 5120;
  float nLongStrips = 2560; 
  float nArea = 9316.096;

  // for the barrel 
  float nStrips = 0; 
  float nReadOut = 0;

  // we set up a map for the strip detector.. 

  //std::map<std::tuple<int,int,int>, Counters>::iterator it_type;  -- not needed!

  // loop over layers and etaModuleIDs
   for (int i=0; i<nLayers; ++i) {
    for (int j=nmin; j<nmax; ++j) {

      if ( i == 0 || i == 1 ) { nStrips = nShortStrips; }
      if ( i == 2 || i == 3 ) { nStrips = nLongStrips; }

      // we have an extra index for the barrel / end cap
      int BEC = 0; 
      Counters c = Counters();
      // number of read out channels averaged over phi is, nStrips * nStaves * 2 (double sided sensors)
      c.setReadOut(nStrips*nStaves[i]*2.0);
      // area is calculated the same as above
      c.setArea(nArea*nStaves[i]*2.0);
      // linker is a counter object (takes layer, etaID and barrel / end cap as constructor ) 
      std::tuple<int,int,int> linker (i,j,BEC);

      // strip barrel is a map declared in the header
      stripBarrel[linker] = c;


    }
   }

   // beginning of declaration for the end cap strips

   // for the first 3 rings there are 32 sectors, thereafter there is 64!

    float strips0 = 1024+1024+1152+1152;
    float strips1 = 1280 +  1280 + 1408 + 1408;
    float strips2 = 1536 + 1536;
    float strips3 = 896 + 896 + 896 +  896;
    float strips4 = 1024+1024;
    float strips5 = 1152 + 1152;
         
    float rInner0 = 384.5;
    float rInner1 = 489.823;
    float rInner2 = 575.594;
    float rInner3 = 638.609;
    float rInner4 = 755.501;
    float rInner5 = 867.462;
             
    float rOuter0 = 488.423;
    float rOuter1 = 574.194;
    float rOuter2 = 637.209;
    float rOuter3 = 755.501;
    float rOuter4 = 866.062;
    float rOuter5 = 967.785;

    // there is a bug in the step 1.5 samples which means the position of the sct clusters in broken in the end cap.
    // for now we need to fix it but these lines should hopefully be able to be removed in future!
    
    float fixR0 = 438.614;
    float fixR1 = 534.639;
    float fixR2 = 609.405;
    float fixR3 = 697.899;
    float fixR4 = 812.471;
    float fixR5 = 918.749;

     
    float fixZ0 = 1512.0;
    float fixZ1 = 1702.0;
    float fixZ2 = 1952.0;
    float fixZ3 = 2252.0;
    float fixZ4 = 2602.0;
    float fixZ5 = 3000.0;
    

    std::vector<float> endcapStrips {strips0,strips1,strips2,strips3,strips4,strips5};
    std::vector<float> innerRadii {rInner0,rInner1,rInner2,rInner3,rInner4,rInner5};
    std::vector<float> outerRadii {rOuter0,rOuter1,rOuter2,rOuter3,rOuter4,rOuter5};

    std::vector<float> fixR {fixR0,fixR1,fixR2,fixR3,fixR4,fixR5};

    // we also fix the Z position of each disk 
    std::vector<float> fixZ {fixZ0,fixZ1,fixZ2,fixZ3,fixZ4,fixZ5};

    float nSectorsEC = 0.0;
    int nDisks = 6;
    int nRings = 6;

    float nReadOutEC = 0;
    float nAreaEC = 0;
    // factor of 2 for double sided sct
    float doubleSided = 2.0;

    // first loop is for BEC
    // second loop is for disks
    // third loop is for rings

    for (int k=0; k < 2; ++k ) {
      for (int i=0; i< nDisks; ++i) {
	for (int j=0; j< nRings; ++j) {

	  // use the indices to change values
	  // first 3 rings have 32 sectors, 64 thereafter
	  nSectorsEC = 32;
	  if ( j > 2 ) { nSectorsEC = 64; }
	  
	  nReadOutEC = endcapStrips[j]*doubleSided*nSectorsEC;
	  nAreaEC = M_PI*(outerRadii[j]*outerRadii[j] - innerRadii[j]*innerRadii[j])*doubleSided;

	  Counters c = Counters();
	  // number of read out channels averaged over phi is, nStrips * nStaves * 2 (double sided sensors)
	  c.setReadOut(nReadOutEC);
	  c.setArea(nAreaEC);

	  // need to fix R pos rather than use the cluster positions
	  // loop over j is for rings
	  c.fixR(fixR[j]);
	  // loop over i is for disks
	  c.fixZ(fixZ[i]); 
	  
	  // linker is a counter object (takes layer, etaID and barrel / end cap as constructor ) 
	  std::tuple<int,int,int> linker (i,j,k);

	  // strip endcap is a map declared in the header
	  stripEndCap[linker] = c;



	}
      }
    }



   /*
     code for mapping over full map, used later!

   for( it_type = stripBarrel.begin(); it_type != stripBarrel.end(); it_type++) {
     // get layer
     std::cout << " layer? " << std::get<0>(it_type->first) << std::endl; 
     // get eta module ID
     std::cout << " eta module id? " <<  std::get<1>(it_type->first) << std::endl;
     // get barrel or end cap
     std::cout << " barrel or end cap? " << std::get<2>(it_type->first) << std::endl;
   }

   */

  return StatusCode::SUCCESS;
}

/*StatusCode avgStripOccupancy::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");

  return StatusCode::SUCCESS;
}

*/

StatusCode avgStripOccupancy::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");

  //----------------------------
  // Event information
  //--------------------------- 
  const xAOD::EventInfo* eventInfo = 0; //NOTE: Everything that comes from the storegate direct from the input files is const!

  // ask the event store to retrieve the xAOD EventInfo container
  //CHECK( evtStore()->retrieve( eventInfo, "EventInfo") );  // the second argument ("EventInfo") is the key name
  CHECK( evtStore()->retrieve( eventInfo) );
  // if there is only one container of that type in the xAOD (as with the EventInfo container), you do not need to pass
  // the key name, the default will be taken as the only key name in the xAOD 

  // check if data or MC
  bool isMC = true;
  if(!eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION ) ){
    isMC = false;
  }
   // SCT Occupancy Studies

  // number of events used in analysis is required for averaging...
  nEvents += 1.0;

  std::cout << " event number! " << nEvents << std::endl; 

   
    const xAOD::TrackMeasurementValidationContainer* SCTClusters = 0;
    CHECK( evtStore()->retrieve(SCTClusters, "SCT_Clusters") );

    ///

    // Occupancy Studies for the SCT Barrel!

    // this index is used for the end cap
    int set_k = 0; 

   for ( const auto* sct : *SCTClusters) {
     
	float clus_size = sct->rdoIdentifierList().size();
	int layer = sct->auxdata<int>("layer");
        int etaModule = sct->auxdata<int>("eta_module");
	int BEC = sct->auxdata<int>("bec");
	
	float x = sct->globalX();
	float y = sct->globalY();
	float z = sct->globalZ();
	float r = sqrt(x*x + y*y);

	std::tuple<int,int,int> linker (layer,etaModule,BEC);

      // check we are in the barrel
      if ( sct->auxdata<int>("bec") == 0 ) {

	std::tuple<int,int,int> linker (layer,etaModule,BEC);
	stripBarrel[linker].add(x,y,z,clus_size);
	//stripBarrel[linker].sumclussize(clus_size); 

      }

      if ( sct->auxdata<int>("bec") != 0 ) {

	if ( sct->auxdata<int>("bec") == 2 ) {
	  /* set_k = 0;
	}

	else { set_k = 1; } */

	std::tuple<int,int,int> linker (layer,etaModule,0);
	stripEndCap[linker].add(x,y,z,clus_size);

	}
      }
   }

 
   
  return StatusCode::SUCCESS;
}



StatusCode avgStripOccupancy::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");

  std::cout << " begin printing occupancies to screen..... " << std::endl;


  std::cout << " how many events? " << nEvents << std::endl; 

  int nLayers = 4; 
  int nmodule = 60; 
  int nmax = nmodule;
  int nmin = -nmodule;
  int BEC = 0;
  
  //TMultiGraph *mg_occ_z = new TMultiGraph("mg_occ_z","mg_occ_z");

  //vector<vector<float>> MyMatrix[nLayers][4];
  //TGraph *gr = new TGraph();

  for (int i=0; i<nLayers; ++i) {

    //create a vector to hold occupancy + hit density..
    ///std::vector<float> 
    std::vector<float> occupancy;
    std::vector<float> hit_density; 
    std::vector<float> zPos;
    //TGraph *occ_z = new TGraph();
    //TGraph *hit_density_z = new TGraph();
    
    // loop over eta modules
    for (int j=nmin; j<nmax; ++j) {
      // linker is a tuple which we use to look up the strip detector..
      std::tuple<int,int,int> linker (i,j,BEC);
      stripBarrel[linker].setEvents(nEvents);

      // only add to vectors if occupancy is non zero (this is a result of overcounting initially..)

      if ( stripBarrel[linker].occupancy()*100.0 != 0.0 ) {

	//float getR = sqrt( stripBarrel[linker].getX()*stripBarrel[linker].getX() + stripBarrel[linker].getYa)*stripBarrel[linker].getY() );
	//std::cout << " get R? " << getR << std::endl;

	std::cout << "get Z, getR, get Occupancy? " << stripBarrel[linker].getZ() << "   "  << stripBarrel[linker].getR() << "  " <<  stripBarrel[linker].occupancy()*100.0 << std::endl; 

	sct_barrel_occ_map2D->Fill(stripBarrel[linker].getZ(),stripBarrel[linker].getR(),stripBarrel[linker].occupancy()*100.0);
	sct_barrel_occ_map2D_norm->Fill(stripBarrel[linker].getZ(),stripBarrel[linker].getR(),1);
	sct_barrel_hitmap2D->Fill(stripBarrel[linker].getZ(),stripBarrel[linker].getR(),stripBarrel[linker].fluency());
	sct_barrel_hitmap2D_norm->Fill(stripBarrel[linker].getZ(),stripBarrel[linker].getR(),1);
	    
	sct_barrel_occ_map2DV2->Fill(stripBarrel[linker].getZ(),stripBarrel[linker].getR(),stripBarrel[linker].occupancy()*100.0);
	sct_barrel_occ_map2DV2_norm->Fill(stripBarrel[linker].getZ(),stripBarrel[linker].getR(),1);
	sct_barrel_hitmap2DV2->Fill(stripBarrel[linker].getZ(),stripBarrel[linker].getR(),stripBarrel[linker].fluency());
	sct_barrel_hitmap2DV2_norm->Fill(stripBarrel[linker].getZ(),stripBarrel[linker].getR(),1);
	
	//occupancy.push_back(stripBarrel[linker].occupancy()*100.0*100.0);
	//zPos.push_back(stripBarrel[linker].getZ());
        // fill the plots for the different layers

       // fill tgraphs for the different layers!

      if ( i == 0 ) {

	occ_z_0->SetPoint(occ_z_0->GetN(),stripBarrel[linker].getZ(), stripBarrel[linker].occupancy()*100.0);
	hit_dens_z_0->SetPoint(hit_dens_z_0->GetN(),stripBarrel[linker].getZ(), stripBarrel[linker].fluency());

	clus_size_z_0->SetPoint(clus_size_z_0->GetN(),stripBarrel[linker].getZ(), stripBarrel[linker].averageClusterSize());

	
      }

      
      if ( i == 1 ) {
	occ_z_1->SetPoint(occ_z_1->GetN(),stripBarrel[linker].getZ(), stripBarrel[linker].occupancy()*100.0);
	hit_dens_z_1->SetPoint(hit_dens_z_1->GetN(),stripBarrel[linker].getZ(), stripBarrel[linker].fluency());

	clus_size_z_1->SetPoint(clus_size_z_1->GetN(),stripBarrel[linker].getZ(), stripBarrel[linker].averageClusterSize());
      }

      
      if ( i == 2 ) {
	occ_z_2->SetPoint(occ_z_2->GetN(),stripBarrel[linker].getZ(), stripBarrel[linker].occupancy()*100.0);
	hit_dens_z_2->SetPoint(hit_dens_z_2->GetN(),stripBarrel[linker].getZ(), stripBarrel[linker].fluency());

	clus_size_z_2->SetPoint(clus_size_z_2->GetN(),stripBarrel[linker].getZ(), stripBarrel[linker].averageClusterSize());
      }

      
      if ( i == 3 ) {
	occ_z_3->SetPoint(occ_z_3->GetN(),stripBarrel[linker].getZ(), stripBarrel[linker].occupancy()*100.0);
	hit_dens_z_3->SetPoint(hit_dens_z_3->GetN(),stripBarrel[linker].getZ(), stripBarrel[linker].fluency());

	clus_size_z_3->SetPoint(clus_size_z_3->GetN(),stripBarrel[linker].getZ(), stripBarrel[linker].averageClusterSize());
      }


      

      }
    }
  } // end of for loop over the barrel



  // --------------------
  // END OF SCT BARREL STUDIES
  // --------------------

  // end cap
    int nDisks = 6;
    int nRings = 6;
    
    // first loop is for BEC
    // second loop is for disks
    // third loop is for rings

    for (int k=0; k < 2; ++k ) {
      for (int i=0; i< nDisks; ++i) {
	for (int j=0; j< nRings; ++j) {
	  
	  // linker is a counter object (takes layer, etaID and barrel / end cap as constructor ) 
	  std::tuple<int,int,int> linker (i,j,k);

	  stripEndCap[linker].setEvents(nEvents);
	  // only add to vectors if occupancy is non zero (this is a result of overcounting initially..)
	  if ( stripEndCap[linker].occupancy() != 0.0 ) {

	    
	    sct_disk_occ_map2D->Fill(stripEndCap[linker].getfixedZ(),stripEndCap[linker].getfixedR(),stripEndCap[linker].occupancy()*100.0);
	    sct_disk_occ_map2D_norm->Fill(stripEndCap[linker].getfixedZ(),stripEndCap[linker].getfixedR(),1);
	    sct_disk_hitmap2D->Fill(stripEndCap[linker].getfixedZ(),stripEndCap[linker].getfixedR(),stripEndCap[linker].fluency());
	    sct_disk_hitmap2D_norm->Fill(stripEndCap[linker].getfixedZ(),stripEndCap[linker].getfixedR(),1);
	    
	    sct_disk_occ_map2DV2->Fill(stripEndCap[linker].getfixedZ(),stripEndCap[linker].getfixedR(),stripEndCap[linker].occupancy()*100.0);
	    sct_disk_occ_map2DV2_norm->Fill(stripEndCap[linker].getfixedZ(),stripEndCap[linker].getfixedR(),1);
	    sct_disk_hitmap2DV2->Fill(stripEndCap[linker].getfixedZ(),stripEndCap[linker].getfixedR(),stripEndCap[linker].fluency());
	    sct_disk_hitmap2DV2_norm->Fill(stripEndCap[linker].getfixedZ(),stripEndCap[linker].getfixedR(),1);

	
	    // only fill tgraphs for positive R, eg k = 0

	    if ( k == 0 ) {
	    
	      if ( i == 0 ) {
		occ_r_0->SetPoint(occ_r_0->GetN(),stripEndCap[linker].getfixedR(), stripEndCap[linker].occupancy()*100.0);
		hit_dens_r_0->SetPoint(hit_dens_r_0->GetN(),stripEndCap[linker].getfixedR(), stripEndCap[linker].fluency());
		clus_size_r_0->SetPoint(clus_size_r_0->GetN(),stripEndCap[linker].getfixedR(), stripEndCap[linker].averageClusterSize());
	      }

      
	      if ( i == 1 ) {
		occ_r_1->SetPoint(occ_r_1->GetN(),stripEndCap[linker].getfixedR(), stripEndCap[linker].occupancy()*100.0);
		hit_dens_r_1->SetPoint(hit_dens_r_1->GetN(),stripEndCap[linker].getfixedR(), stripEndCap[linker].fluency());
		clus_size_r_1->SetPoint(clus_size_r_1->GetN(),stripEndCap[linker].getfixedR(), stripEndCap[linker].averageClusterSize());
	      }

      
	      if ( i == 2 ) {
		occ_r_2->SetPoint(occ_r_2->GetN(),stripEndCap[linker].getfixedR(), stripEndCap[linker].occupancy()*100.0);
		hit_dens_r_2->SetPoint(hit_dens_r_2->GetN(),stripEndCap[linker].getfixedR(), stripEndCap[linker].fluency());
		clus_size_r_2->SetPoint(clus_size_r_2->GetN(),stripEndCap[linker].getfixedR(), stripEndCap[linker].averageClusterSize());
	      }

      
	      if ( i == 3 ) {
		occ_r_3->SetPoint(occ_r_3->GetN(),stripEndCap[linker].getfixedR(), stripEndCap[linker].occupancy()*100.0);
		hit_dens_r_3->SetPoint(hit_dens_r_3->GetN(),stripEndCap[linker].getfixedR(), stripEndCap[linker].fluency());
		clus_size_r_3->SetPoint(clus_size_r_3->GetN(),stripEndCap[linker].getfixedR(), stripEndCap[linker].averageClusterSize());
	      }

	  
      
	      if ( i == 4 ) {
		occ_r_4->SetPoint(occ_r_4->GetN(),stripEndCap[linker].getfixedR(), stripEndCap[linker].occupancy()*100.0);
		hit_dens_r_4->SetPoint(hit_dens_r_4->GetN(),stripEndCap[linker].getfixedR(), stripEndCap[linker].fluency());
		clus_size_r_4->SetPoint(clus_size_r_4->GetN(),stripEndCap[linker].getfixedR(), stripEndCap[linker].averageClusterSize());
	      }

      
	      if ( i == 5 ) {
		occ_r_5->SetPoint(occ_r_5->GetN(),stripEndCap[linker].getfixedR(), stripEndCap[linker].occupancy()*100.0);
		hit_dens_r_5->SetPoint(hit_dens_r_5->GetN(),stripEndCap[linker].getfixedR(), stripEndCap[linker].fluency());
		clus_size_r_5->SetPoint(clus_size_r_5->GetN(),stripEndCap[linker].getfixedR(), stripEndCap[linker].averageClusterSize());
	      }

	    }







	  
	  }



	}
      }
    } // ends for loop over the endcap

  
  /* for( it_type = stripDetector.begin(); it_type != stripDetector.end(); it_type++) {


     } */

  return StatusCode::SUCCESS;
}
  



