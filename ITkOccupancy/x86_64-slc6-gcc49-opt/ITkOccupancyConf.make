#-- start of make_header -----------------

#====================================
#  Document ITkOccupancyConf
#
#   Generated Mon Apr 10 13:50:07 2017  by altaylor
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_ITkOccupancyConf_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_ITkOccupancyConf_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_ITkOccupancyConf

ITkOccupancy_tag = $(tag)

#cmt_local_tagfile_ITkOccupancyConf = $(ITkOccupancy_tag)_ITkOccupancyConf.make
cmt_local_tagfile_ITkOccupancyConf = $(bin)$(ITkOccupancy_tag)_ITkOccupancyConf.make

else

tags      = $(tag),$(CMTEXTRATAGS)

ITkOccupancy_tag = $(tag)

#cmt_local_tagfile_ITkOccupancyConf = $(ITkOccupancy_tag).make
cmt_local_tagfile_ITkOccupancyConf = $(bin)$(ITkOccupancy_tag).make

endif

include $(cmt_local_tagfile_ITkOccupancyConf)
#-include $(cmt_local_tagfile_ITkOccupancyConf)

ifdef cmt_ITkOccupancyConf_has_target_tag

cmt_final_setup_ITkOccupancyConf = $(bin)setup_ITkOccupancyConf.make
cmt_dependencies_in_ITkOccupancyConf = $(bin)dependencies_ITkOccupancyConf.in
#cmt_final_setup_ITkOccupancyConf = $(bin)ITkOccupancy_ITkOccupancyConfsetup.make
cmt_local_ITkOccupancyConf_makefile = $(bin)ITkOccupancyConf.make

else

cmt_final_setup_ITkOccupancyConf = $(bin)setup.make
cmt_dependencies_in_ITkOccupancyConf = $(bin)dependencies.in
#cmt_final_setup_ITkOccupancyConf = $(bin)ITkOccupancysetup.make
cmt_local_ITkOccupancyConf_makefile = $(bin)ITkOccupancyConf.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)ITkOccupancysetup.make

#ITkOccupancyConf :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'ITkOccupancyConf'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = ITkOccupancyConf/
#ITkOccupancyConf::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/genconfig_header
# Author: Wim Lavrijsen (WLavrijsen@lbl.gov)

# Use genconf.exe to create configurables python modules, then have the
# normal python install procedure take over.

.PHONY: ITkOccupancyConf ITkOccupancyConfclean

confpy  := ITkOccupancyConf.py
conflib := $(bin)$(library_prefix)ITkOccupancy.$(shlibsuffix)
confdb  := ITkOccupancy.confdb
instdir := $(CMTINSTALLAREA)$(shared_install_subdir)/python/$(package)
product := $(instdir)/$(confpy)
initpy  := $(instdir)/__init__.py

ifdef GENCONF_ECHO
genconf_silent =
else
genconf_silent = $(silent)
endif

ITkOccupancyConf :: ITkOccupancyConfinstall

install :: ITkOccupancyConfinstall

ITkOccupancyConfinstall : /afs/cern.ch/work/a/altaylor/git_test/ITkOccupancyAthxAOD/ITkOccupancy/genConf/ITkOccupancy/$(confpy)
	@echo "Installing /afs/cern.ch/work/a/altaylor/git_test/ITkOccupancyAthxAOD/ITkOccupancy/genConf/ITkOccupancy in /afs/cern.ch/work/a/altaylor/git_test/ITkOccupancyAthxAOD/InstallArea/python" ; \
	 $(install_command) --exclude="*.py?" --exclude="__init__.py" --exclude="*.confdb" /afs/cern.ch/work/a/altaylor/git_test/ITkOccupancyAthxAOD/ITkOccupancy/genConf/ITkOccupancy /afs/cern.ch/work/a/altaylor/git_test/ITkOccupancyAthxAOD/InstallArea/python ; \

/afs/cern.ch/work/a/altaylor/git_test/ITkOccupancyAthxAOD/ITkOccupancy/genConf/ITkOccupancy/$(confpy) : $(conflib) /afs/cern.ch/work/a/altaylor/git_test/ITkOccupancyAthxAOD/ITkOccupancy/genConf/ITkOccupancy
	$(genconf_silent) $(genconfig_cmd)   -o /afs/cern.ch/work/a/altaylor/git_test/ITkOccupancyAthxAOD/ITkOccupancy/genConf/ITkOccupancy -p $(package) \
	  --configurable-module=GaudiKernel.Proxy \
	  --configurable-default-name=Configurable.DefaultName \
	  --configurable-algorithm=ConfigurableAlgorithm \
	  --configurable-algtool=ConfigurableAlgTool \
	  --configurable-auditor=ConfigurableAuditor \
          --configurable-service=ConfigurableService \
	  -i ../$(tag)/$(library_prefix)ITkOccupancy.$(shlibsuffix)

/afs/cern.ch/work/a/altaylor/git_test/ITkOccupancyAthxAOD/ITkOccupancy/genConf/ITkOccupancy:
	@ if [ ! -d /afs/cern.ch/work/a/altaylor/git_test/ITkOccupancyAthxAOD/ITkOccupancy/genConf/ITkOccupancy ] ; then mkdir -p /afs/cern.ch/work/a/altaylor/git_test/ITkOccupancyAthxAOD/ITkOccupancy/genConf/ITkOccupancy ; fi ;

ITkOccupancyConfclean :: ITkOccupancyConfuninstall
	$(cleanup_silent) $(remove_command) /afs/cern.ch/work/a/altaylor/git_test/ITkOccupancyAthxAOD/ITkOccupancy/genConf/ITkOccupancy/$(confpy) /afs/cern.ch/work/a/altaylor/git_test/ITkOccupancyAthxAOD/ITkOccupancy/genConf/ITkOccupancy/$(confdb)

uninstall :: ITkOccupancyConfuninstall

ITkOccupancyConfuninstall ::
	@$(uninstall_command) /afs/cern.ch/work/a/altaylor/git_test/ITkOccupancyAthxAOD/InstallArea/python
libITkOccupancy_so_dependencies = ../x86_64-slc6-gcc49-opt/libITkOccupancy.so
#-- start of cleanup_header --------------

clean :: ITkOccupancyConfclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(ITkOccupancyConf.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

ITkOccupancyConfclean ::
#-- end of cleanup_header ---------------
