#ifndef MYPACKAGE_MAXSTRIPTRACKOCCSTUDY_H
#define MYPACKAGE_MAXSTRIPTRACKOCCSTUDY_H 1

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h" //included under assumption you'll want to use some tools! Remove if you don't!
#include "TTree.h"

#include "TH1D.h"





class MaxStripTrackOccStudy: public ::AthAlgorithm { 
 public: 
  MaxStripTrackOccStudy( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~MaxStripTrackOccStudy(); 

  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();

 private:

  TTree *myTree; //!

  float n_events; //!
  float maxHits_0; //!
  float maxClusters_0; //!
  float maxOccupancy_0; //!
  float maxHitDensity_0; //!

  float maxHits_1; //!
  float maxClusters_1; //!
  float maxOccupancy_1; //!
  float maxHitDensity_1; //!

  float maxHits_2; //!
  float maxClusters_2; //!
  float maxOccupancy_2; //!
  float maxHitDensity_2; //!

  float maxHits_3; //!
  float maxClusters_3; //!
  float maxOccupancy_3; //!
  float maxHitDensity_3; //!
  
  // maximum hits in the sensor with the maximum clusters
  
  float nHitsinMaxClusters_0; //!
  float nHitsinMaxClusters_1; //!
  float nHitsinMaxClusters_2; //!
  float nHitsinMaxClusters_3; //!

  // maximum clusters in the sensor with the maximum hits
    
  float nClustersinMaxHits_0; //!
  float nClustersinMaxHits_1; //!
  float nClustersinMaxHits_2; //!
  float nClustersinMaxHits_3; //!

  /// DISKS

  float maxEndCapHits_0; //!
  float maxEndCapClusters_0; //!
  float maxEndCapOccupancy_0; //!
  float maxEndCapHitDensity_0; //!

  float maxEndCapHits_1; //!
  float maxEndCapClusters_1; //!
  float maxEndCapOccupancy_1; //!
  float maxEndCapHitDensity_1; //!

  float maxEndCapHits_2; //!
  float maxEndCapClusters_2; //!
  float maxEndCapOccupancy_2; //!
  float maxEndCapHitDensity_2; //!

  float maxEndCapHits_3; //!
  float maxEndCapClusters_3; //!
  float maxEndCapOccupancy_3; //!
  float maxEndCapHitDensity_3; //!
  
  float maxEndCapHits_4; //!
  float maxEndCapClusters_4; //!
  float maxEndCapOccupancy_4; //!
  float maxEndCapHitDensity_4; //!
  
  float maxEndCapHits_5; //!
  float maxEndCapClusters_5; //!
  float maxEndCapOccupancy_5; //!
  float maxEndCapHitDensity_5; //!
  
      
  TH1D *h_clus_row_0; //!
  TH1D *h_hits_row_0; //!
  TH1D *h_clus_row_1; //!
  TH1D *h_hits_row_1; //!
  TH1D *h_clus_row_2; //!
  TH1D *h_hits_row_2; //!
  TH1D *h_clus_row_3; //!
  TH1D *h_hits_row_3; //!
        
  TH1D *h_clus_sensor_0; //!
  TH1D *h_hits_sensor_0; //!
  TH1D *h_clus_sensor_1; //!
  TH1D *h_hits_sensor_1; //!
  TH1D *h_clus_sensor_2; //!
  TH1D *h_hits_sensor_2; //!
  TH1D *h_clus_sensor_3; //!
  TH1D *h_hits_sensor_3; //!

  


  

  

}; 

#endif //> !MYPACKAGE_MAXSTRIPSTUDY_H
