// MyPackage includes
#include "avgPixelOccupancy.h"

#include <math.h>

// MyPackage includes
#include "xAODJet/JetContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetAuxContainer.h"

#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "xAODTracking/TrackMeasurementValidationAuxContainer.h"

#include "xAODTracking/TrackStateValidationContainer.h"
#include "xAODTracking/TrackStateValidationAuxContainer.h"

//#include "xAODJet/JetAuxContainer.h"

#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackParticleAuxContainer.h"

#include "TVector.h"

#include "GaudiKernel/ITHistSvc.h"
#include "Counters.h"


#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TVector3.h"
#include "TH2D.h"

#include <math.h>




avgPixelOccupancy::avgPixelOccupancy( const std::string& name, ISvcLocator* pSvcLocator ) : AthAlgorithm( name, pSvcLocator ),
_incExt("inc")
{

  declareProperty( "incExt", _incExt ); //example property declaration

}


avgPixelOccupancy::~avgPixelOccupancy() {}


StatusCode avgPixelOccupancy::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");

  ServiceHandle<ITHistSvc> histSvc("THistSvc",name()); 
  CHECK( histSvc.retrieve() );


  ATH_MSG_INFO ("INIT: my Settings...");
  ATH_MSG_INFO ("\tincExt: " << _incExt ); 

  
  //TGraph *occ_z = new TGraph(occupancy.size(), &(occupancy[1]), &(zPos[1]));
  //TGraph *occ_z = new TGraph();
  //occ_z->SetName("hello_world");    
  //histSvc->regGraph("/MYSTREAM/occ_z",occ_z).ignore();

   myTree = new TTree("myTree","myTree");
  CHECK( histSvc->regTree("/MYSTREAM/myTree",myTree) );

  myHist = new TH1D("myHist","myHist",100,0,100);
  histSvc->regHist("/MYSTREAM/myHist",myHist).ignore();

  // plots for the barrel
  // each number represents a layer in the Pixel barrel


  // layer 0

  occ_z_B0 = new TGraph();
  occ_z_B0->SetName("occ_z_B0");

  hit_dens_z_B0 = new TGraph();
  hit_dens_z_B0->SetName("hit_dens_z_B0");
  
  clus_dens_z_B0 = new TGraph();
  clus_dens_z_B0->SetName("clus_dens_z_B0");
  
  clus_size_z_B0 = new TGraph();
  clus_size_z_B0->SetName("clus_size_z_B0");

  // layer 1

  occ_z_B1 = new TGraph();
  occ_z_B1->SetName("occ_z_B1");
  
  hit_dens_z_B1 = new TGraph();
  hit_dens_z_B1->SetName("hit_dens_z_B1");
  
  clus_dens_z_B1 = new TGraph();
  clus_dens_z_B1->SetName("clus_dens_z_B1");
  
  clus_size_z_B1 = new TGraph();
  clus_size_z_B1->SetName("clus_size_z_B1");

  // layer 2
    
  occ_z_B2 = new TGraph();
  occ_z_B2->SetName("occ_z_B2");

  hit_dens_z_B2 = new TGraph();
  hit_dens_z_B2->SetName("hit_dens_z_B2");
  
  clus_dens_z_B2 = new TGraph();
  clus_dens_z_B2->SetName("clus_dens_z_B2");
  
  clus_size_z_B2 = new TGraph();
  clus_size_z_B2->SetName("clus_size_z_B2");

  // layer 3
  
  occ_z_B3 = new TGraph();
  occ_z_B3->SetName("occ_z_B3");

  hit_dens_z_B3 = new TGraph();
  hit_dens_z_B3->SetName("hit_dens_z_B3");

  clus_dens_z_B3 = new TGraph();
  clus_dens_z_B3->SetName("clus_dens_z_B3");
  
  clus_size_z_B3 = new TGraph();
  clus_size_z_B3->SetName("clus_size_z_B3");

  // layer 4
  
  occ_z_B4 = new TGraph();
  occ_z_B4->SetName("occ_z_B4");

  hit_dens_z_B4 = new TGraph();
  hit_dens_z_B4->SetName("hit_dens_z_B4");

  clus_dens_z_B4 = new TGraph();
  clus_dens_z_B4->SetName("clus_dens_z_B4");
  
  clus_size_z_B4 = new TGraph();
  clus_size_z_B4->SetName("clus_size_z_B4");


  histSvc->regGraph("/MYSTREAM/occ_z_B0",occ_z_B0).ignore();
  histSvc->regGraph("/MYSTREAM/occ_z_B1",occ_z_B1).ignore();
  histSvc->regGraph("/MYSTREAM/occ_z_B2",occ_z_B2).ignore();
  histSvc->regGraph("/MYSTREAM/occ_z_B3",occ_z_B3).ignore();
  histSvc->regGraph("/MYSTREAM/occ_z_B4",occ_z_B4).ignore();
  
  histSvc->regGraph("/MYSTREAM/hit_dens_z_B0",hit_dens_z_B0).ignore();
  histSvc->regGraph("/MYSTREAM/hit_dens_z_B1",hit_dens_z_B1).ignore();
  histSvc->regGraph("/MYSTREAM/hit_dens_z_B2",hit_dens_z_B2).ignore();
  histSvc->regGraph("/MYSTREAM/hit_dens_z_B3",hit_dens_z_B3).ignore();
  histSvc->regGraph("/MYSTREAM/hit_dens_z_B4",hit_dens_z_B4).ignore();
  
  histSvc->regGraph("/MYSTREAM/clus_dens_z_B0",clus_dens_z_B0).ignore();
  histSvc->regGraph("/MYSTREAM/clus_dens_z_B1",clus_dens_z_B1).ignore();
  histSvc->regGraph("/MYSTREAM/clus_dens_z_B2",clus_dens_z_B2).ignore();
  histSvc->regGraph("/MYSTREAM/clus_dens_z_B3",clus_dens_z_B3).ignore();
  histSvc->regGraph("/MYSTREAM/clus_dens_z_B4",clus_dens_z_B4).ignore();
  
  histSvc->regGraph("/MYSTREAM/clus_size_z_B0",clus_size_z_B0).ignore();
  histSvc->regGraph("/MYSTREAM/clus_size_z_B1",clus_size_z_B1).ignore();
  histSvc->regGraph("/MYSTREAM/clus_size_z_B2",clus_size_z_B2).ignore();
  histSvc->regGraph("/MYSTREAM/clus_size_z_B3",clus_size_z_B3).ignore();
  histSvc->regGraph("/MYSTREAM/clus_size_z_B4",clus_size_z_B4).ignore();

  // ring layer 0 
  
  occ_z_EC0 = new TGraph();
  occ_z_EC0->SetName("occ_z_EC0");

  hit_dens_z_EC0 = new TGraph();
  hit_dens_z_EC0->SetName("hit_dens_z_EC0");
  
  clus_dens_z_EC0 = new TGraph();
  clus_dens_z_EC0->SetName("clus_dens_z_EC0");
  
  clus_size_z_EC0 = new TGraph();
  clus_size_z_EC0->SetName("clus_size_z_EC0");

  // ring layer 1

  occ_z_EC1 = new TGraph();
  occ_z_EC1->SetName("occ_z_EC1");
  
  hit_dens_z_EC1 = new TGraph();
  hit_dens_z_EC1->SetName("hit_dens_z_EC1");

  clus_dens_z_EC1 = new TGraph();
  clus_dens_z_EC1->SetName("clus_dens_z_EC1");
  
  clus_size_z_EC1 = new TGraph();
  clus_size_z_EC1->SetName("clus_size_z_EC1");

  // ring layer 2
    
  occ_z_EC2 = new TGraph();
  occ_z_EC2->SetName("occ_z_EC2");

  hit_dens_z_EC2 = new TGraph();
  hit_dens_z_EC2->SetName("hit_dens_z_EC2");
  
  clus_dens_z_EC2 = new TGraph();
  clus_dens_z_EC2->SetName("clus_dens_z_EC2");
  
  clus_size_z_EC2 = new TGraph();
  clus_size_z_EC2->SetName("clus_size_z_EC2");

  // ring layer 3
  
  occ_z_EC3 = new TGraph();
  occ_z_EC3->SetName("occ_z_EC3");

  hit_dens_z_EC3 = new TGraph();
  hit_dens_z_EC3->SetName("hit_dens_z_EC3");
  
  clus_dens_z_EC3 = new TGraph();
  clus_dens_z_EC3->SetName("clus_dens_z_EC3");
  
  clus_size_z_EC3 = new TGraph();
  clus_size_z_EC3->SetName("clus_size_z_EC3");

  // ring layer 4 
  
  occ_z_EC4 = new TGraph();
  occ_z_EC4->SetName("occ_z_EC4");

  hit_dens_z_EC4 = new TGraph();
  hit_dens_z_EC4->SetName("hit_dens_z_EC4");

  clus_dens_z_EC4 = new TGraph();
  clus_dens_z_EC4->SetName("clus_dens_z_EC4");
  
  clus_size_z_EC4 = new TGraph();
  clus_size_z_EC4->SetName("clus_size_z_EC4");


  histSvc->regGraph("/MYSTREAM/occ_z_EC0",occ_z_EC0).ignore();
  histSvc->regGraph("/MYSTREAM/occ_z_EC1",occ_z_EC1).ignore();
  histSvc->regGraph("/MYSTREAM/occ_z_EC2",occ_z_EC2).ignore();
  histSvc->regGraph("/MYSTREAM/occ_z_EC3",occ_z_EC3).ignore();
  histSvc->regGraph("/MYSTREAM/occ_z_EC4",occ_z_EC4).ignore();
  
  histSvc->regGraph("/MYSTREAM/hit_dens_z_EC0",hit_dens_z_EC0).ignore();
  histSvc->regGraph("/MYSTREAM/hit_dens_z_EC1",hit_dens_z_EC1).ignore();
  histSvc->regGraph("/MYSTREAM/hit_dens_z_EC2",hit_dens_z_EC2).ignore();
  histSvc->regGraph("/MYSTREAM/hit_dens_z_EC3",hit_dens_z_EC3).ignore();
  histSvc->regGraph("/MYSTREAM/hit_dens_z_EC4",hit_dens_z_EC4).ignore();

  histSvc->regGraph("/MYSTREAM/clus_dens_z_EC0",clus_dens_z_EC0).ignore();
  histSvc->regGraph("/MYSTREAM/clus_dens_z_EC1",clus_dens_z_EC1).ignore();
  histSvc->regGraph("/MYSTREAM/clus_dens_z_EC2",clus_dens_z_EC2).ignore();
  histSvc->regGraph("/MYSTREAM/clus_dens_z_EC3",clus_dens_z_EC3).ignore();
  histSvc->regGraph("/MYSTREAM/clus_dens_z_EC4",clus_dens_z_EC4).ignore();
  
  histSvc->regGraph("/MYSTREAM/clus_size_z_EC0",clus_size_z_EC0).ignore();
  histSvc->regGraph("/MYSTREAM/clus_size_z_EC1",clus_size_z_EC1).ignore();
  histSvc->regGraph("/MYSTREAM/clus_size_z_EC2",clus_size_z_EC2).ignore();
  histSvc->regGraph("/MYSTREAM/clus_size_z_EC3",clus_size_z_EC3).ignore();
  histSvc->regGraph("/MYSTREAM/clus_size_z_EC4",clus_size_z_EC4).ignore();


   // plots for the end cap
  // each number is for a disk in the endcap

  /*

  occ_r_0 = new TGraph();
  occ_r_0->SetName("occ_r_0");

  hit_dens_r_0 = new TGraph();
  hit_dens_r_0->SetName("hit_dens_r_0");

  
  clus_size_r_0 = new TGraph();
  clus_size_r_0->SetName("clus_size_r_0");


  occ_r_1 = new TGraph();
  occ_r_1->SetName("occ_r_1");
  
  hit_dens_r_1 = new TGraph();
  hit_dens_r_1->SetName("hit_dens_r_1");

  
  clus_size_r_1 = new TGraph();
  clus_size_r_1->SetName("clus_size_r_1");
    

    
  occ_r_2 = new TGraph();
  occ_r_2->SetName("occ_r_2");

  hit_dens_r_2 = new TGraph();
  hit_dens_r_2->SetName("hit_dens_r_2");

  
  clus_size_r_2 = new TGraph();
  clus_size_r_2->SetName("clus_size_r_2");
  
  
  occ_r_3 = new TGraph();
  occ_r_3->SetName("occ_r_3");

  hit_dens_r_3 = new TGraph();
  hit_dens_r_3->SetName("hit_dens_r_3");

  
  clus_size_r_3 = new TGraph();
  clus_size_r_3->SetName("clus_size_r_3");
    
  occ_r_4 = new TGraph();
  occ_r_4->SetName("occ_r_4");

  hit_dens_r_4 = new TGraph();
  hit_dens_r_4->SetName("hit_dens_r_4");

  
  clus_size_r_4 = new TGraph();
  clus_size_r_4->SetName("clus_size_r_4");
      
  occ_r_5 = new TGraph();
  occ_r_5->SetName("occ_r_5");

  hit_dens_r_5 = new TGraph();
  hit_dens_r_5->SetName("hit_dens_r_5");

  
  clus_size_r_5 = new TGraph();
  clus_size_r_5->SetName("clus_size_r_5");

  */

  //myHist = new TH1D("myHist","myHist",10,0,10);
  //anotherHist = new TH1D("anotherHist","anotherHist",10,0,10);
  //histSvc->regHist("/MYSTREAM/myHist",myHist).ignore(); //or check the statuscode

  // barrel histograms..

  
  pix_barrel_occ_map2D = new TH2D("pix_barrel_occ_map2D","occupancies in percent (200 pileup)",(240/4),-3100,3100,(160/4),0.,1100.);
  pix_barrel_occ_map2D_norm = new TH2D("pix_barrel_occ_map2D_norm","occupancies in percent (200 pileup)",(240/4),-3100.,3100.,(160/4),0.,1100.);
  pix_barrel_hitmap2D = new TH2D("pix_barrel_hitmap2D","Hits / mm^{2} ",(240/4),-3100,3100,(160/4),0.,1100.);
  pix_barrel_hitmap2D_norm = new TH2D("pix_barrel_hitmap2D_norm"," Hits / mm^{2} ",(240/4),-3100.,3100.,(160/4),0.,1100.);
  
  pix_barrel_occ_map2DV2 = new TH2D("pix_barrel_occ_map2DV2","occupancies in percent (200 pileup)",(240/2),-3100,3100,(160/2),0.,1100.);
  pix_barrel_occ_map2DV2_norm = new TH2D("pix_barrel_occ_map2DV2_norm","occupancies in percent (200 pileup)",(240/2),-3100.,3100.,(160/2),0.,1100.);
  pix_barrel_hitmap2DV2 = new TH2D("pix_barrel_hitmap2DV2","Hits / mm^{2} ",(240/2),-3100,3100,(160/2),0.,1100.);
  pix_barrel_hitmap2DV2_norm = new TH2D("pix_barrel_hitmap2DV2_norm"," Hits / mm^{2} ",(240/2),-3100.,3100.,(160/2),0.,1100.);

  // disk histograms..

  pix_disk_occ_map2D = new TH2D("pix_disk_occ_map2D","occupancies in percent (200 pileup)",(240/4),-3100,3100,(160/4),0.,1100.);
  pix_disk_occ_map2D_norm = new TH2D("pix_disk_occ_map2D_norm","occupancies in percent (200 pileup)",(240/4),-3100.,3100.,(160/4),0.,1100.);
  pix_disk_hitmap2D = new TH2D("pix_disk_hitmap2D","Hits / mm^{2} ",(240/4),-3100,3100,(160/4),0.,1100.);
  pix_disk_hitmap2D_norm = new TH2D("pix_disk_hitmap2D_norm"," Hits / mm^{2} ",(240/4),-3100.,3100.,(160/4),0.,1100.);
  
  pix_disk_occ_map2DV2 = new TH2D("pix_disk_occ_map2DV2","occupancies in percent (200 pileup)",(240/2),-3100,3100,(160/2),0.,1100.);
  pix_disk_occ_map2DV2_norm = new TH2D("pix_disk_occ_map2DV2_norm","occupancies in percent (200 pileup)",(240/2),-3100.,3100.,(160/2),0.,1100.);
  pix_disk_hitmap2DV2 = new TH2D("pix_disk_hitmap2DV2","Hits / mm^{2} ",(240/2),-3100,3100,(160/2),0.,1100.);
  pix_disk_hitmap2DV2_norm = new TH2D("pix_disk_hitmap2DV2_norm"," Hits / mm^{2} ",(240/2),-3100.,3100.,(160/2),0.,1100.);

  // barrel 
  histSvc->regHist("/MYSTREAM/pix_barrel_occ_map2D",pix_barrel_occ_map2D).ignore();
  histSvc->regHist("/MYSTREAM/pix_barrel_occ_map2D_norm",pix_barrel_occ_map2D_norm).ignore();
  histSvc->regHist("/MYSTREAM/pix_barrel_hitmap2D",pix_barrel_hitmap2D).ignore();
  histSvc->regHist("/MYSTREAM/pix_barrel_hitmap2D_norm",pix_barrel_hitmap2D_norm).ignore();
    
  histSvc->regHist("/MYSTREAM/pix_barrel_occ_map2DV2",pix_barrel_occ_map2DV2).ignore();
  histSvc->regHist("/MYSTREAM/pix_barrel_occ_map2DV2_norm",pix_barrel_occ_map2DV2_norm).ignore();
  histSvc->regHist("/MYSTREAM/pix_barrel_hitmap2DV2",pix_barrel_hitmap2DV2).ignore();
  histSvc->regHist("/MYSTREAM/pix_barrel_hitmap2DV2_norm",pix_barrel_hitmap2DV2_norm).ignore();
  
  // disk registration
  histSvc->regHist("/MYSTREAM/pix_disk_occ_map2D",pix_disk_occ_map2D).ignore();
  histSvc->regHist("/MYSTREAM/pix_disk_occ_map2D_norm",pix_disk_occ_map2D_norm).ignore();
  histSvc->regHist("/MYSTREAM/pix_disk_hitmap2D",pix_disk_hitmap2D).ignore();
  histSvc->regHist("/MYSTREAM/pix_disk_hitmap2D_norm",pix_disk_hitmap2D_norm).ignore();
    
  histSvc->regHist("/MYSTREAM/pix_disk_occ_map2DV2",pix_disk_occ_map2DV2).ignore();
  histSvc->regHist("/MYSTREAM/pix_disk_occ_map2DV2_norm",pix_disk_occ_map2DV2_norm).ignore();
  histSvc->regHist("/MYSTREAM/pix_disk_hitmap2DV2",pix_disk_hitmap2DV2).ignore();
  histSvc->regHist("/MYSTREAM/pix_disk_hitmap2DV2_norm",pix_disk_hitmap2DV2_norm).ignore();
  

  // there are 4 layers in the strip barrel
  int nLayers = 5; 
  // nmodule is the number of modules along z in the x aod, it is eta_module(min) -> eta_module(max)
  // modules with zero occupancy are thrown away at the end.  
  
  int nmodule = 60; 
  int nmax = nmodule;
  int nmin = -nmodule;

  // number of staves as we go along the barrel
  std::vector<float> nStaves_Extended {16.0,16.0,32.0,44.0,54.0};
  std::vector<float> nStaves_Inclined {18.0,18.0,32.0,44.0,54.0};
  std::vector<float> nStaves = nStaves_Extended; 
  float inclinedFactor = -1; 

  // if inclined factor = 2 --> inclined layout, if = 1 --> extended layout
  // this needs changed and code recompiled when changing layout
  // to do -- this needs fixed so it takes it from the job options.


  // switching to extended!
  if(_incExt.find("inc")!=string::npos || _incExt.find("Inc")!=string::npos){
    ATH_MSG_INFO("Using settings for inclined layout");
    inclinedFactor=2;
    nStaves = nStaves_Inclined; 
  }
  else if(_incExt.find("ext")!=string::npos || _incExt.find("Ext")!=string::npos){
    ATH_MSG_INFO("Using settings for extended layout");
    inclinedFactor=1;
    nStaves =  nStaves_Extended; 
  }  
  else{
    ATH_MSG_WARNING("Warning geometry not specified (inc/ext). Check jobOptions! Default inclined used. ");
    inclinedFactor=2;
    nStaves = nStaves_Inclined; 
  }
  ATH_MSG_INFO("inclinedFactor: "<<inclinedFactor);

  // nRows x nCols
  float nRows = 336.0;
  float nCols = 400.0;

  float chipArea = 20.0*16.8;
  float nChips = 0;

  // loop over layers and etaModuleIDs
   for (int i=0; i<nLayers; ++i) {
    for (int j=nmin; j<nmax; ++j) {

      // in layer 0, we have double chips and thereafter we have quads..
      // also note that in the inclined layout, in the barrel where the modules are inclined, there is half the number of chips in a module.

      if ( i == 0 ) { nChips = 2; }
      if ( i != 0 ) { nChips = 4 ; }

      int BEC = 0;
      Counters c = Counters();
      c.setReadOut(nRows*nCols*nChips*nStaves[i]); 
      c.setArea(chipArea*nChips*nStaves[i]);

      // also note that in the inclined layout, in the barrel where the modules are inclined, there is half the number of chips in a module.
      // number of inclined modules also varies depending on the layer...

      if (i == 0 && abs(j) > 4 ) {
	c.setReadOut(nRows*nCols*nChips*nStaves[i]*(1.0/inclinedFactor)); 
	c.setArea(chipArea*nChips*nStaves[i]*(1.0/inclinedFactor));
      }

      
      if (i == 1 && abs(j) > 5 ) {
	c.setReadOut(nRows*nCols*nChips*nStaves[i]*(1.0/inclinedFactor)); 
	c.setArea(chipArea*nChips*nStaves[i]*(1.0/inclinedFactor));
      }
      

      if (i == 2 && abs(j) > 6 ) {
	c.setReadOut(nRows*nCols*nChips*nStaves[i]*(1.0/inclinedFactor)); 
	c.setArea(chipArea*nChips*nStaves[i]*(1.0/inclinedFactor));
      }
      

      if (i == 3 && abs(j) > 7 ) {
	c.setReadOut(nRows*nCols*nChips*nStaves[i]*(1.0/inclinedFactor)); 
	c.setArea(chipArea*nChips*nStaves[i]*(1.0/inclinedFactor));
      }
      

      if (i == 4 && abs(j) > 8 ) {
	c.setReadOut(nRows*nCols*nChips*nStaves[i]*(1.0/inclinedFactor)); 
	c.setArea(chipArea*nChips*nStaves[i]*(1.0/inclinedFactor));
      }
      

      
      // linker is a counter object (takes layer, etaID and barrel / end cap as constructor ) 
      std::tuple<int,int,int> linker (i,j,BEC);

      // pixel barrel is a map declared in the header
      pixelBarrel[linker] = c;


    }
   }

   // beginning of declaration for the end cap pixels
   
    // first loop is for BEC
    // second loop is for disks
    // third loop is for rings

   // swap nDisks for nRingLayers
   int nRingLayers = 4;
   int nRings = 20;

   
  // number of staves as we go along the endcap
  std::vector<float> nStaves_EndCap {24.0,36.0,48.0,60.0};
  int nChipsEC = 4;

    for (int k=0; k < 2; ++k ) {
      for (int i=0; i< nRingLayers; ++i) {
	for (int j=0; j< nRings; ++j) {

	  
	  Counters c = Counters();
	  c.setReadOut(nRows*nCols*nChipsEC*nStaves_EndCap[i]);
	  c.setArea(chipArea*nChipsEC*nStaves_EndCap[i]);

	  // linker is a counter object (takes layer, etaID and barrel / end cap as constructor ) 
	  std::tuple<int,int,int> linker (i,j,k);

	  // strip endcap is a map declared in the header
	  pixelEndCap[linker] = c;



	}
      }
    }



  return StatusCode::SUCCESS;
}

/*StatusCode avgPixelOccupancy::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");

  return StatusCode::SUCCESS;
}

*/

StatusCode avgPixelOccupancy::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");

  //----------------------------
  // Event information
  //--------------------------- 
  const xAOD::EventInfo* eventInfo = 0; //NOTE: Everything that comes from the storegate direct from the input files is const!

  // ask the event store to retrieve the xAOD EventInfo container
  //CHECK( evtStore()->retrieve( eventInfo, "EventInfo") );  // the second argument ("EventInfo") is the key name
  CHECK( evtStore()->retrieve( eventInfo) );
  // if there is only one container of that type in the xAOD (as with the EventInfo container), you do not need to pass
  // the key name, the default will be taken as the only key name in the xAOD 

  // check if data or MC
  bool isMC = true;
  if(!eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION ) ){
    isMC = false;
  }
   // PIX Occupancy Studies

  // number of events used in analysis is required for averaging...
  nEvents += 1.0;

  if (0==int(nEvents)%100)
    std::cout << " processing event number: " << nEvents << std::endl; 
   
    const xAOD::TrackMeasurementValidationContainer* PIXClusters = 0;
    CHECK( evtStore()->retrieve(PIXClusters, "PixelClusters") );

    ///

    // Occupancy Studies for the PIX Barrel!

    // this index is used for the end cap
    int set_k = 0; 

   for ( const auto* pix : *PIXClusters) {
     
	float clus_size = pix->rdoIdentifierList().size();
	int layer = pix->auxdata<int>("layer");
        int etaModule = pix->auxdata<int>("eta_module");
	int BEC = pix->auxdata<int>("bec");
	
	float x = pix->globalX();
	float y = pix->globalY();
	float z = pix->globalZ();
	float r = sqrt(x*x + y*y);

	std::tuple<int,int,int> linker (layer,etaModule,BEC);

      // check we are in the barrel
      if ( pix->auxdata<int>("bec") == 0 ) {

	std::tuple<int,int,int> linker (layer,etaModule,BEC);
	pixelBarrel[linker].add(x,y,z,clus_size);
	//pixelBarrel[linker].sumclussize(clus_size); 

      }

      if ( pix->auxdata<int>("bec") != 0 ) {

	if ( pix->auxdata<int>("bec") == 2 ) {

	  

	//set_k=0;

	/*if ( pix->auxdata<int>("bec") == 2 ) {


          set_k = 0;
        }


        else { set_k = 1; } */

	  //if ( layer == 0 && etaModule == 3 && set_k == 0) { std::cout << " print out z clus pos " << z << std::endl; }

	  if ( z > 0.0 ) { 


	std::tuple<int,int,int> linker (layer,etaModule,0);
	pixelEndCap[linker].add(x,y,z,clus_size);

	  }

	}

      } // closes brace with the endcap

   }

   std::tuple<int,int,int> test_linker (0,3,0);
   //std::cout << "running avg... " << pixelEndCap[test_linker].getZ() << std::endl; 


   
  return StatusCode::SUCCESS;
}



StatusCode avgPixelOccupancy::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");

  std::cout << " begin printing occupancies to screen..... " << std::endl;

  std::cout << " how many events? " << nEvents << std::endl; 

  int nLayers = 5; 
  int nmodule = 60; 
  int nmax = nmodule;
  int nmin = -nmodule;
  int BEC = 0;
  
  //TMultiGraph *mg_occ_z = new TMultiGraph("mg_occ_z","mg_occ_z");

  //vector<vector<float>> MyMatrix[nLayers][4];
  //TGraph *gr = new TGraph();

  for (int i=0; i<nLayers; ++i) {

    //create a vector to hold occupancy + hit density..
    ///std::vector<float> 
    std::vector<float> occupancy;
    std::vector<float> hit_density; 
    std::vector<float> zPos;
    //TGraph *occ_z = new TGraph();
    //TGraph *hit_density_z = new TGraph();
    
    // loop over eta modules
    for (int j=nmin; j<nmax; ++j) {
      // linker is a tuple which we use to look up the strip detector..
      std::tuple<int,int,int> linker (i,j,BEC);
      pixelBarrel[linker].setEvents(nEvents);

      // only add to vectors if occupancy is non zero (this is a result of overcounting initially..)

      if ( pixelBarrel[linker].occupancy()*100.0 != 0.0 ) {

	//float getR = sqrt( pixelBarrel[linker].getX()*pixelBarrel[linker].getX() + pixelBarrel[linker].getYa)*pixelBarrel[linker].getY() );
	//std::cout << " get R? " << getR << std::endl;

	std::cout << "get Z, getR, get Occupancy? " << pixelBarrel[linker].getZ() << "   "  << pixelBarrel[linker].getR() << "  " <<  pixelBarrel[linker].occupancy()*100.0 << std::endl; 

	pix_barrel_occ_map2D->Fill(pixelBarrel[linker].getZ(),pixelBarrel[linker].getR(),pixelBarrel[linker].occupancy()*100.0);
	pix_barrel_occ_map2D_norm->Fill(pixelBarrel[linker].getZ(),pixelBarrel[linker].getR(),1);
	pix_barrel_hitmap2D->Fill(pixelBarrel[linker].getZ(),pixelBarrel[linker].getR(),pixelBarrel[linker].fluency());
	pix_barrel_hitmap2D_norm->Fill(pixelBarrel[linker].getZ(),pixelBarrel[linker].getR(),1);
	    
	pix_barrel_occ_map2DV2->Fill(pixelBarrel[linker].getZ(),pixelBarrel[linker].getR(),pixelBarrel[linker].occupancy()*100.0);
	pix_barrel_occ_map2DV2_norm->Fill(pixelBarrel[linker].getZ(),pixelBarrel[linker].getR(),1);
	pix_barrel_hitmap2DV2->Fill(pixelBarrel[linker].getZ(),pixelBarrel[linker].getR(),pixelBarrel[linker].fluency());
	pix_barrel_hitmap2DV2_norm->Fill(pixelBarrel[linker].getZ(),pixelBarrel[linker].getR(),1);
	
	//occupancy.push_back(pixelBarrel[linker].occupancy()*100.0*100.0);
	//zPos.push_back(pixelBarrel[linker].getZ());
        // fill the plots for the different layers

       // fill tgraphs for the different layers!

      if ( i == 0 ) {
	occ_z_B0->SetPoint(occ_z_B0->GetN(),pixelBarrel[linker].getZ(), pixelBarrel[linker].occupancy()*100.0);
	hit_dens_z_B0->SetPoint(hit_dens_z_B0->GetN(),pixelBarrel[linker].getZ(), pixelBarrel[linker].fluency());
	clus_dens_z_B0->SetPoint(clus_dens_z_B0->GetN(),pixelBarrel[linker].getZ(), pixelBarrel[linker].clusterDensity());
	clus_size_z_B0->SetPoint(clus_size_z_B0->GetN(),pixelBarrel[linker].getZ(), pixelBarrel[linker].averageClusterSize());
	
      }

      
      if ( i == 1 ) {
	occ_z_B1->SetPoint(occ_z_B1->GetN(),pixelBarrel[linker].getZ(), pixelBarrel[linker].occupancy()*100.0);
	hit_dens_z_B1->SetPoint(hit_dens_z_B1->GetN(),pixelBarrel[linker].getZ(), pixelBarrel[linker].fluency());
	clus_dens_z_B1->SetPoint(clus_dens_z_B1->GetN(),pixelBarrel[linker].getZ(), pixelBarrel[linker].clusterDensity());
	clus_size_z_B1->SetPoint(clus_size_z_B1->GetN(),pixelBarrel[linker].getZ(), pixelBarrel[linker].averageClusterSize());
      }

      
      if ( i == 2 ) {
	occ_z_B2->SetPoint(occ_z_B2->GetN(),pixelBarrel[linker].getZ(), pixelBarrel[linker].occupancy()*100.0);
	hit_dens_z_B2->SetPoint(hit_dens_z_B2->GetN(),pixelBarrel[linker].getZ(), pixelBarrel[linker].fluency());
	clus_dens_z_B2->SetPoint(clus_dens_z_B2->GetN(),pixelBarrel[linker].getZ(), pixelBarrel[linker].clusterDensity());
	clus_size_z_B2->SetPoint(clus_size_z_B2->GetN(),pixelBarrel[linker].getZ(), pixelBarrel[linker].averageClusterSize());
      }

      
      if ( i == 3 ) {
	occ_z_B3->SetPoint(occ_z_B3->GetN(),pixelBarrel[linker].getZ(), pixelBarrel[linker].occupancy()*100.0);
	hit_dens_z_B3->SetPoint(hit_dens_z_B3->GetN(),pixelBarrel[linker].getZ(), pixelBarrel[linker].fluency());
	clus_dens_z_B3->SetPoint(clus_dens_z_B3->GetN(),pixelBarrel[linker].getZ(), pixelBarrel[linker].clusterDensity());
	clus_size_z_B3->SetPoint(clus_size_z_B3->GetN(),pixelBarrel[linker].getZ(), pixelBarrel[linker].averageClusterSize());
      }

      
      
      if ( i == 4 ) {
	occ_z_B4->SetPoint(occ_z_B4->GetN(),pixelBarrel[linker].getZ(), pixelBarrel[linker].occupancy()*100.0);
	hit_dens_z_B4->SetPoint(hit_dens_z_B4->GetN(),pixelBarrel[linker].getZ(), pixelBarrel[linker].fluency());
	clus_dens_z_B4->SetPoint(clus_dens_z_B4->GetN(),pixelBarrel[linker].getZ(), pixelBarrel[linker].clusterDensity());
	clus_size_z_B4->SetPoint(clus_size_z_B4->GetN(),pixelBarrel[linker].getZ(), pixelBarrel[linker].averageClusterSize());
      }




      

      }
    }
  } // end of for loop over the barrel



  // --------------------
  // END OF PIX BARREL STUDIES
  // --------------------

  // end cap
    int nRingLayers = 6;
    int nRings = 20;
    
    // first loop is for BEC
    // second loop is for disks
    // third loop is for rings

    for (int k=0; k < 2; ++k ) {
      for (int i=0; i< nRingLayers; ++i) {
	for (int j=0; j< nRings; ++j) {
	  
	  // linker is a counter object (takes layer, etaID and barrel / end cap as constructor ) 
	  std::tuple<int,int,int> linker (i,j,k);

	  pixelEndCap[linker].setEvents(nEvents);
	  // only add to vectors if occupancy is non zero (this is a result of overcounting initially..)
	  if ( pixelEndCap[linker].occupancy() != 0.0 ) {

	    
	    pix_disk_occ_map2D->Fill(pixelEndCap[linker].getZ(),pixelEndCap[linker].getR(),pixelEndCap[linker].occupancy()*100.0);
	    pix_disk_occ_map2D_norm->Fill(pixelEndCap[linker].getZ(),pixelEndCap[linker].getR(),1);
	    pix_disk_hitmap2D->Fill(pixelEndCap[linker].getZ(),pixelEndCap[linker].getR(),pixelEndCap[linker].fluency());
	    pix_disk_hitmap2D_norm->Fill(pixelEndCap[linker].getZ(),pixelEndCap[linker].getR(),1);
	    
	    pix_disk_occ_map2DV2->Fill(pixelEndCap[linker].getZ(),pixelEndCap[linker].getR(),pixelEndCap[linker].occupancy()*100.0);
	    pix_disk_occ_map2DV2_norm->Fill(pixelEndCap[linker].getZ(),pixelEndCap[linker].getR(),1);
	    pix_disk_hitmap2DV2->Fill(pixelEndCap[linker].getZ(),pixelEndCap[linker].getR(),pixelEndCap[linker].fluency());
	    pix_disk_hitmap2DV2_norm->Fill(pixelEndCap[linker].getZ(),pixelEndCap[linker].getR(),1);

	
	    // only fill tgraphs for positive R, eg k = 0

	    if ( k == 0 ) {
	
	      if ( i == 0 ) {

		std::cout << " print out Z position! of pixel module! " << pixelEndCap[linker].getZ() << std::endl; 

		occ_z_EC0->SetPoint(occ_z_EC0->GetN(),pixelEndCap[linker].getZ(), pixelEndCap[linker].occupancy()*100.0);
		hit_dens_z_EC0->SetPoint(hit_dens_z_EC0->GetN(),pixelEndCap[linker].getZ(), pixelEndCap[linker].fluency());
		clus_dens_z_EC0->SetPoint(clus_dens_z_EC0->GetN(),pixelEndCap[linker].getZ(), pixelEndCap[linker].clusterDensity());
		clus_size_z_EC0->SetPoint(clus_size_z_EC0->GetN(),pixelEndCap[linker].getZ(), pixelEndCap[linker].averageClusterSize());
	      }

      
	      if ( i == 1 ) {
		occ_z_EC1->SetPoint(occ_z_EC1->GetN(),pixelEndCap[linker].getZ(), pixelEndCap[linker].occupancy()*100.0);
		hit_dens_z_EC1->SetPoint(hit_dens_z_EC1->GetN(),pixelEndCap[linker].getZ(), pixelEndCap[linker].fluency());
		clus_dens_z_EC1->SetPoint(clus_dens_z_EC1->GetN(),pixelEndCap[linker].getZ(), pixelEndCap[linker].clusterDensity());
		clus_size_z_EC1->SetPoint(clus_size_z_EC1->GetN(),pixelEndCap[linker].getZ(), pixelEndCap[linker].averageClusterSize());
	      }

      
	      if ( i == 2 ) {
		occ_z_EC2->SetPoint(occ_z_EC2->GetN(),pixelEndCap[linker].getZ(), pixelEndCap[linker].occupancy()*100.0);
		hit_dens_z_EC2->SetPoint(hit_dens_z_EC2->GetN(),pixelEndCap[linker].getZ(), pixelEndCap[linker].fluency());
		clus_dens_z_EC2->SetPoint(clus_dens_z_EC2->GetN(),pixelEndCap[linker].getZ(), pixelEndCap[linker].clusterDensity());
		clus_size_z_EC2->SetPoint(clus_size_z_EC2->GetN(),pixelEndCap[linker].getZ(), pixelEndCap[linker].averageClusterSize());
	      }

      
	      if ( i == 3 ) {
		occ_z_EC3->SetPoint(occ_z_EC3->GetN(),pixelEndCap[linker].getZ(), pixelEndCap[linker].occupancy()*100.0);
		hit_dens_z_EC3->SetPoint(hit_dens_z_EC3->GetN(),pixelEndCap[linker].getZ(), pixelEndCap[linker].fluency());
		clus_dens_z_EC3->SetPoint(clus_dens_z_EC3->GetN(),pixelEndCap[linker].getZ(), pixelEndCap[linker].clusterDensity());
		clus_size_z_EC3->SetPoint(clus_size_z_EC3->GetN(),pixelEndCap[linker].getZ(), pixelEndCap[linker].averageClusterSize());
	      }

      
      
	      if ( i == 4 ) {
		occ_z_EC4->SetPoint(occ_z_EC4->GetN(),pixelEndCap[linker].getZ(), pixelEndCap[linker].occupancy()*100.0);
		hit_dens_z_EC4->SetPoint(hit_dens_z_EC4->GetN(),pixelEndCap[linker].getZ(), pixelEndCap[linker].fluency());
		clus_dens_z_EC4->SetPoint(clus_dens_z_EC4->GetN(),pixelEndCap[linker].getZ(), pixelEndCap[linker].clusterDensity());
		clus_size_z_EC4->SetPoint(clus_size_z_EC4->GetN(),pixelEndCap[linker].getZ(), pixelEndCap[linker].averageClusterSize());
	      }

	    }



	  }
	}
      }
    }// ends for loop over the endcap


  return StatusCode::SUCCESS;
}
  



