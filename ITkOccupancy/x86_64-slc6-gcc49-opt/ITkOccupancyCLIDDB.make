#-- start of make_header -----------------

#====================================
#  Document ITkOccupancyCLIDDB
#
#   Generated Mon Apr 10 13:50:07 2017  by altaylor
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_ITkOccupancyCLIDDB_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_ITkOccupancyCLIDDB_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_ITkOccupancyCLIDDB

ITkOccupancy_tag = $(tag)

#cmt_local_tagfile_ITkOccupancyCLIDDB = $(ITkOccupancy_tag)_ITkOccupancyCLIDDB.make
cmt_local_tagfile_ITkOccupancyCLIDDB = $(bin)$(ITkOccupancy_tag)_ITkOccupancyCLIDDB.make

else

tags      = $(tag),$(CMTEXTRATAGS)

ITkOccupancy_tag = $(tag)

#cmt_local_tagfile_ITkOccupancyCLIDDB = $(ITkOccupancy_tag).make
cmt_local_tagfile_ITkOccupancyCLIDDB = $(bin)$(ITkOccupancy_tag).make

endif

include $(cmt_local_tagfile_ITkOccupancyCLIDDB)
#-include $(cmt_local_tagfile_ITkOccupancyCLIDDB)

ifdef cmt_ITkOccupancyCLIDDB_has_target_tag

cmt_final_setup_ITkOccupancyCLIDDB = $(bin)setup_ITkOccupancyCLIDDB.make
cmt_dependencies_in_ITkOccupancyCLIDDB = $(bin)dependencies_ITkOccupancyCLIDDB.in
#cmt_final_setup_ITkOccupancyCLIDDB = $(bin)ITkOccupancy_ITkOccupancyCLIDDBsetup.make
cmt_local_ITkOccupancyCLIDDB_makefile = $(bin)ITkOccupancyCLIDDB.make

else

cmt_final_setup_ITkOccupancyCLIDDB = $(bin)setup.make
cmt_dependencies_in_ITkOccupancyCLIDDB = $(bin)dependencies.in
#cmt_final_setup_ITkOccupancyCLIDDB = $(bin)ITkOccupancysetup.make
cmt_local_ITkOccupancyCLIDDB_makefile = $(bin)ITkOccupancyCLIDDB.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)ITkOccupancysetup.make

#ITkOccupancyCLIDDB :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'ITkOccupancyCLIDDB'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = ITkOccupancyCLIDDB/
#ITkOccupancyCLIDDB::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/genCLIDDB_header
# Author: Paolo Calafiura
# derived from genconf_header

# Use genCLIDDB_cmd to create package clid.db files

.PHONY: ITkOccupancyCLIDDB ITkOccupancyCLIDDBclean

outname := clid.db
cliddb  := ITkOccupancy_$(outname)
instdir := $(CMTINSTALLAREA)/share
result  := $(instdir)/$(cliddb)
product := $(instdir)/$(outname)
conflib := $(bin)$(library_prefix)ITkOccupancy.$(shlibsuffix)

ITkOccupancyCLIDDB :: $(result)

$(instdir) :
	$(mkdir) -p $(instdir)

$(result) : $(conflib) $(product)
	@$(genCLIDDB_cmd) -p ITkOccupancy -i$(product) -o $(result)

$(product) : $(instdir)
	touch $(product)

ITkOccupancyCLIDDBclean ::
	$(cleanup_silent) $(uninstall_command) $(product) $(result)
	$(cleanup_silent) $(cmt_uninstallarea_command) $(product) $(result)

#-- start of cleanup_header --------------

clean :: ITkOccupancyCLIDDBclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(ITkOccupancyCLIDDB.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

ITkOccupancyCLIDDBclean ::
#-- end of cleanup_header ---------------
