
import AthenaPoolCnvSvc.ReadAthenaPool

from AthenaCommon.AppMgr import theApp

from AthenaCommon.AppMgr import ServiceMgr as svcMgr

# list of files to analyse
#FNAMES=[
#    "root://eosatlas.cern.ch//eos/atlas/atlasgroupdisk/perf-idtracking/dq2/rucio/mc15_14TeV/b1/9d/DAOD_IDTRKVALID.11169403._000001.pool.root.1",
#    "root://eosatlas.cern.ch//eos/atlas/atlasgroupdisk/perf-idtracking/dq2/rucio/mc15_14TeV/27/51/DAOD_IDTRKVALID.11169403._000002.pool.root.1"
#]

# analyse files in ascii list samples.txt, format: one sample per line
with open('samples.txt','r') as inSamp:
   FNAMES = [line.rstrip('\n') for line in inSamp]
svcMgr.EventSelector.InputCollections = FNAMES    

algseq = CfgMgr.AthSequencer("AthAlgSeq") 

svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += ["MYSTREAM DATAFILE='out.root' OPT='RECREATE'"]

algseq += CfgMgr.avgPixelOccupancy(incExt="inc")

theApp.EvtMax=100
#theApp.EvtMax=-1

