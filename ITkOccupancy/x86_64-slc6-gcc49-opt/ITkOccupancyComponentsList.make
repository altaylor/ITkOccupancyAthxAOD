#-- start of make_header -----------------

#====================================
#  Document ITkOccupancyComponentsList
#
#   Generated Mon Apr 10 13:50:07 2017  by altaylor
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_ITkOccupancyComponentsList_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_ITkOccupancyComponentsList_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_ITkOccupancyComponentsList

ITkOccupancy_tag = $(tag)

#cmt_local_tagfile_ITkOccupancyComponentsList = $(ITkOccupancy_tag)_ITkOccupancyComponentsList.make
cmt_local_tagfile_ITkOccupancyComponentsList = $(bin)$(ITkOccupancy_tag)_ITkOccupancyComponentsList.make

else

tags      = $(tag),$(CMTEXTRATAGS)

ITkOccupancy_tag = $(tag)

#cmt_local_tagfile_ITkOccupancyComponentsList = $(ITkOccupancy_tag).make
cmt_local_tagfile_ITkOccupancyComponentsList = $(bin)$(ITkOccupancy_tag).make

endif

include $(cmt_local_tagfile_ITkOccupancyComponentsList)
#-include $(cmt_local_tagfile_ITkOccupancyComponentsList)

ifdef cmt_ITkOccupancyComponentsList_has_target_tag

cmt_final_setup_ITkOccupancyComponentsList = $(bin)setup_ITkOccupancyComponentsList.make
cmt_dependencies_in_ITkOccupancyComponentsList = $(bin)dependencies_ITkOccupancyComponentsList.in
#cmt_final_setup_ITkOccupancyComponentsList = $(bin)ITkOccupancy_ITkOccupancyComponentsListsetup.make
cmt_local_ITkOccupancyComponentsList_makefile = $(bin)ITkOccupancyComponentsList.make

else

cmt_final_setup_ITkOccupancyComponentsList = $(bin)setup.make
cmt_dependencies_in_ITkOccupancyComponentsList = $(bin)dependencies.in
#cmt_final_setup_ITkOccupancyComponentsList = $(bin)ITkOccupancysetup.make
cmt_local_ITkOccupancyComponentsList_makefile = $(bin)ITkOccupancyComponentsList.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)ITkOccupancysetup.make

#ITkOccupancyComponentsList :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'ITkOccupancyComponentsList'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = ITkOccupancyComponentsList/
#ITkOccupancyComponentsList::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
##
componentslistfile = ITkOccupancy.components
COMPONENTSLIST_DIR = ../$(tag)
fulllibname = libITkOccupancy.$(shlibsuffix)

ITkOccupancyComponentsList :: ${COMPONENTSLIST_DIR}/$(componentslistfile)
	@:

${COMPONENTSLIST_DIR}/$(componentslistfile) :: $(bin)$(fulllibname)
	@echo 'Generating componentslist file for $(fulllibname)'
	cd ../$(tag);$(listcomponents_cmd) --output ${COMPONENTSLIST_DIR}/$(componentslistfile) $(fulllibname)

install :: ITkOccupancyComponentsListinstall
ITkOccupancyComponentsListinstall :: ITkOccupancyComponentsList

uninstall :: ITkOccupancyComponentsListuninstall
ITkOccupancyComponentsListuninstall :: ITkOccupancyComponentsListclean

ITkOccupancyComponentsListclean ::
	@echo 'Deleting $(componentslistfile)'
	@rm -f ${COMPONENTSLIST_DIR}/$(componentslistfile)

#-- start of cleanup_header --------------

clean :: ITkOccupancyComponentsListclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(ITkOccupancyComponentsList.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

ITkOccupancyComponentsListclean ::
#-- end of cleanup_header ---------------
