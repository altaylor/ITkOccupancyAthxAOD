#-- start of make_header -----------------

#====================================
#  Library ITkOccupancy
#
#   Generated Mon Apr 10 13:49:46 2017  by altaylor
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_ITkOccupancy_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_ITkOccupancy_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_ITkOccupancy

ITkOccupancy_tag = $(tag)

#cmt_local_tagfile_ITkOccupancy = $(ITkOccupancy_tag)_ITkOccupancy.make
cmt_local_tagfile_ITkOccupancy = $(bin)$(ITkOccupancy_tag)_ITkOccupancy.make

else

tags      = $(tag),$(CMTEXTRATAGS)

ITkOccupancy_tag = $(tag)

#cmt_local_tagfile_ITkOccupancy = $(ITkOccupancy_tag).make
cmt_local_tagfile_ITkOccupancy = $(bin)$(ITkOccupancy_tag).make

endif

include $(cmt_local_tagfile_ITkOccupancy)
#-include $(cmt_local_tagfile_ITkOccupancy)

ifdef cmt_ITkOccupancy_has_target_tag

cmt_final_setup_ITkOccupancy = $(bin)setup_ITkOccupancy.make
cmt_dependencies_in_ITkOccupancy = $(bin)dependencies_ITkOccupancy.in
#cmt_final_setup_ITkOccupancy = $(bin)ITkOccupancy_ITkOccupancysetup.make
cmt_local_ITkOccupancy_makefile = $(bin)ITkOccupancy.make

else

cmt_final_setup_ITkOccupancy = $(bin)setup.make
cmt_dependencies_in_ITkOccupancy = $(bin)dependencies.in
#cmt_final_setup_ITkOccupancy = $(bin)ITkOccupancysetup.make
cmt_local_ITkOccupancy_makefile = $(bin)ITkOccupancy.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)ITkOccupancysetup.make

#ITkOccupancy :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'ITkOccupancy'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = ITkOccupancy/
#ITkOccupancy::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

ITkOccupancylibname   = $(bin)$(library_prefix)ITkOccupancy$(library_suffix)
ITkOccupancylib       = $(ITkOccupancylibname).a
ITkOccupancystamp     = $(bin)ITkOccupancy.stamp
ITkOccupancyshstamp   = $(bin)ITkOccupancy.shstamp

ITkOccupancy :: dirs  ITkOccupancyLIB
	$(echo) "ITkOccupancy ok"

#-- end of libary_header ----------------
#-- start of library_no_static ------

#ITkOccupancyLIB :: $(ITkOccupancylib) $(ITkOccupancyshstamp)
ITkOccupancyLIB :: $(ITkOccupancyshstamp)
	$(echo) "ITkOccupancy : library ok"

$(ITkOccupancylib) :: $(bin)Counters.o $(bin)ITkOccupancyAlg.o $(bin)PerEventPixelsRegOcc.o $(bin)avgPixelOccupancy.o $(bin)avgStripOccupancy.o $(bin)MaxStripStudy.o $(bin)MaxStripTrackOccStudy.o $(bin)ITkOccupancy_entries.o $(bin)ITkOccupancy_load.o
	$(lib_echo) "static library $@"
	$(lib_silent) cd $(bin); \
	  $(ar) $(ITkOccupancylib) $?
	$(lib_silent) $(ranlib) $(ITkOccupancylib)
	$(lib_silent) cat /dev/null >$(ITkOccupancystamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

#
# We add one level of dependency upon the true shared library 
# (rather than simply upon the stamp file)
# this is for cases where the shared library has not been built
# while the stamp was created (error??) 
#

$(ITkOccupancylibname).$(shlibsuffix) :: $(bin)Counters.o $(bin)ITkOccupancyAlg.o $(bin)PerEventPixelsRegOcc.o $(bin)avgPixelOccupancy.o $(bin)avgStripOccupancy.o $(bin)MaxStripStudy.o $(bin)MaxStripTrackOccStudy.o $(bin)ITkOccupancy_entries.o $(bin)ITkOccupancy_load.o $(use_requirements) $(ITkOccupancystamps)
	$(lib_echo) "shared library $@"
	$(lib_silent) $(shlibbuilder) $(shlibflags) -o $@ $(bin)Counters.o $(bin)ITkOccupancyAlg.o $(bin)PerEventPixelsRegOcc.o $(bin)avgPixelOccupancy.o $(bin)avgStripOccupancy.o $(bin)MaxStripStudy.o $(bin)MaxStripTrackOccStudy.o $(bin)ITkOccupancy_entries.o $(bin)ITkOccupancy_load.o $(ITkOccupancy_shlibflags)
	$(lib_silent) cat /dev/null >$(ITkOccupancystamp) && \
	  cat /dev/null >$(ITkOccupancyshstamp)

$(ITkOccupancyshstamp) :: $(ITkOccupancylibname).$(shlibsuffix)
	$(lib_silent) if test -f $(ITkOccupancylibname).$(shlibsuffix) ; then \
	  cat /dev/null >$(ITkOccupancystamp) && \
	  cat /dev/null >$(ITkOccupancyshstamp) ; fi

ITkOccupancyclean ::
	$(cleanup_echo) objects ITkOccupancy
	$(cleanup_silent) /bin/rm -f $(bin)Counters.o $(bin)ITkOccupancyAlg.o $(bin)PerEventPixelsRegOcc.o $(bin)avgPixelOccupancy.o $(bin)avgStripOccupancy.o $(bin)MaxStripStudy.o $(bin)MaxStripTrackOccStudy.o $(bin)ITkOccupancy_entries.o $(bin)ITkOccupancy_load.o
	$(cleanup_silent) /bin/rm -f $(patsubst %.o,%.d,$(bin)Counters.o $(bin)ITkOccupancyAlg.o $(bin)PerEventPixelsRegOcc.o $(bin)avgPixelOccupancy.o $(bin)avgStripOccupancy.o $(bin)MaxStripStudy.o $(bin)MaxStripTrackOccStudy.o $(bin)ITkOccupancy_entries.o $(bin)ITkOccupancy_load.o) $(patsubst %.o,%.dep,$(bin)Counters.o $(bin)ITkOccupancyAlg.o $(bin)PerEventPixelsRegOcc.o $(bin)avgPixelOccupancy.o $(bin)avgStripOccupancy.o $(bin)MaxStripStudy.o $(bin)MaxStripTrackOccStudy.o $(bin)ITkOccupancy_entries.o $(bin)ITkOccupancy_load.o) $(patsubst %.o,%.d.stamp,$(bin)Counters.o $(bin)ITkOccupancyAlg.o $(bin)PerEventPixelsRegOcc.o $(bin)avgPixelOccupancy.o $(bin)avgStripOccupancy.o $(bin)MaxStripStudy.o $(bin)MaxStripTrackOccStudy.o $(bin)ITkOccupancy_entries.o $(bin)ITkOccupancy_load.o)
	$(cleanup_silent) cd $(bin); /bin/rm -rf ITkOccupancy_deps ITkOccupancy_dependencies.make

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

install_dir = ${CMTINSTALLAREA}/$(tag)/lib
ITkOccupancyinstallname = $(library_prefix)ITkOccupancy$(library_suffix).$(shlibsuffix)

ITkOccupancy :: ITkOccupancyinstall ;

install :: ITkOccupancyinstall ;

ITkOccupancyinstall :: $(install_dir)/$(ITkOccupancyinstallname)
ifdef CMTINSTALLAREA
	$(echo) "installation done"
endif

$(install_dir)/$(ITkOccupancyinstallname) :: $(bin)$(ITkOccupancyinstallname)
ifdef CMTINSTALLAREA
	$(install_silent) $(cmt_install_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(ITkOccupancyinstallname)" \
	    -out "$(install_dir)" \
	    -cmd "$(cmt_installarea_command)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

##ITkOccupancyclean :: ITkOccupancyuninstall

uninstall :: ITkOccupancyuninstall ;

ITkOccupancyuninstall ::
ifdef CMTINSTALLAREA
	$(cleanup_silent) $(cmt_uninstall_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(ITkOccupancyinstallname)" \
	    -out "$(install_dir)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

#-- end of library_no_static ------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),ITkOccupancyclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)Counters.d

$(bin)$(binobj)Counters.d :

$(bin)$(binobj)Counters.o : $(cmt_final_setup_ITkOccupancy)

$(bin)$(binobj)Counters.o : $(src)Counters.cxx
	$(cpp_echo) $(src)Counters.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(ITkOccupancy_pp_cppflags) $(lib_ITkOccupancy_pp_cppflags) $(Counters_pp_cppflags) $(use_cppflags) $(ITkOccupancy_cppflags) $(lib_ITkOccupancy_cppflags) $(Counters_cppflags) $(Counters_cxx_cppflags)  $(src)Counters.cxx
endif
endif

else
$(bin)ITkOccupancy_dependencies.make : $(Counters_cxx_dependencies)

$(bin)ITkOccupancy_dependencies.make : $(src)Counters.cxx

$(bin)$(binobj)Counters.o : $(Counters_cxx_dependencies)
	$(cpp_echo) $(src)Counters.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(ITkOccupancy_pp_cppflags) $(lib_ITkOccupancy_pp_cppflags) $(Counters_pp_cppflags) $(use_cppflags) $(ITkOccupancy_cppflags) $(lib_ITkOccupancy_cppflags) $(Counters_cppflags) $(Counters_cxx_cppflags)  $(src)Counters.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),ITkOccupancyclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)ITkOccupancyAlg.d

$(bin)$(binobj)ITkOccupancyAlg.d :

$(bin)$(binobj)ITkOccupancyAlg.o : $(cmt_final_setup_ITkOccupancy)

$(bin)$(binobj)ITkOccupancyAlg.o : $(src)ITkOccupancyAlg.cxx
	$(cpp_echo) $(src)ITkOccupancyAlg.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(ITkOccupancy_pp_cppflags) $(lib_ITkOccupancy_pp_cppflags) $(ITkOccupancyAlg_pp_cppflags) $(use_cppflags) $(ITkOccupancy_cppflags) $(lib_ITkOccupancy_cppflags) $(ITkOccupancyAlg_cppflags) $(ITkOccupancyAlg_cxx_cppflags)  $(src)ITkOccupancyAlg.cxx
endif
endif

else
$(bin)ITkOccupancy_dependencies.make : $(ITkOccupancyAlg_cxx_dependencies)

$(bin)ITkOccupancy_dependencies.make : $(src)ITkOccupancyAlg.cxx

$(bin)$(binobj)ITkOccupancyAlg.o : $(ITkOccupancyAlg_cxx_dependencies)
	$(cpp_echo) $(src)ITkOccupancyAlg.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(ITkOccupancy_pp_cppflags) $(lib_ITkOccupancy_pp_cppflags) $(ITkOccupancyAlg_pp_cppflags) $(use_cppflags) $(ITkOccupancy_cppflags) $(lib_ITkOccupancy_cppflags) $(ITkOccupancyAlg_cppflags) $(ITkOccupancyAlg_cxx_cppflags)  $(src)ITkOccupancyAlg.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),ITkOccupancyclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)PerEventPixelsRegOcc.d

$(bin)$(binobj)PerEventPixelsRegOcc.d :

$(bin)$(binobj)PerEventPixelsRegOcc.o : $(cmt_final_setup_ITkOccupancy)

$(bin)$(binobj)PerEventPixelsRegOcc.o : $(src)PerEventPixelsRegOcc.cxx
	$(cpp_echo) $(src)PerEventPixelsRegOcc.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(ITkOccupancy_pp_cppflags) $(lib_ITkOccupancy_pp_cppflags) $(PerEventPixelsRegOcc_pp_cppflags) $(use_cppflags) $(ITkOccupancy_cppflags) $(lib_ITkOccupancy_cppflags) $(PerEventPixelsRegOcc_cppflags) $(PerEventPixelsRegOcc_cxx_cppflags)  $(src)PerEventPixelsRegOcc.cxx
endif
endif

else
$(bin)ITkOccupancy_dependencies.make : $(PerEventPixelsRegOcc_cxx_dependencies)

$(bin)ITkOccupancy_dependencies.make : $(src)PerEventPixelsRegOcc.cxx

$(bin)$(binobj)PerEventPixelsRegOcc.o : $(PerEventPixelsRegOcc_cxx_dependencies)
	$(cpp_echo) $(src)PerEventPixelsRegOcc.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(ITkOccupancy_pp_cppflags) $(lib_ITkOccupancy_pp_cppflags) $(PerEventPixelsRegOcc_pp_cppflags) $(use_cppflags) $(ITkOccupancy_cppflags) $(lib_ITkOccupancy_cppflags) $(PerEventPixelsRegOcc_cppflags) $(PerEventPixelsRegOcc_cxx_cppflags)  $(src)PerEventPixelsRegOcc.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),ITkOccupancyclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)avgPixelOccupancy.d

$(bin)$(binobj)avgPixelOccupancy.d :

$(bin)$(binobj)avgPixelOccupancy.o : $(cmt_final_setup_ITkOccupancy)

$(bin)$(binobj)avgPixelOccupancy.o : $(src)avgPixelOccupancy.cxx
	$(cpp_echo) $(src)avgPixelOccupancy.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(ITkOccupancy_pp_cppflags) $(lib_ITkOccupancy_pp_cppflags) $(avgPixelOccupancy_pp_cppflags) $(use_cppflags) $(ITkOccupancy_cppflags) $(lib_ITkOccupancy_cppflags) $(avgPixelOccupancy_cppflags) $(avgPixelOccupancy_cxx_cppflags)  $(src)avgPixelOccupancy.cxx
endif
endif

else
$(bin)ITkOccupancy_dependencies.make : $(avgPixelOccupancy_cxx_dependencies)

$(bin)ITkOccupancy_dependencies.make : $(src)avgPixelOccupancy.cxx

$(bin)$(binobj)avgPixelOccupancy.o : $(avgPixelOccupancy_cxx_dependencies)
	$(cpp_echo) $(src)avgPixelOccupancy.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(ITkOccupancy_pp_cppflags) $(lib_ITkOccupancy_pp_cppflags) $(avgPixelOccupancy_pp_cppflags) $(use_cppflags) $(ITkOccupancy_cppflags) $(lib_ITkOccupancy_cppflags) $(avgPixelOccupancy_cppflags) $(avgPixelOccupancy_cxx_cppflags)  $(src)avgPixelOccupancy.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),ITkOccupancyclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)avgStripOccupancy.d

$(bin)$(binobj)avgStripOccupancy.d :

$(bin)$(binobj)avgStripOccupancy.o : $(cmt_final_setup_ITkOccupancy)

$(bin)$(binobj)avgStripOccupancy.o : $(src)avgStripOccupancy.cxx
	$(cpp_echo) $(src)avgStripOccupancy.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(ITkOccupancy_pp_cppflags) $(lib_ITkOccupancy_pp_cppflags) $(avgStripOccupancy_pp_cppflags) $(use_cppflags) $(ITkOccupancy_cppflags) $(lib_ITkOccupancy_cppflags) $(avgStripOccupancy_cppflags) $(avgStripOccupancy_cxx_cppflags)  $(src)avgStripOccupancy.cxx
endif
endif

else
$(bin)ITkOccupancy_dependencies.make : $(avgStripOccupancy_cxx_dependencies)

$(bin)ITkOccupancy_dependencies.make : $(src)avgStripOccupancy.cxx

$(bin)$(binobj)avgStripOccupancy.o : $(avgStripOccupancy_cxx_dependencies)
	$(cpp_echo) $(src)avgStripOccupancy.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(ITkOccupancy_pp_cppflags) $(lib_ITkOccupancy_pp_cppflags) $(avgStripOccupancy_pp_cppflags) $(use_cppflags) $(ITkOccupancy_cppflags) $(lib_ITkOccupancy_cppflags) $(avgStripOccupancy_cppflags) $(avgStripOccupancy_cxx_cppflags)  $(src)avgStripOccupancy.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),ITkOccupancyclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)MaxStripStudy.d

$(bin)$(binobj)MaxStripStudy.d :

$(bin)$(binobj)MaxStripStudy.o : $(cmt_final_setup_ITkOccupancy)

$(bin)$(binobj)MaxStripStudy.o : $(src)MaxStripStudy.cxx
	$(cpp_echo) $(src)MaxStripStudy.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(ITkOccupancy_pp_cppflags) $(lib_ITkOccupancy_pp_cppflags) $(MaxStripStudy_pp_cppflags) $(use_cppflags) $(ITkOccupancy_cppflags) $(lib_ITkOccupancy_cppflags) $(MaxStripStudy_cppflags) $(MaxStripStudy_cxx_cppflags)  $(src)MaxStripStudy.cxx
endif
endif

else
$(bin)ITkOccupancy_dependencies.make : $(MaxStripStudy_cxx_dependencies)

$(bin)ITkOccupancy_dependencies.make : $(src)MaxStripStudy.cxx

$(bin)$(binobj)MaxStripStudy.o : $(MaxStripStudy_cxx_dependencies)
	$(cpp_echo) $(src)MaxStripStudy.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(ITkOccupancy_pp_cppflags) $(lib_ITkOccupancy_pp_cppflags) $(MaxStripStudy_pp_cppflags) $(use_cppflags) $(ITkOccupancy_cppflags) $(lib_ITkOccupancy_cppflags) $(MaxStripStudy_cppflags) $(MaxStripStudy_cxx_cppflags)  $(src)MaxStripStudy.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),ITkOccupancyclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)MaxStripTrackOccStudy.d

$(bin)$(binobj)MaxStripTrackOccStudy.d :

$(bin)$(binobj)MaxStripTrackOccStudy.o : $(cmt_final_setup_ITkOccupancy)

$(bin)$(binobj)MaxStripTrackOccStudy.o : $(src)MaxStripTrackOccStudy.cxx
	$(cpp_echo) $(src)MaxStripTrackOccStudy.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(ITkOccupancy_pp_cppflags) $(lib_ITkOccupancy_pp_cppflags) $(MaxStripTrackOccStudy_pp_cppflags) $(use_cppflags) $(ITkOccupancy_cppflags) $(lib_ITkOccupancy_cppflags) $(MaxStripTrackOccStudy_cppflags) $(MaxStripTrackOccStudy_cxx_cppflags)  $(src)MaxStripTrackOccStudy.cxx
endif
endif

else
$(bin)ITkOccupancy_dependencies.make : $(MaxStripTrackOccStudy_cxx_dependencies)

$(bin)ITkOccupancy_dependencies.make : $(src)MaxStripTrackOccStudy.cxx

$(bin)$(binobj)MaxStripTrackOccStudy.o : $(MaxStripTrackOccStudy_cxx_dependencies)
	$(cpp_echo) $(src)MaxStripTrackOccStudy.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(ITkOccupancy_pp_cppflags) $(lib_ITkOccupancy_pp_cppflags) $(MaxStripTrackOccStudy_pp_cppflags) $(use_cppflags) $(ITkOccupancy_cppflags) $(lib_ITkOccupancy_cppflags) $(MaxStripTrackOccStudy_cppflags) $(MaxStripTrackOccStudy_cxx_cppflags)  $(src)MaxStripTrackOccStudy.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),ITkOccupancyclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)ITkOccupancy_entries.d

$(bin)$(binobj)ITkOccupancy_entries.d :

$(bin)$(binobj)ITkOccupancy_entries.o : $(cmt_final_setup_ITkOccupancy)

$(bin)$(binobj)ITkOccupancy_entries.o : $(src)components/ITkOccupancy_entries.cxx
	$(cpp_echo) $(src)components/ITkOccupancy_entries.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(ITkOccupancy_pp_cppflags) $(lib_ITkOccupancy_pp_cppflags) $(ITkOccupancy_entries_pp_cppflags) $(use_cppflags) $(ITkOccupancy_cppflags) $(lib_ITkOccupancy_cppflags) $(ITkOccupancy_entries_cppflags) $(ITkOccupancy_entries_cxx_cppflags) -I../src/components $(src)components/ITkOccupancy_entries.cxx
endif
endif

else
$(bin)ITkOccupancy_dependencies.make : $(ITkOccupancy_entries_cxx_dependencies)

$(bin)ITkOccupancy_dependencies.make : $(src)components/ITkOccupancy_entries.cxx

$(bin)$(binobj)ITkOccupancy_entries.o : $(ITkOccupancy_entries_cxx_dependencies)
	$(cpp_echo) $(src)components/ITkOccupancy_entries.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(ITkOccupancy_pp_cppflags) $(lib_ITkOccupancy_pp_cppflags) $(ITkOccupancy_entries_pp_cppflags) $(use_cppflags) $(ITkOccupancy_cppflags) $(lib_ITkOccupancy_cppflags) $(ITkOccupancy_entries_cppflags) $(ITkOccupancy_entries_cxx_cppflags) -I../src/components $(src)components/ITkOccupancy_entries.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),ITkOccupancyclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)ITkOccupancy_load.d

$(bin)$(binobj)ITkOccupancy_load.d :

$(bin)$(binobj)ITkOccupancy_load.o : $(cmt_final_setup_ITkOccupancy)

$(bin)$(binobj)ITkOccupancy_load.o : $(src)components/ITkOccupancy_load.cxx
	$(cpp_echo) $(src)components/ITkOccupancy_load.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(ITkOccupancy_pp_cppflags) $(lib_ITkOccupancy_pp_cppflags) $(ITkOccupancy_load_pp_cppflags) $(use_cppflags) $(ITkOccupancy_cppflags) $(lib_ITkOccupancy_cppflags) $(ITkOccupancy_load_cppflags) $(ITkOccupancy_load_cxx_cppflags) -I../src/components $(src)components/ITkOccupancy_load.cxx
endif
endif

else
$(bin)ITkOccupancy_dependencies.make : $(ITkOccupancy_load_cxx_dependencies)

$(bin)ITkOccupancy_dependencies.make : $(src)components/ITkOccupancy_load.cxx

$(bin)$(binobj)ITkOccupancy_load.o : $(ITkOccupancy_load_cxx_dependencies)
	$(cpp_echo) $(src)components/ITkOccupancy_load.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(ITkOccupancy_pp_cppflags) $(lib_ITkOccupancy_pp_cppflags) $(ITkOccupancy_load_pp_cppflags) $(use_cppflags) $(ITkOccupancy_cppflags) $(lib_ITkOccupancy_cppflags) $(ITkOccupancy_load_cppflags) $(ITkOccupancy_load_cxx_cppflags) -I../src/components $(src)components/ITkOccupancy_load.cxx

endif

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: ITkOccupancyclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(ITkOccupancy.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

ITkOccupancyclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library ITkOccupancy
	-$(cleanup_silent) cd $(bin) && \rm -f $(library_prefix)ITkOccupancy$(library_suffix).a $(library_prefix)ITkOccupancy$(library_suffix).$(shlibsuffix) ITkOccupancy.stamp ITkOccupancy.shstamp
#-- end of cleanup_library ---------------
