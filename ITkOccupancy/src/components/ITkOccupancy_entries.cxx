
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../ITkOccupancyAlg.h"

DECLARE_ALGORITHM_FACTORY( ITkOccupancyAlg )


#include "../PerEventPixelsRegOcc.h"
DECLARE_ALGORITHM_FACTORY( PerEventPixelsRegOcc )


#include "../avgPixelOccupancy.h"
DECLARE_ALGORITHM_FACTORY( avgPixelOccupancy )


#include "../avgStripOccupancy.h"
DECLARE_ALGORITHM_FACTORY( avgStripOccupancy )


#include "../MaxStripStudy.h"
DECLARE_ALGORITHM_FACTORY( MaxStripStudy )


#include "../MaxStripTrackOccStudy.h"
DECLARE_ALGORITHM_FACTORY( MaxStripTrackOccStudy )

DECLARE_FACTORY_ENTRIES( ITkOccupancy ) 
{
  DECLARE_ALGORITHM( MaxStripTrackOccStudy );
  DECLARE_ALGORITHM( MaxStripStudy );
  DECLARE_ALGORITHM( avgStripOccupancy );
  DECLARE_ALGORITHM( avgPixelOccupancy );
  DECLARE_ALGORITHM( PerEventPixelsRegOcc );
  DECLARE_ALGORITHM( ITkOccupancyAlg );
}
