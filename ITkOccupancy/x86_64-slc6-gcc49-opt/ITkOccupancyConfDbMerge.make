#-- start of make_header -----------------

#====================================
#  Document ITkOccupancyConfDbMerge
#
#   Generated Mon Apr 10 13:50:07 2017  by altaylor
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_ITkOccupancyConfDbMerge_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_ITkOccupancyConfDbMerge_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_ITkOccupancyConfDbMerge

ITkOccupancy_tag = $(tag)

#cmt_local_tagfile_ITkOccupancyConfDbMerge = $(ITkOccupancy_tag)_ITkOccupancyConfDbMerge.make
cmt_local_tagfile_ITkOccupancyConfDbMerge = $(bin)$(ITkOccupancy_tag)_ITkOccupancyConfDbMerge.make

else

tags      = $(tag),$(CMTEXTRATAGS)

ITkOccupancy_tag = $(tag)

#cmt_local_tagfile_ITkOccupancyConfDbMerge = $(ITkOccupancy_tag).make
cmt_local_tagfile_ITkOccupancyConfDbMerge = $(bin)$(ITkOccupancy_tag).make

endif

include $(cmt_local_tagfile_ITkOccupancyConfDbMerge)
#-include $(cmt_local_tagfile_ITkOccupancyConfDbMerge)

ifdef cmt_ITkOccupancyConfDbMerge_has_target_tag

cmt_final_setup_ITkOccupancyConfDbMerge = $(bin)setup_ITkOccupancyConfDbMerge.make
cmt_dependencies_in_ITkOccupancyConfDbMerge = $(bin)dependencies_ITkOccupancyConfDbMerge.in
#cmt_final_setup_ITkOccupancyConfDbMerge = $(bin)ITkOccupancy_ITkOccupancyConfDbMergesetup.make
cmt_local_ITkOccupancyConfDbMerge_makefile = $(bin)ITkOccupancyConfDbMerge.make

else

cmt_final_setup_ITkOccupancyConfDbMerge = $(bin)setup.make
cmt_dependencies_in_ITkOccupancyConfDbMerge = $(bin)dependencies.in
#cmt_final_setup_ITkOccupancyConfDbMerge = $(bin)ITkOccupancysetup.make
cmt_local_ITkOccupancyConfDbMerge_makefile = $(bin)ITkOccupancyConfDbMerge.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)ITkOccupancysetup.make

#ITkOccupancyConfDbMerge :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'ITkOccupancyConfDbMerge'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = ITkOccupancyConfDbMerge/
#ITkOccupancyConfDbMerge::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/merge_genconfDb_header
# Author: Sebastien Binet (binet@cern.ch)

# Makefile fragment to merge a <library>.confdb file into a single
# <project>.confdb file in the (lib) install area

.PHONY: ITkOccupancyConfDbMerge ITkOccupancyConfDbMergeclean

# default is already '#'
#genconfDb_comment_char := "'#'"

instdir      := ${CMTINSTALLAREA}/$(tag)
confDbRef    := /afs/cern.ch/work/a/altaylor/git_test/ITkOccupancyAthxAOD/ITkOccupancy/genConf/ITkOccupancy/ITkOccupancy.confdb
stampConfDb  := $(confDbRef).stamp
mergedConfDb := $(instdir)/lib/$(project).confdb

ITkOccupancyConfDbMerge :: $(stampConfDb) $(mergedConfDb)
	@:

.NOTPARALLEL : $(stampConfDb) $(mergedConfDb)

$(stampConfDb) $(mergedConfDb) :: $(confDbRef)
	@echo "Running merge_genconfDb  ITkOccupancyConfDbMerge"
	$(merge_genconfDb_cmd) \
          --do-merge \
          --input-file $(confDbRef) \
          --merged-file $(mergedConfDb)

ITkOccupancyConfDbMergeclean ::
	$(cleanup_silent) $(merge_genconfDb_cmd) \
          --un-merge \
          --input-file $(confDbRef) \
          --merged-file $(mergedConfDb)	;
	$(cleanup_silent) $(remove_command) $(stampConfDb)
libITkOccupancy_so_dependencies = ../x86_64-slc6-gcc49-opt/libITkOccupancy.so
#-- start of cleanup_header --------------

clean :: ITkOccupancyConfDbMergeclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(ITkOccupancyConfDbMerge.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

ITkOccupancyConfDbMergeclean ::
#-- end of cleanup_header ---------------
