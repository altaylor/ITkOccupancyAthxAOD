#ifndef MYPACKAGE_AVGPIXELOCCUPANCY_H
#define MYPACKAGE_AVGPIXELOCCUPANCY_H 1

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h" //included under assumption you'll want to use some tools! Remove if you don't!
#include "TTree.h"
#include "TGraph.h"
#include "TH1D.h"
#include "TH2D.h"
#include "Counters.h"

using namespace std;



class avgPixelOccupancy: public ::AthAlgorithm { 
 public: 
  avgPixelOccupancy( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~avgPixelOccupancy(); 

  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();

 private:

  TTree *myTree; //!
  TH1D *myHist; //!

  TH2D *pix_disk_occ_map2D; //!
  TH2D *pix_disk_occ_map2D_norm; //!
  TH2D *pix_disk_hitmap2D; //!
  TH2D *pix_disk_hitmap2D_norm; //!

  // also register with smaller binning also

  TH2D *pix_disk_occ_map2DV2; //!
  TH2D *pix_disk_occ_map2DV2_norm; //!
  TH2D *pix_disk_hitmap2DV2; //!
  TH2D *pix_disk_hitmap2DV2_norm; //!

  
  TH2D *pix_barrel_occ_map2D; //!
  TH2D *pix_barrel_occ_map2D_norm; //!
  TH2D *pix_barrel_hitmap2D; //!
  TH2D *pix_barrel_hitmap2D_norm; //!

  // also register with smaller binning also

  TH2D *pix_barrel_occ_map2DV2; //!
  TH2D *pix_barrel_occ_map2DV2_norm; //!
  TH2D *pix_barrel_hitmap2DV2; //!
  TH2D *pix_barrel_hitmap2DV2_norm; //!


  // plots for the barrel

  TGraph *occ_z_B0; //!
  TGraph *occ_z_B1; //!
  TGraph *occ_z_B2; //!
  TGraph *occ_z_B3; //!
  TGraph *occ_z_B4; //!
  
  TGraph *hit_dens_z_B0; //!
  TGraph *hit_dens_z_B1; //!
  TGraph *hit_dens_z_B2; //!
  TGraph *hit_dens_z_B3; //!
  TGraph *hit_dens_z_B4; //!

    
  TGraph *clus_dens_z_B0; //!
  TGraph *clus_dens_z_B1; //!
  TGraph *clus_dens_z_B2; //!
  TGraph *clus_dens_z_B3; //!
  TGraph *clus_dens_z_B4; //!
  
   
  TGraph *clus_size_z_B0; //!
  TGraph *clus_size_z_B1; //!
  TGraph *clus_size_z_B2; //!
  TGraph *clus_size_z_B3; //!
  TGraph *clus_size_z_B4; //!

  // plots for the endcap

   
  TGraph *occ_z_EC0; //!
  TGraph *occ_z_EC1; //!
  TGraph *occ_z_EC2; //!
  TGraph *occ_z_EC3; //!
  TGraph *occ_z_EC4; //!
  
  TGraph *hit_dens_z_EC0; //!
  TGraph *hit_dens_z_EC1; //!
  TGraph *hit_dens_z_EC2; //!
  TGraph *hit_dens_z_EC3; //!
  TGraph *hit_dens_z_EC4; //!

   
  TGraph *clus_dens_z_EC0; //!
  TGraph *clus_dens_z_EC1; //!
  TGraph *clus_dens_z_EC2; //!
  TGraph *clus_dens_z_EC3; //!
  TGraph *clus_dens_z_EC4; //!
  
   
  TGraph *clus_size_z_EC0; //!
  TGraph *clus_size_z_EC1; //!
  TGraph *clus_size_z_EC2; //!
  TGraph *clus_size_z_EC3; //!
  TGraph *clus_size_z_EC4; //!


   /*
  
  TGraph *occ_r_0; //!
  TGraph *occ_r_1; //!
  TGraph *occ_r_2; //!
  TGraph *occ_r_3; //!
  TGraph *occ_r_4; //!
  TGraph *occ_r_5; //!
   
  TGraph *hit_dens_r_0; //!
  TGraph *hit_dens_r_1; //!
  TGraph *hit_dens_r_2; //!
  TGraph *hit_dens_r_3; //!
  TGraph *hit_dens_r_4; //!
  TGraph *hit_dens_r_5; //!

  
  TGraph *clus_size_r_0; //!
  TGraph *clus_size_r_1; //!
  TGraph *clus_size_r_2; //!
  TGraph *clus_size_r_3; //!
  TGraph *clus_size_r_4; //!
  TGraph *clus_size_r_5; //!

   */

  // register 2D occupancy plots & hit maps 




  //Counters tester; //!

  //std::vector<Counters> tester; //!

  std::map<std::tuple<int,int,int>, Counters> pixelBarrel; //!
  std::map<std::tuple<int,int,int>, Counters> pixelEndCap; //!
  

  float nEvents; //
  string _incExt;


  

}; 

#endif //> !MYPACKAGE_AVGSTRIPOCCUPANCY_H
