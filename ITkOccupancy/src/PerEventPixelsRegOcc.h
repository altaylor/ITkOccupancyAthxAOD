#ifndef MYPACKAGE_PEREVENTPIXELSREGOCC_H
#define MYPACKAGE_PEREVENTPIXELSREGOCC_H 1

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h" //included under assumption you'll want to use some tools! Remove if you don't!
#include "TTree.h"
#include "TGraph.h"
#include "TH1F.h"
#include "TH2F.h"
#include "Counters.h"

using namespace std;



class PerEventPixelsRegOcc: public ::AthAlgorithm { 
 public: 
  PerEventPixelsRegOcc( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~PerEventPixelsRegOcc(); 

  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();

 private:

  TTree *myTree; //!
  TH1D *myHist; //!

  float maxHits_chip_0; //!
  float maxClusters_chip_0; //!
  float maxHits_chip_1; //!
  float maxClusters_chip_1; //!
  float maxHits_chip_2; //!
  float maxClusters_chip_2; //!
  float maxHits_chip_3; //!
  float maxClusters_chip_3;//!
  float maxHits_chip_4; //!
  float maxClusters_chip_4;//!

  
  float maxHits_sensor_0; //!
  float maxClusters_sensor_0; //!
  float maxHits_sensor_1; //!
  float maxClusters_sensor_1; //!
  float maxHits_sensor_2; //!
  float maxClusters_sensor_2; //!
  float maxHits_sensor_3; //!
  float maxClusters_sensor_3;//!
  float maxHits_sensor_4; //!
  float maxClusters_sensor_4;//!

  
  //int getChip(int layer, int etaModule, float localX, float localY); //!
  // th1d histograms...

  int getChipInclined(int layer, int inclinedSec, float localX, float localY); //!
  int getChipExtended(int layer, float localX, float localY); //!

  int getChipV2(int eta_pixel, int phi_pixel); //!
  
  
  /* TH1D *h_clus_chip_I; //!
  TH1D *h_hits_chip_I; //!
    
  TH1D *h_clus_chip_NI; //!
  TH1D *h_hits_chip_NI; //!
    
  TH1D *h_clus_sensor_I; //!
  TH1D *h_hits_sensor_I; //!
    
  TH1D *h_clus_sensor_NI; //!
  TH1D *h_hits_sensor_NI; //! */


  //
    
  TH1D *h_clus_chip_0; //!
  TH1D *h_hits_chip_0; //!
  TH1D *h_clus_sensor_0; //!
  TH1D *h_hits_sensor_0; //! 
      
  TH1D *h_clus_chip_1; //!
  TH1D *h_hits_chip_1; //!  
  TH1D *h_clus_sensor_1; //!
  TH1D *h_hits_sensor_1; //!
  
  TH1D *h_clus_chip_2; //!
  TH1D *h_hits_chip_2; //!
  TH1D *h_clus_sensor_2; //!
  TH1D *h_hits_sensor_2; //! 
      
  TH1D *h_clus_chip_3; //!
  TH1D *h_hits_chip_3; //!  
  TH1D *h_clus_sensor_3; //!
  TH1D *h_hits_sensor_3; //!
        
  TH1D *h_clus_chip_4; //!
  TH1D *h_hits_chip_4; //!  
  TH1D *h_clus_sensor_4; //!
  TH1D *h_hits_sensor_4; //!

  TH1D *h_bitsPerChip_0; //!
  TH1D *h_bitsPerChip_1; //!
  TH1D *h_bitsPerChip_2; //!
  TH1D *h_bitsPerChip_3; //!
  TH1D *h_bitsPerChip_4; //!

  
  
  //h_region_occ = new TH1I("h_region_occ","h_region_occ",10,0.5,10.5);
  //histSvc->regHist("/MYSTREAM/h_region_occ",h_region_occ).ignore();

  //TH1I *h_region_occ; 

  
  TH1D *h_region_occ; //!

  
  TH1D *h_region_occ_0_f; //!
  TH1D *h_region_occ_1_f; //!
  TH1D *h_region_occ_2_f; //!
  TH1D *h_region_occ_3_f; //!
  TH1D *h_region_occ_4_f; //!

  TH1D *h_region_occ_0_I; //!
  TH1D *h_region_occ_1_I; //!
  TH1D *h_region_occ_2_I; //!
  TH1D *h_region_occ_3_I; //!
  TH1D *h_region_occ_4_I; //!

  
  TH1D *h_region_occ_0; //!
  TH1D *h_region_occ_1; //!
  TH1D *h_region_occ_2; //!
  TH1D *h_region_occ_3; //!
  TH1D *h_region_occ_4; //!



  TH1D *h_eventCounter; //!
  

  


  




 


  // th1d histograms...
  TH1D *localY_0; //!
  TH1D *localY_0_inclined; //!
  TH1D *localY_gr0; //!
  TH1D *localY_gr0_inclined; //!

  // th1d histograms...
  TH1D *localX_0; //!
  TH1D *localX_0_inclined; //!
  TH1D *localX_gr0; //!
  TH1D *localX_gr0_inclined; //!

 
  TH2D *pix_disk_occ_map2D; //!
  TH2D *pix_disk_occ_map2D_norm; //!
  TH2D *pix_disk_hitmap2D; //!
  TH2D *pix_disk_hitmap2D_norm; //!

  // also register with smaller binning also

  TH2D *pix_disk_occ_map2DV2; //!
  TH2D *pix_disk_occ_map2DV2_norm; //!
  TH2D *pix_disk_hitmap2DV2; //!
  TH2D *pix_disk_hitmap2DV2_norm; //!

  
  TH2D *pix_barrel_occ_map2D; //!
  TH2D *pix_barrel_occ_map2D_norm; //!
  TH2D *pix_barrel_hitmap2D; //!
  TH2D *pix_barrel_hitmap2D_norm; //!

  // also register with smaller binning also

  TH2D *pix_barrel_occ_map2DV2; //!
  TH2D *pix_barrel_occ_map2DV2_norm; //!
  TH2D *pix_barrel_hitmap2DV2; //!
  TH2D *pix_barrel_hitmap2DV2_norm; //!


  // plots for the barrel

  TGraph *occ_z_B0; //!
  TGraph *occ_z_B1; //!
  TGraph *occ_z_B2; //!
  TGraph *occ_z_B3; //!
  TGraph *occ_z_B4; //!
  
  TGraph *hit_dens_z_B0; //!
  TGraph *hit_dens_z_B1; //!
  TGraph *hit_dens_z_B2; //!
  TGraph *hit_dens_z_B3; //!
  TGraph *hit_dens_z_B4; //!

    
  TGraph *clus_dens_z_B0; //!
  TGraph *clus_dens_z_B1; //!
  TGraph *clus_dens_z_B2; //!
  TGraph *clus_dens_z_B3; //!
  TGraph *clus_dens_z_B4; //!
  
   
  TGraph *clus_size_z_B0; //!
  TGraph *clus_size_z_B1; //!
  TGraph *clus_size_z_B2; //!
  TGraph *clus_size_z_B3; //!
  TGraph *clus_size_z_B4; //!

  // plots for the endcap

   
  TGraph *occ_z_EC0; //!
  TGraph *occ_z_EC1; //!
  TGraph *occ_z_EC2; //!
  TGraph *occ_z_EC3; //!
  TGraph *occ_z_EC4; //!
  
  TGraph *hit_dens_z_EC0; //!
  TGraph *hit_dens_z_EC1; //!
  TGraph *hit_dens_z_EC2; //!
  TGraph *hit_dens_z_EC3; //!
  TGraph *hit_dens_z_EC4; //!

   
  TGraph *clus_dens_z_EC0; //!
  TGraph *clus_dens_z_EC1; //!
  TGraph *clus_dens_z_EC2; //!
  TGraph *clus_dens_z_EC3; //!
  TGraph *clus_dens_z_EC4; //!
  
   
  TGraph *clus_size_z_EC0; //!
  TGraph *clus_size_z_EC1; //!
  TGraph *clus_size_z_EC2; //!
  TGraph *clus_size_z_EC3; //!
  TGraph *clus_size_z_EC4; //!

  //Counters tester; //!

  //std::vector<Counters> tester; //!

  std::map<std::tuple<int,int,int>, Counters> pixelBarrel; //!
  std::map<std::tuple<int,int,int>, Counters> pixelEndCap; //!
  

  float nEvents; //!


  

}; 

#endif //> !MYPACKAGE_AVGSTRIPOCCUPANCY_H
