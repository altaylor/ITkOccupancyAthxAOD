#ifndef ITKOCCUPANCY_MAXSTRIPSTUDY_H
#define ITKOCCUPANCY_MAXSTRIPSTUDY_H 1

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h" //included under assumption you'll want to use some tools! Remove if you don't!



class MaxStripStudy: public ::AthAlgorithm { 
 public: 
  MaxStripStudy( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~MaxStripStudy(); 

  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();

 private: 

}; 

#endif //> !ITKOCCUPANCY_MAXSTRIPSTUDY_H
