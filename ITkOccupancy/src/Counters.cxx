#include "Counters.h"
#include <iostream>
#include <vector>
#include <tuple>
#include <map>

#include "TH1F.h"
#include "TH2F.h"

Counters::Counters() 
  {

  sumX = 0.0; sumY = 0.0; sumZ = 0.0; sumR = 0.0;
  sumClusSize = 0.0; sumClusSizeSq = 0.0;
  sumClus = 0.0;

  m_trackAssociated = 0;

  m_truthtrackAssociated = 0; 

  }
   
Counters::Counters(int layer, int etaModuleID, int BEC)
{
  SetIndices(layer, etaModuleID, BEC);

  sumX = 0.0; sumY = 0.0; sumZ = 0.0; sumR = 0.0;
  sumClusSize = 0.0; sumClusSizeSq = 0.0;
   sumClus = 0.0;

   m_trackAssociated = 0;
    m_truthtrackAssociated = 0; 

  
}

  
Counters::Counters(int withBoxes)
{

  sumX = 0.0; sumY = 0.0; sumZ = 0.0; sumR = 0.0;
  sumClusSize = 0.0; sumClusSizeSq = 0.0;
  sumClus = 0.0;

   m_trackAssociated = 0;
   m_truthtrackAssociated = 0;

   std::vector<std::tuple<int,int,int>> boxesV3;

   // -------------------------------
   // this is the 2 x 2 read out scheme
   // -------------------------------

   // should be 401 / 2

   /*
   int eta_indices = 201;

   // 337 / 2
   int phi_indices = 169; 

   for (int i=0; i < 201; i++ ) {
     for (int j=0; j < 169; j++ ) {

       int a = int(i);
       int b = int(j); 

       std::tuple<int,int,int> tuple_holder (a,b,0); 
       boxesV2.push_back(tuple_holder); 

     }
   }

   */

   
   // -------------------------------
   // this is the 4 x 1 read out scheme. 
   // -------------------------------

   // should be 401 / 4.
   int eta_indicesV2 = 101;

   // should be 338 / 1
   int phi_indicesV2 = 339;

   // our tuples are labelled by box index X, box index Y and number of times that box has been hit

   
   for (int k=0; k < eta_indicesV2; k++ ) {
     for (int l=0; l < phi_indicesV2; l++ ) {

       int a = int(k);
       int b = int(l); 

       std::tuple<int,int,int> tuple_holder (a,b,0); 
       boxesV2.push_back(tuple_holder); 

     }
   }


  
}


// create a set indices method, for both layer, etaID and Barrel/End Cap
// also overload with another method -- used for maximum occupancy studies, for layer,
 
void Counters::SetIndices(int layer, int etaModuleID, int BEC)
{
  m_layer = layer;
  m_etaModuleID = etaModuleID;
  m_BEC = BEC;
}

 
void Counters::SetIndices(int layer, int etaModuleID, int phiModuleID, int side)
{
  m_layer = layer;
  m_etaModuleID = etaModuleID;
  m_phiModuleID = phiModuleID; 
  m_side = side; 
}

//void SetPixelIndices(int layer, int etaModuleID, int phiModuleID, int inclinedSection, int chipIndex); 

void Counters::SetPixelIndices(int layer, int etaModuleID, int phiModuleID, int inclinedSection, int chipIndex)
{
  m_layer = layer;
  m_etaModuleID = etaModuleID;
  m_phiModuleID = phiModuleID;
  m_inclinedSection = inclinedSection;
  m_chipIndex = chipIndex; 

}


void Counters::SetTrackAssociation()
{
  m_trackAssociated = 1; 
}

void Counters::SetTruthTrackAssociation()
{
  m_truthtrackAssociated = 1; 
}



void Counters::SetIndices(int layer, int etaModuleID, int phiModuleID, int side, int BEC)
{
  m_layer = layer;
  m_etaModuleID = etaModuleID;
  m_phiModuleID = phiModuleID; 
  m_side = side;
  m_BEC = BEC; 
}



 
void Counters::SetSensorID(uint64_t sensorID)
{
   m_sensorID = sensorID;
}



//void SetSensorID(uint64_t sensorID);




void Counters::setReadOut(float readOut)
{
  m_readOut = readOut;

}


void Counters::setEvents(float nEvents)
{
  // m_readOut = readOut;
  m_events = nEvents;

}

float Counters::getReadOut()
{
   return m_readOut;

}

void Counters::setArea(float area)
{

  m_area = area;

}


void Counters::fixR(float radius)
{

  m_R = radius; 

}


void Counters::fixZ(float fixedZ)
{

  m_Z = fixedZ; 

}




float Counters::getfixedR()
{

  return m_R; 
}



float Counters::getfixedZ()
{

  return m_Z; 
}


 
// this function is for the average occupancy studies..

void Counters::add(float x, float y, float z, float clussize)
{

  sumX += x;
  sumY += y;
  sumZ += z;
  sumR += sqrt(x*x + y*y);
  sumClusSize += clussize;
  sumClusSizeSq += clussize*clussize;
  sumClus += 1.0; 

}


void Counters::addCluster(float clussize)
{
  sumClusSize += clussize;
  sumClus += 1.0; 

}


void Counters::addEtaPixels(std::vector<int> eta_pixels)
{

all_eta_pixels.push_back(eta_pixels); 

}


void Counters::addPhiPixels(std::vector<int> phi_pixels)
{

all_phi_pixels.push_back(phi_pixels); 

}



float Counters::occupancy()
{


  if (sumClusSize != 0.0 ) {

    //std::cout << " cluster size? " << sumClusSize << " read out? " << m_readOut << " events? " << m_events << std::endl;
    float occupancy = float(sumClusSize) / float( m_readOut*m_events); 
    return occupancy; 
  }
  else return 0.0; 

}



std::vector<std::vector<int>> Counters::getAllEtaPixels()
{
  return all_eta_pixels; 
}


std::vector<std::vector<int>> Counters::getAllPhiPixels()
{
  return all_phi_pixels; 
}








float Counters::fluency()
{
  if ( m_area != 0.0 ) { 
  return (sumClusSize) / (m_area*m_events) ;
  }

  else return 0.0; 

}


float Counters::clusterDensity()
{
  if ( m_area != 0.0 ) { 
  return (sumClus) / (m_area*m_events) ;
  }

  else return 0.0; 

}



float Counters::averageClusterSize()
{
  return ( sumClusSize / sumClus) ;

}


float Counters::getNHits()
{
  
  return sumClusSize;

}


float Counters::getNClusters()
{
  
  return sumClus;

}



void Counters::sumclussize(float clus)
{
  
  sumClusSize += clus; 

}



float Counters::getClusterSize()
{
   return sumClusSize;

}



float Counters::getX()
{
  if (sumClusSize != 0.0 ) {

    float avgX = float(sumX) / float(sumClus); 
    return avgX; 
  }
  else return 0.0; 

}


float Counters::getY()
{
  if (sumClusSize != 0.0 ) {

    float avgY = float(sumY) / float(sumClus); 
    return avgY; 
  }
  else return 0.0; 

}




float Counters::getZ()
{
  if (sumClusSize != 0.0 ) {

    float avgZ = float(sumZ) / float(sumClus); 
    return avgZ; 
  }
  else return 0.0; 

}



float Counters::getR()
{
  if (sumClusSize != 0.0 ) {

    float avgR = float(sumR) / float(sumClus); 
    return avgR; 
  }
  else return 0.0; 

}





void Counters::addBox(std::vector<int> aBox)
{
  boxes.push_back(aBox); 

}



void Counters::assignToBox(int etaIndex, int phiIndex)
{


  int newEtaIndex = etaIndex;
  int newPhiIndex = phiIndex; 

  if ( newEtaIndex > 401 ) { newEtaIndex = newEtaIndex - 401; }
  if ( newPhiIndex > 337 ) { newPhiIndex = newPhiIndex - 337; }



  // sort the eta & phi indices out!
  int boxX = floor(( newEtaIndex / 2));
  int boxY = floor(( newPhiIndex / 2));


  // this is sloppy --> should use look up table instead. 
  // increment the z component of a specific vector
  for (int k=0; k < boxesV2.size(); k++ ) {
    if ( boxX == std::get<0>(boxesV2[k]) && boxY == std::get<1>(boxesV2[k])   ) {
      std::get<2>(boxesV2[k]) = std::get<2>(boxesV2[k]) + 1;
    }

  }

}



void Counters::assignToBoxV2(int etaIndex, int phiIndex)
{


  int newEtaIndex = etaIndex;
  int newPhiIndex = phiIndex; 

  if ( newEtaIndex > 401 ) { newEtaIndex = newEtaIndex - 401; }
  if ( newPhiIndex > 337 ) { newPhiIndex = newPhiIndex - 337; }


  // sort the eta & phi indices so compatible with 2x2 readout ToT
  int boxX = floor(( newEtaIndex / 2));
  int boxY = floor(( newPhiIndex / 2));

  bool seenBox = false;
  // if seen before --> increment 
  
  for (int k=0; k < boxesV3.size(); k++ ) {
    if ( boxX == std::get<0>(boxesV3[k]) && boxY == std::get<1>(boxesV3[k])   ) {
      seenBox = true; 
      std::get<2>(boxesV3[k]) = std::get<2>(boxesV3[k]) + 1;
    }
  }

  // if not seen before --> create box
  if (!seenBox)  {
    std::tuple<int,int,int> tuple_holder (newEtaIndex,newPhiIndex,1); 
    boxesV3.push_back(tuple_holder); 
  }

}






/*
void Counters::assignToBoxV2(int etaIndex, int phiIndex)
{


  int newEtaIndex = etaIndex;
  int newPhiIndex = phiIndex; 

  if ( newEtaIndex > 401 ) { newEtaIndex = newEtaIndex - 401; }
  if ( newPhiIndex > 337 ) { newPhiIndex = newPhiIndex - 337; }



  // sort the eta & phi indices out!
  int boxX = floor(( etaIndex / 2));
  int boxY = floor(( phiIndex / 2));

  // this is sloppy --> should use look up table instead. 
  // increment the z component of a specific vector
  for (int k=0; k < boxesV2.size(); k++ ) {
    if ( boxX == std::get<0>(boxesV2[k]) && boxY == std::get<1>(boxesV2[k])   ) {
      std::get<2>(boxesV2[k]) = std::get<2>(boxesV2[k]) + 1;
    }
    }

  
  std::tuple<int,int> linker (boxX, boxY);
  pixelBoxes[linker] = pixelBoxes[linker] + 1; 


  

}







void Counters::assignToBoxV3(int etaIndex, int phiIndex)
{


  int newEtaIndex = etaIndex;
  int newPhiIndex = phiIndex; 

  if ( newEtaIndex > 401 ) { newEtaIndex = newEtaIndex - 401; }
  if ( newPhiIndex > 337 ) { newPhiIndex = newPhiIndex - 337; }


  bool seenBefore = false; 



  // sort the eta & phi indices out!
  int boxX = floor(( etaIndex / 2));
  int boxY = floor(( phiIndex / 2));

  // increment the z component of a specific vector
  for (int k=0; k < boxesV3.size(); k++ ) {
    // if we have seen before, increment
    if ( boxX == std::get<0>(boxesV3[k]) && boxY == std::get<1>(boxesV3[k])   ) {
      seenBefore = true;
      std::get<2>(boxesV3[k]) = std::get<2>(boxesV3[k]) + 1;
    }
  }

    // it not seen before, create new box and increment 

    if (!seenBefore) {
      // value of 1 to show it has been hit
       std::tuple<int,int,int> tuple_holder (newEtaIndex,newPhiIndex,1); 
       boxesV3.push_back(tuple_holder); 

  }

}

    
*/



std::vector<std::tuple<int,int,int>> Counters::getBoxes()
{
  return boxesV2; 
}


std::vector<std::tuple<int,int,int>> Counters::getBoxesV2()
{
  return boxesV3; 
  //boxesV2;
}




/*


TH1F Counters::getHisto()
{
  return histoA; 
  //boxesV2;
}



*/







/*


std::map<std::tuple<int,int>, int>  Counters::getMap()
{
  return pixelBoxes; 
  //boxesV2;
}

*/




void Counters::assignToBoxFourByOne(int etaIndex, int phiIndex)
{


  int newEtaIndex = etaIndex;
  int newPhiIndex = phiIndex; 

  // this adjustment is a result of the eta and phi indices being written per sensor, not per chip

  if ( newEtaIndex > 401 ) { newEtaIndex = newEtaIndex - 401; }
  if ( newPhiIndex > 337 ) { newPhiIndex = newPhiIndex - 337; }

  // sort the eta & phi indices out.
  // divide by 4 and round down.

  
  int boxX = floor(( newEtaIndex / 4));
  int boxY = floor(( newPhiIndex / 1));


  // this is sloppy --> should use look up table instead. 
  // increment the z component of a specific vector
  for (int k=0; k < boxesV2.size(); k++ ) {
    if ( boxX == std::get<0>(boxesV2[k]) && boxY == std::get<1>(boxesV2[k])   ) {
      std::get<2>(boxesV2[k]) = std::get<2>(boxesV2[k]) + 1;
    }

  }

}




void Counters::assignToBoxFourByOneCreation(int etaIndex, int phiIndex)
{


  int newEtaIndex = etaIndex;
  int newPhiIndex = phiIndex; 

  if ( newEtaIndex > 401 ) { newEtaIndex = newEtaIndex - 401; }
  if ( newPhiIndex > 337 ) { newPhiIndex = newPhiIndex - 337; }

  // sort the eta & phi indices out.
  // divide by 4 and round down.
  
  int boxX = floor(( newEtaIndex / 4));
  int boxY = floor(( newPhiIndex / 1));

  // this is sloppy --> should use look up table instead. 
  // increment the z component of a specific vector

  bool seenBox = false; 
  
  for (int k=0; k < boxesV3.size(); k++ ) {
    if ( boxX == std::get<0>(boxesV3[k]) && boxY == std::get<1>(boxesV3[k])   ) {
      seenBox = true; 
      std::get<2>(boxesV3[k]) = std::get<2>(boxesV3[k]) + 1;
    }
  }

  if (!seenBox) {
      // value of 1 to show it has been hit
       std::tuple<int,int,int> tuple_holder (newEtaIndex,newPhiIndex,1); 
       boxesV3.push_back(tuple_holder);

  }

}
























