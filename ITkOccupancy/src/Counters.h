#ifndef COUNTERS_H
#define COUNTERS_H

#include <math.h>
#include <iostream>
#include <vector>
#include <tuple>
#include <map>
#include "TH1F.h"
#include "TH2F.h"


class Counters
{
 private:
  int m_layer;
  int m_etaModuleID;
  int m_phiModuleID;
  int m_side;

  int m_inclinedSection;
  int m_chipIndex;

  uint64_t m_sensorID;
  
  int m_trackAssociated;
  int m_truthtrackAssociated;


  //TH1F histoA = TH1F("histoA","histoA",1000,0.0,1000.0);

  std::vector<std::tuple<int,int,int>> boxesV2;
  std::vector<std::tuple<int,int,int>> boxesV3;

  // below is the next iteration attempt
  //std::map<std::tuple<int,int>, int> pixelBoxes;
  
  //m_etaModuleID = etaModuleID;
  //m_phiModuleID = phiModuleID; 
  //m_side = side; 
  
  int m_BEC;
  float m_readOut;
  float m_area;
  float m_R;
  float m_Z; 

  float m_events;
  float sumX;
  float sumY;
  float sumZ;
  float sumR;
  float sumClusSize;
  float sumClusSizeSq;
  float sumClus;

  //std::vector<std::vector<int>> boxesV2;
  std::vector<std::vector<int>> boxes;

  //std::map<std::tuple<int,int,int>, Counters> stripBarrel; 


  // for the pixel chips, for the eta_pixel_indices and phi_pixel_indices.
  // create vector of vectors
  //std::vector<int> eta_pixels = pix->auxdata<std::vector<int>>("rdo_eta_pixel_index");
  //std::vector<int> phi_pixels = pix->auxdata<std::vector<int>>("rdo_phi_pixel_index");

  //std::vector<vector<int>> all_eta_pixels;
  //std::vector<vector<int>> all_phi_pixels;

  std::vector<std::vector<int>> all_eta_pixels;
  std::vector<std::vector<int>> all_phi_pixels; 
  

    

  

 
 public:
  Counters(int layer, int etaModuleID, int BEC);
  Counters();
  Counters(int withBoxes); 
 
  void SetIndices(int layer, int etaModuleID, int BEC);
  void SetIndices(int layer, int etaModuleID, int phiModuleID, int side);

  //c.SetIndices(layer, etaModule, phiModule, inclinedSection, chipIndex);
  //
  void SetIndices(int layer, int etaModuleID, int phiModuleID, int side, int BEC);
  void SetPixelIndices(int layer, int etaModuleID, int phiModuleID, int inclinedSection, int chipIndex); 
  void SetSensorID(uint64_t sensorID);
  void setReadOut(float readOut);
  void setArea(float area);


  //void SetPixelIndices(int layer, int etaModuleID, int phiModuleID, int inclinedSection, int chipIndex);
  //void SetPixelIndices(int layer, int etaModuleID, int phiModuleID, int inclinedSection, int chipIndex);

  void addEtaPixels(std::vector<int> eta_pixels);
  void addPhiPixels(std::vector<int> phi_pixels);



  
  void SetTrackAssociation();
  void SetTruthTrackAssociation();


  
  
  void fixR(float radius);
  void fixZ(float fixedZ);


  
  void setEvents(float nEvents);
  void sumclussize(float clus);
  
  int getLayer() { return m_layer; }
  int getEtaID() { return m_etaModuleID; }
  int getPhiID() { return m_phiModuleID; }
  int getSide() { return m_side; }
  int getBEC()  { return m_BEC; }
  int getSensorID()  { return m_sensorID; }

  int getInclined() { return m_inclinedSection; }
  int getChip() { return m_chipIndex; }

  int getTrackAssociation() { return m_trackAssociated; }
  int getTruthTrackAssociation() { return m_truthtrackAssociated; }

  float getReadOut();
  float getClusterSize();
  
  float getX();
  float getY();
  float getZ();

  float getR();

  float getfixedR();
  float getfixedZ();

  float getNHits();
  float getNClusters();
  float occupancy();
  float fluency();

  float clusterDensity();
  float averageClusterSize();

  
  std::vector<std::vector<int>> getAllEtaPixels();
  std::vector<std::vector<int>> getAllPhiPixels();

  std::vector<std::tuple<int,int,int>> getBoxes();

  std::vector<std::tuple<int,int,int>> getBoxesV2();

  std::map<std::tuple<int,int>, int>  getMap();

  //TH1F getHisto();
  // below is the next iteration attempt
  //std::map<std::tuple<int,int>, int> pixelBoxes;

  void add(float x, float y, float z, float clussize);

  void addCluster(float clussize);


  void addBox(std::vector<int> aBox);

  void assignToBox(int etaIndex, int phiIndex);
  void assignToBoxV2(int etaIndex, int phiIndex);
  void assignToBoxV3(int etaIndex, int phiIndex);


  void assignToBoxFourByOne(int etaIndex, int phiIndex);

   void assignToBoxFourByOneCreation(int etaIndex, int phiIndex);


  //void Counters::assignToBox(int etaIndex, int phiIndex)



  //void initialiseBoxes();

  //void eventIncrement();
  

  // float getReadOut() { return m_readOut; } 



};
 
#endif
