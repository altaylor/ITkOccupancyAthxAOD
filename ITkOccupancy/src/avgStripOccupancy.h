#ifndef MYPACKAGE_AVGSTRIPOCCUPANCY_H
#define MYPACKAGE_AVGSTRIPOCCUPANCY_H 1

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h" //included under assumption you'll want to use some tools! Remove if you don't!
#include "TTree.h"
#include "TGraph.h"
#include "TH1D.h"
#include "TH2D.h"
#include "Counters.h"

using namespace std;



class avgStripOccupancy: public ::AthAlgorithm { 
 public: 
  avgStripOccupancy( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~avgStripOccupancy(); 

  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();

 private:

  TTree *myTree; //!
  TH1D *myHist; //!

  TH2D *sct_disk_occ_map2D; //!
  TH2D *sct_disk_occ_map2D_norm; //!
  TH2D *sct_disk_hitmap2D; //!
  TH2D *sct_disk_hitmap2D_norm; //!

  // also register with smaller binning also

  TH2D *sct_disk_occ_map2DV2; //!
  TH2D *sct_disk_occ_map2DV2_norm; //!
  TH2D *sct_disk_hitmap2DV2; //!
  TH2D *sct_disk_hitmap2DV2_norm; //!

  
  TH2D *sct_barrel_occ_map2D; //!
  TH2D *sct_barrel_occ_map2D_norm; //!
  TH2D *sct_barrel_hitmap2D; //!
  TH2D *sct_barrel_hitmap2D_norm; //!

  // also register with smaller binning also

  TH2D *sct_barrel_occ_map2DV2; //!
  TH2D *sct_barrel_occ_map2DV2_norm; //!
  TH2D *sct_barrel_hitmap2DV2; //!
  TH2D *sct_barrel_hitmap2DV2_norm; //!


  // plots for the barrel

  TGraph *occ_z_0; //!
  TGraph *occ_z_1; //!
  TGraph *occ_z_2; //!
  TGraph *occ_z_3; //!
  
  TGraph *hit_dens_z_0; //!
  TGraph *hit_dens_z_1; //!
  TGraph *hit_dens_z_2; //!
  TGraph *hit_dens_z_3; //!
   
  TGraph *clus_size_z_0; //!
  TGraph *clus_size_z_1; //!
  TGraph *clus_size_z_2; //!
  TGraph *clus_size_z_3; //!

  // plots for the endcap
  
  TGraph *occ_r_0; //!
  TGraph *occ_r_1; //!
  TGraph *occ_r_2; //!
  TGraph *occ_r_3; //!
  TGraph *occ_r_4; //!
  TGraph *occ_r_5; //!
   
  TGraph *hit_dens_r_0; //!
  TGraph *hit_dens_r_1; //!
  TGraph *hit_dens_r_2; //!
  TGraph *hit_dens_r_3; //!
  TGraph *hit_dens_r_4; //!
  TGraph *hit_dens_r_5; //!

  
  TGraph *clus_size_r_0; //!
  TGraph *clus_size_r_1; //!
  TGraph *clus_size_r_2; //!
  TGraph *clus_size_r_3; //!
  TGraph *clus_size_r_4; //!
  TGraph *clus_size_r_5; //!

  // register 2D occupancy plots & hit maps 




  //Counters tester; //!

  //std::vector<Counters> tester; //!

  std::map<std::tuple<int,int,int>, Counters> stripBarrel; //!
  std::map<std::tuple<int,int,int>, Counters> stripEndCap; //!
  

  float nEvents; //!


  

}; 

#endif //> !MYPACKAGE_AVGSTRIPOCCUPANCY_H
