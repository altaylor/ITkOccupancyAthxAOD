from ROOT import *
ROOT.gROOT.LoadMacro("AtlasStyle.C")
ROOT.gROOT.LoadMacro("AtlasLabels.C")
ROOT.gROOT.LoadMacro("AtlasUtils.C")

SetAtlasStyle()

ROOT.gStyle.SetOptStat(False)
ROOT.gStyle.SetMarkerSize(1.5)

#signal = TFile("/afs/cern.ch/work/a/altaylor/BB_YYAnalysis/TestPackage/util/h013/ttH_h013/hist-ttH_h013.root")
signal = TFile("/afs/cern.ch/work/a/altaylor/itk_occupancy_svn2/ITkOccupancy/ITkOccupancy/ITkOccupancy/run/Plotting/avgStudies_step15.root")
signal.ls()

def main():

    # arguments are the graphs required to produce multigraph, y axis title, y axis max and savename
    
    # plot the channel occupancy for the sct barrel
    # graph names, then y axis maximum
    PlotBarrelOccupancy("occ_z_0","occ_z_1","occ_z_2","occ_z_3", "Strip Channel Occupancy in %", 1.0, "occ_vs_z_barrel")
    
    # plot the hit density for the sct barrel
    PlotBarrelOccupancy("hit_dens_z_0","hit_dens_z_1","hit_dens_z_2","hit_dens_z_3", "Strip Hits / mm^{2}", 0.0055, "hit_density_vs_z_barrel")
    
    # plot the cluster size for the sct barrel
    PlotBarrelOccupancy("clus_size_z_0","clus_size_z_1","clus_size_z_2","clus_size_z_3", "Average cluster size [strips]", 3.1, "cluster_size_vs_z_barrel")

    # plot the channel occupancy for the sct end cap
    PlotEndCapOccupancy("occ_r_0","occ_r_1","occ_r_2","occ_r_3", "occ_r_4","occ_r_5","Strip Channel Occupancy in %", 1.3, "occ_vs_r_endcap")

    # plot the hit density for the sct end cap
    PlotEndCapOccupancy("hit_dens_r_0","hit_dens_r_1","hit_dens_r_2","hit_dens_r_3", "hit_dens_r_4","hit_dens_r_5","Strip Hits / mm^{2}", 1.3, "hit_density_vs_r_endcap")

    # plot the end cap cluster size
    PlotEndCapOccupancy("clus_size_r_0","clus_size_r_1","clus_size_r_2","clus_size_r_3", "clus_size_r_4","clus_size_r_5","Average cluster size [strips]", 2.5, "clus_size_vs_r_endcap")

    
def PlotBarrelOccupancy(graph_0, graph_1, graph_2, graph_3, yaxis, yaxismax, saveName):

    g_0 = TGraph()
    g_1 = TGraph()
    g_2 = TGraph()
    g_3 = TGraph()
    
    signal.GetObject(graph_0, g_0)
    signal.GetObject(graph_1, g_1)
    signal.GetObject(graph_2, g_2)
    signal.GetObject(graph_3, g_3)

    g_0.SetMarkerColor(1)
    g_1.SetMarkerColor(2)
    g_2.SetMarkerColor(3)
    g_3.SetMarkerColor(4)

    g_0.SetMarkerStyle(20)
    g_1.SetMarkerStyle(21)
    g_2.SetMarkerStyle(22)
    g_3.SetMarkerStyle(23)

    MultiG = TMultiGraph()

    MultiG.Add(g_0)
    MultiG.Add(g_1)
    MultiG.Add(g_2)
    MultiG.Add(g_3)
    MultiG.Draw("AP")

    MultiG.GetXaxis().SetTitle("z [mm]")
    MultiG.GetYaxis().SetTitle(yaxis)
        
    MultiG.GetYaxis().SetRangeUser(0,yaxismax)
    MultiG.GetXaxis().SetRangeUser(0,1400.0)

    leg = TLegend(0.73,0.65,0.88,0.80)


     
    if yaxis == "Average cluster size [strips]":
        leg = TLegend(0.63+0.03,0.74,0.78+0.03,0.89)



    leg.AddEntry(g_0, "Layer 0", "p")
    leg.AddEntry(g_1, "Layer 1", "p")
    leg.AddEntry(g_2, "Layer 2", "p")
    leg.AddEntry(g_3, "Layer 3", "p")
    leg.SetFillColor(0)
    leg.SetTextSize(0.04)
    leg.SetBorderSize(0)
    leg.Draw()
        

    #ATLAS_LABEL(0.2,0.75); myText(0.32,0.75,1,"Simulation Internal");
    #myText(0.2,0.70,1,"ITk Inclined");
    #myText(0.2,0.65,1,"minimum bias, <#mu> = 200");

    
    
    if yaxis != "Average cluster size [strips]":
        ATLAS_LABEL(0.2,0.75); myText(0.32,0.75,1,"Simulation Preliminary");
        myText(0.2,0.70,1,"ITk Inclined");
        myText(0.2,0.65,1,"minimum bias, <#mu> = 200");


    
    if yaxis == "Average cluster size [strips]":
        MultiG.GetYaxis().SetRangeUser(2.2,yaxismax)
        ATLAS_LABEL(0.2,0.85); myText(0.32,0.85,1,"Simulation Preliminary");
        myText(0.2,0.80,1,"ITk Inclined");
        myText(0.2,0.75,1,"minimum bias, <#mu> = 200")



    c1.Update()

    
    c1.SaveAs("plots/strips/barrel/%s.pdf" %(saveName))
    c1.SaveAs("plots/strips/barrel/%s.eps" %(saveName))
    c1.SaveAs("plots/strips/barrel/%s.png" %(saveName))

    
    
def PlotEndCapOccupancy(graph_0, graph_1, graph_2, graph_3, graph_4, graph_5, yaxis, yaxismax, saveName):

    g_0 = TGraph()
    g_1 = TGraph()
    g_2 = TGraph()
    g_3 = TGraph()
    g_4 = TGraph()
    g_5 = TGraph()
    
    signal.GetObject(graph_0, g_0)
    signal.GetObject(graph_1, g_1)
    signal.GetObject(graph_2, g_2)
    signal.GetObject(graph_3, g_3)
    signal.GetObject(graph_4, g_4)
    signal.GetObject(graph_5, g_5)

    g_0.SetMarkerColor(1)
    g_1.SetMarkerColor(2)
    g_2.SetMarkerColor(3)
    g_3.SetMarkerColor(4)
    g_4.SetMarkerColor(8)
    g_5.SetMarkerColor(6)

    g_0.SetMarkerStyle(20)
    g_1.SetMarkerStyle(21)
    g_2.SetMarkerStyle(22)
    g_3.SetMarkerStyle(23)
    g_4.SetMarkerStyle(29)
    g_5.SetMarkerStyle(34)

    MultiG = TMultiGraph()

    MultiG.Add(g_0)
    MultiG.Add(g_1)
    MultiG.Add(g_2)
    MultiG.Add(g_3)
    MultiG.Add(g_4)
    MultiG.Add(g_5)
    MultiG.Draw("AP")

    MultiG.GetXaxis().SetTitle("R [mm]")
    MultiG.GetYaxis().SetTitle(yaxis)

    # below was commented out before??
    # for the cluster size plot...
    #MultiG.GetYaxis().SetRangeUser(0,yaxismax)
    MultiG.GetYaxis().SetRangeUser(1.35,yaxismax)



    MultiG.GetXaxis().SetRangeUser(0,1400.0)

    leg = TLegend(0.72,0.6,0.90,0.86)

    
    if saveName == "occ_vs_r_endcap":
        
            
        ATLAS_LABEL(0.25,0.85); myText(0.37,0.85,1,"Simulation Preliminary");
        myText(0.25,0.80,1,"ITk Inclined");
        myText(0.25,0.75,1,"minimum bias, <#mu> = 200");

    if saveName == "hit_density_vs_r_endcap":
            

        ATLAS_LABEL(0.3,0.85); myText(0.42,0.85,1,"Simulation Preliminary");
        myText(0.3,0.80,1,"ITk Inclined");
        myText(0.3,0.75,1,"minimum bias, <#mu> = 200");

        leg = TLegend(0.72+0.03,0.6,0.90+0.03,0.86)




    #leg = TLegend(0.72,0.6,0.90,0.86)
    leg.AddEntry(g_0, "Disk 0", "p")
    leg.AddEntry(g_1, "Disk 1", "p")
    leg.AddEntry(g_2, "Disk 2", "p")
    leg.AddEntry(g_3, "Disk 3", "p")
    leg.AddEntry(g_4, "Disk 4", "p")
    leg.AddEntry(g_5, "Disk 5", "p")
    leg.SetFillColor(0)
    leg.SetTextSize(0.04)
    leg.SetBorderSize(0)

    
    leg.Draw()

    
        
    if saveName == "clus_size_vs_r_endcap":
            
        leg = TLegend((0.72-0.5),0.6,(0.90-0.5),0.86)
        leg.AddEntry(g_0, "Disk 0", "p")
        leg.AddEntry(g_1, "Disk 1", "p")
        leg.AddEntry(g_2, "Disk 2", "p")
        leg.AddEntry(g_3, "Disk 3", "p")
        leg.AddEntry(g_4, "Disk 4", "p")
        leg.AddEntry(g_5, "Disk 5", "p")
        leg.SetFillColor(0)
        leg.SetTextSize(0.04)
        leg.SetBorderSize(0)
        leg.Draw()

        ATLAS_LABEL(0.45,0.85); myText((0.45+0.12),0.85,1,"Simulation Preliminary");
        myText(0.45,0.80,1,"ITk Inclined");
        myText(0.45,0.75,1,"minimum bias, <#mu> = 200");


        
    '''

    if saveName == "occ_vs_r_endcap":
        
            
        ATLAS_LABEL(0.25,0.85); myText(0.37,0.85,1,"Simulation Internal");
        myText(0.25,0.80,1,"ITk Inclined");
        myText(0.25,0.75,1,"minimum bias, <#mu> = 200");

    if saveName == "hit_density_vs_r_endcap":
            

        ATLAS_LABEL(0.3,0.85); myText(0.42,0.85,1,"Simulation Internal");
        myText(0.3,0.80,1,"ITk Inclined");
        myText(0.3,0.75,1,"minimum bias, <#mu> = 200");

    '''



    c1.Update()

    
    c1.SaveAs("plots/strips/end_cap/%s.pdf" %(saveName))
    c1.SaveAs("plots/strips/end_cap/%s.eps" %(saveName))
    c1.SaveAs("plots/strips/end_cap/%s.png" %(saveName))

    



main()

#ATLAS_LABEL(0.2,0.8)
#first argument is where the text starts on the x-axis.
#second argument is where the test starts on the y-axis
#myText(0.2,0.75,1,"Internal")


#c1.SetLogy()
#c1.SaveAs("tester.png")


