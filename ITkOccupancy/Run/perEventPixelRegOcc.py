#Skeleton joboption for a simple analysis job

theApp.EvtMax=1                                      #says how many events to run over. Set to -1 for all event

import AthenaPoolCnvSvc.ReadAthenaPool                   #sets up reading of POOL files (e.g. xAODs)
import glob

import AthenaCommon.Constants as Lvl

# for job configuration: the application manager object                                                                                                                                                                                                                                  
# as the name implies, there is only one application manager                                                                                                                                                                                                                             
from AthenaCommon.AppMgr import theApp

# for job configuration: the object to hold all services
# note: the real object is 'ServiceMgr' but we give it a shorter alias 'svcMgr'                                                                                                                                                                                                          
from AthenaCommon.AppMgr import ServiceMgr as svcMgr

#root://eosatlas.cern.ch//eos/atlas/atlasgroupdisk/perf-idtracking/dq2/rucio/mc15_14TeV/70/42/DAOD_IDTRKVALID.09342210._000001.pool.root.1
#root://eosatlas.cern.ch//eos/atlas/atlasgroupdisk/perf-idtracking/dq2/rucio/mc15_14TeV/6b/af/DAOD_IDTRKVALID.09342210._000002.pool.root.1
# this works below

inclinedEvents = ["/afs/cern.ch/work/a/altaylor/public/itk_step16/inclined/mc15_14TeV.117050.PowhegPythia_P2011C_ttbar.recon.DAOD_IDTRKVALID.e2176_s2988_s3000_r8832/itk_inclined_step16.root"]
inclinedEvents = ['/afs/cern.ch/work/a/altaylor/public/itk_step16/inclined/mc15_14TeV.117050.PowhegPythia_P2011C_ttbar.recon.DAOD_IDTRKVALID.e2176_s2988_s3000_r8832/itk_inclined_step16.root']


svcMgr.EventSelector.InputCollections = inclinedEvents

#svcMgr.EventSelector.InputCollections = extendedEvents



#svcMgr.EventSelector.InputCollections=["/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"]   #insert your list of input files here
algseq = CfgMgr.AthSequencer("AthAlgSeq") 

#algseq += CfgMgr.pixelBox()
#from ITkOccupancy.itk_occ import itk_occ
#algseq += itk_occ()

algseq = CfgMgr.AthSequencer("AthAlgSeq")                #gets the main AthSequencer       

svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += ["MYSTREAM DATAFILE='100events_inclined.root' OPT='RECREATE'"]


algseq += CfgMgr.PerEventPixelsRegOcc()
