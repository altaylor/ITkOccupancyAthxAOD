'''
from ROOT import *
ROOT.gROOT.LoadMacro("AtlasStyle.C")
ROOT.gROOT.LoadMacro("AtlasLabels.C")
ROOT.gROOT.LoadMacro("AtlasUtils.C")

SetAtlasStyle()

#ROOT.gStyle.SetOptStat(True) #turn off stats boxes
ROOT.gStyle.SetMarkerSize(1.5)
ROOT.gStyle.SetMarkerStyle(20)
ROOT.gStyle.SetFillColor(0)
ROOT.gStyle.SetPalette(1)

from array import array
import numpy as np
import scipy
import sys
#import SetPalette

ROOT.gStyle.SetOptStat(False)

'''


from ROOT import *
ROOT.gROOT.LoadMacro("AtlasStyle.C")
ROOT.gROOT.LoadMacro("AtlasLabels.C")
ROOT.gROOT.LoadMacro("AtlasUtils.C")

SetAtlasStyle()

ROOT.gStyle.SetOptStat(False)
ROOT.gStyle.SetMarkerSize(1.5)


from array import array
import numpy as np
import scipy
import sys




c1 = TCanvas()
c1.SetLogz()



strips = TFile("/afs/cern.ch/work/a/altaylor/itk_occupancy_svn2/ITkOccupancy/ITkOccupancy/ITkOccupancy/run/Plotting/avgStudies_step15.root")
pixels = TFile("/afs/cern.ch/work/a/altaylor/itk_occupancy_svn2/ITkOccupancy/ITkOccupancy/ITkOccupancy/run/Plotting/avgPixelStudies_inclined_step15_V2.root")


pixels.ls()

# produce custom palette!

'''

NRGBs = 5
NCont = 255

stops = [ 0.00, 0.34, 0.61, 0.84, 1.00 ]
red = [ 0.00, 0.00, 0.87, 1.00, 0.51 ]
green = [ 0.00, 0.81, 1.00, 0.20, 0.00 ]
blue = [ 0.51, 1.00, 0.12, 0.00, 0.00 ]

stopsArray = array('d', stops)
redArray   = array('d', red)
greenArray = array('d', green)
blueArray  = array('d', blue)

TColor.CreateGradientColorTable(NRGBs, stopsArray, redArray, greenArray, blueArray, NCont)
ROOT.gStyle.SetNumberContours(NCont)

'''

def main():
            
    # pass four plots & y-axis name & saveName
    Plot2DOccupancy("sct_barrel_occ_map2DV2", "sct_barrel_occ_map2DV2_norm", "sct_disk_occ_map2DV2", "sct_disk_occ_map2DV2_norm", "Channel Occupancy in %", "pix_and_strips_total")
    #Plot2DOccupancy("sct_barrel_hitmap2D", "sct_barrel_hitmap2D_norm", "sct_disk_hitmap2D", "sct_disk_hitmap2D_norm", "Strip hits / mm^{2}",  "hit_map_total")


    
def Plot2DOccupancy(barrelmap, barrelmap_norm, diskmap, diskmap_norm, yaxisName, saveName):

    # grab the SCT barrel plots and normalise it.

    barrelPlot = TH2D()
    normbarrelPlot = TH2D()
    strips.GetObject(barrelmap, barrelPlot);
    barrelPlot.SetDirectory(0)
    strips.GetObject(barrelmap_norm, normbarrelPlot);
    normbarrelPlot.SetDirectory(0)

    #normalise plot in the case of double counting in a bin
    barrelPlot.Divide(normbarrelPlot)
    # grab the SCT end-cap plots and normalise it.
    diskPlot = TH2D()
    normdiskPlot = TH2D()
    
    strips.GetObject(diskmap, diskPlot);
    diskPlot.SetDirectory(0)
    strips.GetObject(diskmap_norm, normdiskPlot);
    normdiskPlot.SetDirectory(0)
    # normalise plot in case of double counting
    diskPlot.Divide(normdiskPlot)

    # here we do the nasty hack for the barrel..
        
    
    sct_map2D = TH2D("sct_occ_map2d","occupancies in percent (200 pileup)",(240/2),-3100,3100,(160/2),0.,1100.)

    saveValue = 0
    saveYindex = 0
    saveXindex = 0
    content = 0

    for i in range(barrelPlot.GetXaxis().GetNbins()):
       for j in range(barrelPlot.GetYaxis().GetNbins()):

          if barrelPlot.GetBinContent(i,j) != 0:
             content = barrelPlot.GetBinContent(i,j)
             sct_map2D.Fill(barrelPlot.GetXaxis().GetBinCenter(i), barrelPlot.GetYaxis().GetBinCenter(j), content)
             #sct_map2D.Fill(barrelPlot.GetXaxis().GetBinCenter(i), barrelPlot.GetYaxis().GetBinCenter(j), 0.1)

          if barrelPlot.GetBinContent(i,j) == 0 and barrelPlot.GetBinContent(i-1,j) != 0 and barrelPlot.GetBinContent(i+1,j) != 0:
             sumocc = 0.5*(barrelPlot.GetBinContent(i+1,j)  + barrelPlot.GetBinContent(i-1,j))
             sct_map2D.Fill(barrelPlot.GetXaxis().GetBinCenter(i), barrelPlot.GetYaxis().GetBinCenter(j), sumocc)
             #sct_map2D.Fill(barrelPlot.GetXaxis().GetBinCenter(i), barrelPlot.GetYaxis().GetBinCenter(j), 0.1)

    
    for i in range(sct_map2D.GetXaxis().GetNbins()):
       for j in range(sct_map2D.GetYaxis().GetNbins()):

          if sct_map2D.GetBinContent(i,j) == 0 and sct_map2D.GetBinContent(i-1,j) != 0 and sct_map2D.GetBinContent(i+2,j) != 0:
             sumocc = 0.5*(sct_map2D.GetBinContent(i+2,j)  + sct_map2D.GetBinContent(i-1,j))
             sct_map2D.Fill(sct_map2D.GetXaxis().GetBinCenter(i), sct_map2D.GetYaxis().GetBinCenter(j), sumocc)

    '''

    hack the barrel....

    for i in range(barrelPlot.GetXaxis().GetNbins()):
       for j in range(barrelPlot.GetYaxis().GetNbins()):

          if barrelPlot.GetBinContent(i,j) == 0 and barrelPlot.GetBinContent(i-1,j) != 0 and barrelPlot.GetBinContent(i+1,j) != 0:
             sumocc = 0.5*(barrelPlot.GetBinContent(i+1,j)  + barrelPlot.GetBinContent(i-1,j))
             barrelPlot.Fill(barrelPlot.GetXaxis().GetBinCenter(i), barrelPlot.GetYaxis().GetBinCenter(j), sumocc)
             #sct_map2D.Fill(barrelPlot.GetXaxis().GetBinCenter(i), barrelPlot.GetYaxis().GetBinCenter(j), 0.1)

    '''


    # now we do the nasty hack for the end cap...

    '''
    saveXindex = 10000000

    # require the +10 or you miss the final disk..
    for i in range(diskPlot.GetXaxis().GetNbins()+10):
       for j in range(diskPlot.GetYaxis().GetNbins()+10):

            if diskPlot.GetBinContent(i,j) != 0:
                contentV2 = diskPlot.GetBinContent(i,j)
                saveXindex = i

            if diskPlot.GetBinContent(i,j) == 0 and j < 36:
                if i == saveXindex:
                    diskPlot.Fill(diskPlot.GetXaxis().GetBinCenter(i),diskPlot.GetYaxis().GetBinCenter(j),contentV2)
    '''

    # hack the end cap....


    sct_diskmap2D = TH2D("sct_disk_occ_map2d","occupancies in percent (200 pileup)",(240/2),-3100,3100,(160/2),0.,1100.)

    saveValue = 0
    saveYindex = 0
    saveXindex = 0
    contentV2 = 0

    for i in range(diskPlot.GetXaxis().GetNbins()):
       for j in range(diskPlot.GetYaxis().GetNbins()):

          #print 'indices' , i, j 

          if diskPlot.GetBinContent(i,j) != 0:
              #print 'indices' , i, j 
              contentV2 = diskPlot.GetBinContent(i,j)
              print 'indices' , i, j , contentV2
              sct_diskmap2D.Fill(diskPlot.GetXaxis().GetBinCenter(i), diskPlot.GetYaxis().GetBinCenter(j), contentV2)
              #sct_diskmap2D.Fill(diskPlot.GetXaxis().GetBinCenter(i), diskPlot.GetYaxis().GetBinCenter(j), 0.1)
              saveXindex = i 

          if diskPlot.GetBinContent(i,j) == 0 and j < 70:
              if i == saveXindex:
                sct_diskmap2D.Fill(diskPlot.GetXaxis().GetBinCenter(i), diskPlot.GetYaxis().GetBinCenter(j), contentV2)
                #sct_diskmap2D.Fill(diskPlot.GetXaxis().GetBinCenter(i), diskPlot.GetYaxis().GetBinCenter(j), 0.1)

    sct_map2D.Add(sct_diskmap2D)
    #sct_map2D.GetXaxis().SetRangeUser(0, 3100)  
    #sct_map2D.GetYaxis().SetRangeUser(0,1500)

    #sct_map2D.Draw("COLZ")
    #strip barrel, adds disk plot
    #barrelPlot.Add(diskPlot)

    # insert pixel plot here!
    pixbarrelPlot = TH2D()
    normpixbarrelPlot = TH2D()
    pixels.GetObject("pix_barrel_occ_map2DV2", pixbarrelPlot);
    pixbarrelPlot.SetDirectory(0)
    pixels.GetObject("pix_barrel_occ_map2DV2_norm", normpixbarrelPlot);
    normpixbarrelPlot.SetDirectory(0)

    #normalise plot in the case of double counting in a bin
    pixbarrelPlot.Divide(normpixbarrelPlot)

    # end - caps
    pixdiskPlot = TH2D()
    normpixdiskPlot = TH2D()

    pixels.GetObject("pix_disk_occ_map2DV2", pixdiskPlot);
    pixdiskPlot.SetDirectory(0)

    pixels.GetObject("pix_disk_occ_map2DV2_norm", normpixdiskPlot);
    normpixdiskPlot.SetDirectory(0)

    # divide in case of double counting
    pixdiskPlot.Divide(normpixdiskPlot)
    

    sct_map2D.GetXaxis().SetRangeUser(0, 3100)  
    sct_map2D.GetYaxis().SetRangeUser(0,1500)

    sct_map2D.Add(pixbarrelPlot)
    sct_map2D.Add(pixdiskPlot)
    #barrelPlot.Scale(1/100.0)

    #sct_map2D.Draw("COLZ")
    #c1.Update()

    sct_map2D.SetXTitle("z [mm]")
    sct_map2D.SetYTitle("R [mm]")

    c1.SetRightMargin(0.18)
    c1.SetLeftMargin(0.15)

    y_label = 0.88
    t = TLatex()
    t.SetTextAngle(90)
    t.SetNDC()

    #barrelPlot.SetMaximum(1.3)   

    sct_map2D.Draw("COLZ")
    c1.Update()

    t.DrawLatex(0.97, 0.4, "Channel Occupancy in %")


    ATLAS_LABEL(0.35,0.9); myText(0.47,0.9,1,"Simulation Preliminary");  
    myText(0.5,0.85,1,"ITk Inclined");

    c1.SaveAs("plots/%s.pdf" %(saveName))
    c1.SaveAs("plots/%s.eps" %(saveName))
    c1.SaveAs("plots/%s.png" %(saveName))








    # the above is the default palette...
    # try custom palette also...

    

    NRGBs = 5                                                                                                                                           
    NCont = 255                                                                                                                                        
    stops = [ 0.00, 0.34, 0.61, 0.84, 1.00 ]                                                                                                             
    red = [ 0.00, 0.00, 0.87, 1.00, 0.51 ]                                                                                                               
    green = [ 0.00, 0.81, 1.00, 0.20, 0.00 ]                                                                                                            
    blue = [ 0.51, 1.00, 0.12, 0.00, 0.00 ]                                                                                                
    stopsArray = array('d', stops)                                                                                                                       
    redArray   = array('d', red)                                                                                                                        
    greenArray = array('d', green)                                                                                                                      
    blueArray  = array('d', blue)                                                                                                                       
    TColor.CreateGradientColorTable(NRGBs, stopsArray, redArray, greenArray, blueArray, NCont)                                                           
    ROOT.gStyle.SetNumberContours(NCont)


    sct_map2D.SetMaximum(3.5)


    sct_map2D.Draw("COLZ")
    c1.Update()

    t.DrawLatex(0.97, 0.4, "Channel Occupancy in %")


    ATLAS_LABEL(0.35,0.9); myText(0.47,0.9,1,"Simulation Preliminary");
    myText(0.5,0.85,1,"ITk Inclined");


    #ATLAS_LABEL(0.20,0.89); myText(0.32,0.89,1,"Simulation Preliminary, ITk Inclined");
    #myText(0.45,0.851,1,"minimum bias, <#mu> = 200");

    c1.SaveAs("plots/%s.pdf" %(saveName))
    c1.SaveAs("plots/%s.eps" %(saveName))
    c1.SaveAs("plots/%s.png" %(saveName))









main()

    
