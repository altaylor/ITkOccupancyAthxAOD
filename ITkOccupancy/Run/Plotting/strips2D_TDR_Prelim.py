
'''
from ROOT import *
ROOT.gROOT.LoadMacro("AtlasStyle.C")
ROOT.gROOT.LoadMacro("AtlasLabels.C")
ROOT.gROOT.LoadMacro("AtlasUtils.C")

SetAtlasStyle()

#ROOT.gStyle.SetOptStat(True) #turn off stats boxes
ROOT.gStyle.SetMarkerSize(1.5)
ROOT.gStyle.SetMarkerStyle(20)
ROOT.gStyle.SetFillColor(0)
ROOT.gStyle.SetPalette(1)

from array import array
import numpy as np
import scipy
import sys
#import SetPalette

ROOT.gStyle.SetOptStat(False)

'''

from ROOT import *
ROOT.gROOT.LoadMacro("AtlasStyle.C")
ROOT.gROOT.LoadMacro("AtlasLabels.C")
ROOT.gROOT.LoadMacro("AtlasUtils.C")

SetAtlasStyle()

ROOT.gStyle.SetOptStat(False)
ROOT.gStyle.SetMarkerSize(1.5)

                                                                                                                             
from array import array                                                                                                       
import numpy as np                                                                                                            
import scipy                                                                                                                  
import sys  


c1 = TCanvas()
signal = TFile("/afs/cern.ch/work/a/altaylor/itk_occupancy_svn2/ITkOccupancy/ITkOccupancy/ITkOccupancy/run/Plotting/avgStudies_step15.root")

# produce custom palette!

NRGBs = 5
NCont = 255

stops = [ 0.00, 0.34, 0.61, 0.84, 1.00 ]
red = [ 0.00, 0.00, 0.87, 1.00, 0.51 ]
green = [ 0.00, 0.81, 1.00, 0.20, 0.00 ]
blue = [ 0.51, 1.00, 0.12, 0.00, 0.00 ]

stopsArray = array('d', stops)
redArray   = array('d', red)
greenArray = array('d', green)
blueArray  = array('d', blue)

TColor.CreateGradientColorTable(NRGBs, stopsArray, redArray, greenArray, blueArray, NCont)
ROOT.gStyle.SetNumberContours(NCont)

def main():
            
    # pass four plots & y-axis name & saveName
    Plot2DOccupancy("sct_barrel_occ_map2D", "sct_barrel_occ_map2D_norm", "sct_disk_occ_map2D", "sct_disk_occ_map2D_norm", "Strip Channel Occupancy %", "occ_total")
    Plot2DOccupancy("sct_barrel_hitmap2D", "sct_barrel_hitmap2D_norm", "sct_disk_hitmap2D", "sct_disk_hitmap2D_norm", "Strip hits / mm^{2}",  "hit_map_total")


    
def Plot2DOccupancy(barrelmap, barrelmap_norm, diskmap, diskmap_norm, yaxisName, saveName):

    # grab the SCT barrel plots and normalise it.

    barrelPlot = TH2D()
    normbarrelPlot = TH2D()
    signal.GetObject(barrelmap, barrelPlot);
    barrelPlot.SetDirectory(0)
    signal.GetObject(barrelmap_norm, normbarrelPlot);
    normbarrelPlot.SetDirectory(0)

    #normalise plot in the case of double counting in a bin
    barrelPlot.Divide(normbarrelPlot)
    # grab the SCT end-cap plots and normalise it.
    diskPlot = TH2D()
    normdiskPlot = TH2D()
    
    signal.GetObject(diskmap, diskPlot);
    diskPlot.SetDirectory(0)
    signal.GetObject(diskmap_norm, normdiskPlot);
    normdiskPlot.SetDirectory(0)
    # normalise plot in case of double counting
    diskPlot.Divide(normdiskPlot)

    # here we do the nasty hack for the barrel..

    for i in range(barrelPlot.GetXaxis().GetNbins()):
       for j in range(barrelPlot.GetYaxis().GetNbins()):

          if barrelPlot.GetBinContent(i,j) == 0 and barrelPlot.GetBinContent(i-1,j) != 0 and barrelPlot.GetBinContent(i+1,j) != 0:
             sumocc = 0.5*(barrelPlot.GetBinContent(i+1,j)  + barrelPlot.GetBinContent(i-1,j))
             barrelPlot.Fill(barrelPlot.GetXaxis().GetBinCenter(i), barrelPlot.GetYaxis().GetBinCenter(j), sumocc)
             #sct_map2D.Fill(barrelPlot.GetXaxis().GetBinCenter(i), barrelPlot.GetYaxis().GetBinCenter(j), 0.1)


    # now we do the nasty hack for the end cap...
    saveXindex = 10000000

    # require the +10 or you miss the final disk..
    for i in range(diskPlot.GetXaxis().GetNbins()+10):
       for j in range(diskPlot.GetYaxis().GetNbins()+10):

            if diskPlot.GetBinContent(i,j) != 0:
                contentV2 = diskPlot.GetBinContent(i,j)
                saveXindex = i

            if diskPlot.GetBinContent(i,j) == 0 and j < 36:
                if i == saveXindex:
                    diskPlot.Fill(diskPlot.GetXaxis().GetBinCenter(i),diskPlot.GetYaxis().GetBinCenter(j),contentV2)

    
    diskPlot.GetYaxis().SetRangeUser(350, 1100)
    diskPlot.GetXaxis().SetRangeUser(0, 3100)
    
    barrelPlot.GetYaxis().SetRangeUser(350, 1100)
    barrelPlot.GetXaxis().SetRangeUser(0, 3100)

    barrelPlot.Add(diskPlot)    
    barrelPlot.Draw("COLZ")


    
    barrelPlot.SetXTitle("z [mm]")
    barrelPlot.SetYTitle("R [mm]")


    y_label = 0.88
    t = TLatex()
    t.SetTextAngle(90)
    t.SetNDC()

    if yaxisName == "Strip hits / mm^{2}":
          t.DrawLatex(0.98, 0.32, yaxisName)

    else:
        t.DrawLatex(0.97, 0.35, yaxisName)


    c1.SetRightMargin(0.18)
    c1.SetLeftMargin(0.15)


    ATLAS_LABEL(0.20-0.02,0.88); myText(0.32-0.02,0.88,1,"Simulation Preliminary, ITk Inclined");
    myText(0.45,0.83,1,"minimum bias, <#mu> = 200");


    c1.Update()


    '''
    c1.SaveAs("plots/variables/%s.pdf" %(saveName))
    c1.SaveAs("plots/variables/%s.eps" %(saveName))
    '''


    c1.SaveAs("plots/strips/%s.pdf" %(saveName))                                                  
    c1.SaveAs("plots/strips/%s.eps" %(saveName))
    c1.SaveAs("plots/strips/%s.png" %(saveName))


main()

    
