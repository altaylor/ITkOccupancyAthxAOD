#-- start of make_header -----------------

#====================================
#  Document ITkOccupancyMergeComponentsList
#
#   Generated Mon Apr 10 13:50:07 2017  by altaylor
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_ITkOccupancyMergeComponentsList_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_ITkOccupancyMergeComponentsList_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_ITkOccupancyMergeComponentsList

ITkOccupancy_tag = $(tag)

#cmt_local_tagfile_ITkOccupancyMergeComponentsList = $(ITkOccupancy_tag)_ITkOccupancyMergeComponentsList.make
cmt_local_tagfile_ITkOccupancyMergeComponentsList = $(bin)$(ITkOccupancy_tag)_ITkOccupancyMergeComponentsList.make

else

tags      = $(tag),$(CMTEXTRATAGS)

ITkOccupancy_tag = $(tag)

#cmt_local_tagfile_ITkOccupancyMergeComponentsList = $(ITkOccupancy_tag).make
cmt_local_tagfile_ITkOccupancyMergeComponentsList = $(bin)$(ITkOccupancy_tag).make

endif

include $(cmt_local_tagfile_ITkOccupancyMergeComponentsList)
#-include $(cmt_local_tagfile_ITkOccupancyMergeComponentsList)

ifdef cmt_ITkOccupancyMergeComponentsList_has_target_tag

cmt_final_setup_ITkOccupancyMergeComponentsList = $(bin)setup_ITkOccupancyMergeComponentsList.make
cmt_dependencies_in_ITkOccupancyMergeComponentsList = $(bin)dependencies_ITkOccupancyMergeComponentsList.in
#cmt_final_setup_ITkOccupancyMergeComponentsList = $(bin)ITkOccupancy_ITkOccupancyMergeComponentsListsetup.make
cmt_local_ITkOccupancyMergeComponentsList_makefile = $(bin)ITkOccupancyMergeComponentsList.make

else

cmt_final_setup_ITkOccupancyMergeComponentsList = $(bin)setup.make
cmt_dependencies_in_ITkOccupancyMergeComponentsList = $(bin)dependencies.in
#cmt_final_setup_ITkOccupancyMergeComponentsList = $(bin)ITkOccupancysetup.make
cmt_local_ITkOccupancyMergeComponentsList_makefile = $(bin)ITkOccupancyMergeComponentsList.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)ITkOccupancysetup.make

#ITkOccupancyMergeComponentsList :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'ITkOccupancyMergeComponentsList'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = ITkOccupancyMergeComponentsList/
#ITkOccupancyMergeComponentsList::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/merge_componentslist_header
# Author: Sebastien Binet (binet@cern.ch)

# Makefile fragment to merge a <library>.components file into a single
# <project>.components file in the (lib) install area
# If no InstallArea is present the fragment is dummy


.PHONY: ITkOccupancyMergeComponentsList ITkOccupancyMergeComponentsListclean

# default is already '#'
#genmap_comment_char := "'#'"

componentsListRef    := ../$(tag)/ITkOccupancy.components

ifdef CMTINSTALLAREA
componentsListDir    := ${CMTINSTALLAREA}/$(tag)/lib
mergedComponentsList := $(componentsListDir)/$(project).components
stampComponentsList  := $(componentsListRef).stamp
else
componentsListDir    := ../$(tag)
mergedComponentsList :=
stampComponentsList  :=
endif

ITkOccupancyMergeComponentsList :: $(stampComponentsList) $(mergedComponentsList)
	@:

.NOTPARALLEL : $(stampComponentsList) $(mergedComponentsList)

$(stampComponentsList) $(mergedComponentsList) :: $(componentsListRef)
	@echo "Running merge_componentslist  ITkOccupancyMergeComponentsList"
	$(merge_componentslist_cmd) --do-merge \
         --input-file $(componentsListRef) --merged-file $(mergedComponentsList)

ITkOccupancyMergeComponentsListclean ::
	$(cleanup_silent) $(merge_componentslist_cmd) --un-merge \
         --input-file $(componentsListRef) --merged-file $(mergedComponentsList) ;
	$(cleanup_silent) $(remove_command) $(stampComponentsList)
libITkOccupancy_so_dependencies = ../x86_64-slc6-gcc49-opt/libITkOccupancy.so
#-- start of cleanup_header --------------

clean :: ITkOccupancyMergeComponentsListclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(ITkOccupancyMergeComponentsList.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

ITkOccupancyMergeComponentsListclean ::
#-- end of cleanup_header ---------------
