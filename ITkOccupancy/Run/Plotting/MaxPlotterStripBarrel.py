from ROOT import *
ROOT.gROOT.LoadMacro("AtlasStyle.C")
ROOT.gROOT.LoadMacro("AtlasLabels.C")
ROOT.gROOT.LoadMacro("AtlasUtils.C")

SetAtlasStyle()

#ROOT.gStyle.SetOptStat(True)

#signal = TFile("/afs/cern.ch/user/a/altaylor/UpgradeITK_V2/InnerDetector/InDetPerformance/InDetUpgradePerformanceAnalysis/Plotting/MaxStudies_step15_corrected.root")
#the below is for the plots that were presented at itk meetings..

#signal = TFile("/afs/cern.ch/work/a/altaylor/itk_max_ttbar/user.altaylor.maxoccwant.step15_MYSTREAM/ttbar_max_occ.root")

#new file... now recording differently for the end cap
#signal = TFile("/afs/cern.ch/work/a/altaylor/itk_tdr/user.altaylor.maxoccv_striptdr.step15_MYSTREAM/ttbar_max_strip_tdr.root")

signal = TFile("/afs/cern.ch/work/a/altaylor/fix_itk_ttbar_max_occ/user.altaylor.maxoccv_striptdr_endcapfixV4.step15_MYSTREAM/merged_ttbar_study.root")



signalTree = signal.Get("myTree")

#tester

def main():

    cut = "maxHits_0 > -1"

    Plotter("maxHits_0", "maxHits_1", "maxHits_2", "maxHits_3", 60, 0.5, 600.5, "Maximum Hits in Strip Sensor per Event", "barrel_hits",cut, "Fraction of Events / 10 Hits ")
    Plotter("maxClusters_0", "maxClusters_1", "maxClusters_2", "maxClusters_3", 100, 0.5, 100.5, "Maximum Clusters in Strip Sensor per Event", "barrel_clusters",cut, "Fraction of Events / Cluster")
    Plotter("maxOccupancy_0", "maxOccupancy_1", "maxOccupancy_2", "maxOccupancy_3", 20, 0.0, 20, "Maximum Channel Occupancy (%) for Strip Sensor per Event", "barrel_channel_occ",cut, "Fraction of Events / 1%")
    Plotter("maxHitDensity_0", "maxHitDensity_1", "maxHitDensity_2", "maxHitDensity_3", 20, 0.0, 0.06, "Maximum Hit Density (Hits/mm^{2}) in Strip Sensor per Event", "barrel_hit_density",cut, "Fraction of Events / 0.003 (Hits/mm^{2})")

    '''

    PlotterV2("maxEndCapHits_0", "maxEndCapHits_1", "maxEndCapHits_2", "maxEndCapHits_3","maxEndCapHits_4","maxEndCapHits_5", 50, 0.5, 500.5, "Maximum Hits in Sensor per Event", "endcap_hits",cut, "Entries / 10 Hits ")
    PlotterV2("maxEndCapClusters_0", "maxEndCapClusters_1", "maxEndCapClusters_2", "maxEndCapClusters_3", "maxEndCapClusters_4","maxEndCapClusters_5", 100, 0.5, 100.5, "Maximum Clusters in Sensor per Event", "endcap_clusters",cut, "Entries")
    PlotterV2("maxEndCapOccupancy_0", "maxEndCapOccupancy_1", "maxEndCapOccupancy_2", "maxEndCapOccupancy_3", "maxEndCapOccupancy_4","maxEndCapOccupancy_5",25, 0.0, 25, "Maximum Channel Occupancy (%) for Sensor per Event", "endcap_channel_occ",cut, "Entries / 1%")
    PlotterV2("maxEndCapHitDensity_0", "maxEndCapHitDensity_1", "maxEndCapHitDensity_2", "maxEndCapHitDensity_3","maxEndCapHitDensity_4", "maxEndCapHitDensity_5", 20, 0.0, 0.08, "Maximum Hit Density (Hits/mm^{2}) in Sensor per Event", "endcap_hit_density",cut, "Entries / 0.004")


    

    '''
    #TH1D *h_hits = new TH1D(Form("ev%i_layer_%i_%s_hits", nEvents, layer, elem_str), "hits in event", 750, 0.5, 750.5);


    

def Plotter(var1,var2,var3,var4, nbins, lbin, ubin,axisname,savename,cut,yaxis):

    h_0 = TH1F("h_0", "", nbins, lbin, ubin)
    h_1 = TH1F("h_1", "", nbins, lbin, ubin)
    h_2 = TH1F("h_2", "", nbins, lbin, ubin)
    h_3 = TH1F("h_3", "", nbins, lbin, ubin)

    print ' variable name ? ' , var1

    max0 = signalTree.GetMaximum(var1)
    max1 = signalTree.GetMaximum(var2)  
    max2 = signalTree.GetMaximum(var3)
    max3 = signalTree.GetMaximum(var4)

    print ' h_0? max ' , max0
    print ' h_1? max ' , max1
    print ' h_2? max' , max2
    print ' h_3? max' , max3
    
    
    signalTree.Draw("%s >> h_0" %var1, "%s" %cut)
    signalTree.Draw("%s >> h_1" %var2, "%s" %cut)
    signalTree.Draw("%s >> h_2" %var3, "%s" %cut)
    signalTree.Draw("%s >> h_3" %var4, "%s" %cut)

    print ' h_0? mean ' , h_0.GetMean()
    print ' h_1? mean ' , h_1.GetMean()
    print ' h_2? mean' , h_2.GetMean()
    print ' h_3? mean' , h_3.GetMean()
    
    
    h_0.SetLineColor(kBlack)
    h_1.SetLineColor(kRed)
    h_2.SetLineColor(kGreen)
    h_3.SetLineColor(kBlue)

    h_0.SetLineWidth(4)
    h_1.SetLineWidth(4)
    h_2.SetLineWidth(4)
    h_3.SetLineWidth(4)

    
    
    h_0.SetLineStyle(3)
    h_1.SetLineStyle(7)
    h_2.SetLineStyle(9)
    h_3.SetLineStyle(6)

    h_0.Scale(1.0/h_0.Integral())
    h_1.Scale(1.0/h_1.Integral())
    h_2.Scale(1.0/h_2.Integral())
    h_3.Scale(1.0/h_3.Integral())

    

    h_0.Draw()
    h_1.Draw("same")
    h_2.Draw("same")
    h_3.Draw("same")
    
    h_0.SetTitle(axisname)
    h_0.GetXaxis().SetTitleOffset(0.9)
    h_0.GetXaxis().SetTitle("%s" %axisname)
    h_0.GetYaxis().SetTitle(yaxis)
    
    leg = TLegend(0.68,0.4,0.93,0.60)
    leg.SetLineColor(kWhite)
    leg.SetFillColor(kWhite)
    leg.SetTextSize(0.05)
    leg.SetBorderSize(0)

    
    max_height = 0

    
    for hist in [h_0, h_1, h_2, h_3]:
        if hist.GetMaximum() > max_height:
            max_height = hist.GetMaximum()

    h_0.GetYaxis().SetRangeUser(0,1.2*max_height)

    leg.AddEntry(h_0,"Layer 0","l")
    leg.AddEntry(h_1,"Layer 1","l")
    leg.AddEntry(h_2,"Layer 2","l")
    leg.AddEntry(h_3,"Layer 3","l")

    #ATLAS_LABEL(0.6,0.8); myText(0.72,0.8,1,"Internal");
    #myText(0.6,0.75,1,"Fully Inclined 4.0");
    #myText(0.6,0.7,1,"Fully Inclined 4.0");


    #ATLAS_LABEL(0.55,0.85); myText(0.67,0.85,1,"Simulation Internal");
    #myText(0.55,0.80,1,"ITk Inclined");
    #myText(0.55,0.75,1,"t#bar{t}, <#mu> = 200");

    if savename == "barrel_hits":
        ATLAS_LABEL(0.20,0.85+0.038); myText(0.32,0.85+0.038,1,"Simulation Internal");
        myText(0.20,0.80+0.038,1,"ITk Inclined, Strip Barrel");
        myText(0.20,0.75+0.038,1,"t#bar{t}, <#mu> = 200");


        

        
    elif savename == "barrel_clusters":
        ATLAS_LABEL(0.20,0.85+0.038); myText(0.32,0.85+0.038,1,"Simulation Internal");
        myText(0.20,0.80+0.038,1,"ITk Inclined, Strip Barrel");
        myText(0.20,0.75+0.038,1,"t#bar{t}, <#mu> = 200");

        
    elif savename == "barrel_channel_occ":
        ATLAS_LABEL(0.45,0.85); myText(0.62-0.05,0.85,1,"Simulation Internal");
        myText(0.45,0.80,1,"ITk Inclined, Strip Barrel");
        myText(0.45,0.75,1,"t#bar{t}, <#mu> = 200");

                
    elif savename == "barrel_hit_density":
        ATLAS_LABEL(0.20,0.85+0.038); myText(0.32,0.85+0.038,1,"Simulation Internal");
        myText(0.20,0.80+0.038,1,"ITk Inclined, Strip Barrel");
        myText(0.20,0.75+0.038,1,"t#bar{t}, <#mu> = 200");

        


        

    '''
    
    else:
        ATLAS_LABEL(0.20,0.85); myText(0.32,0.85,1,"Simulation Internal");
        myText(0.20,0.80,1,"ITk Inclined, Strip Barrel");
        myText(0.20,0.75,1,"t#bar{t}, <#mu> = 200");

    '''

    


    #hello
    #myText(0.46,0.75,1,"\mathrm{<\mu> = 200}");


    gPad.Print("label.png")

    
    leg.Draw()

    #c1.SaveAs(savename)

    # save plots as eps and .pdf
    pdf_string = savename + ".pdf"
    eps_string = savename + ".eps"

    c1.SaveAs(pdf_string)
    c1.SaveAs(eps_string)


    '''

    
    c1.SaveAs("plots/strips/barrel/%s.pdf" %(savename))
    c1.SaveAs("plots/strips/barrel/%s.eps" %(savename))
    c1.SaveAs("plots/strips/barrel/%s.png" %(savename))

    

    '''

       
    c1.SaveAs("plots/strips/max_studies/%s.pdf" %(savename))
    c1.SaveAs("plots/strips/max_studies/%s.eps" %(savename))
    c1.SaveAs("plots/strips/max_studies/%s.png" %(savename))

    





    
def PlotterV2(var1,var2,var3,var4,var5,var6, nbins, lbin, ubin,axisname,savename,cut,yaxis):

    h_0 = TH1F("h_0", "", nbins, lbin, ubin)
    h_1 = TH1F("h_1", "", nbins, lbin, ubin)
    h_2 = TH1F("h_2", "", nbins, lbin, ubin)
    h_3 = TH1F("h_3", "", nbins, lbin, ubin)
    h_4 = TH1F("h_4", "", nbins, lbin, ubin)
    h_5 = TH1F("h_5", "", nbins, lbin, ubin)

    print ' variable name ? ' , var1

    max0 = signalTree.GetMaximum(var1)
    max1 = signalTree.GetMaximum(var2)  
    max2 = signalTree.GetMaximum(var3)
    max3 = signalTree.GetMaximum(var4)

    max4 = signalTree.GetMaximum(var5)
    max5 = signalTree.GetMaximum(var6)


    print ' h_0? max ' , max0
    print ' h_1? max ' , max1
    print ' h_2? max' , max2
    print ' h_3? max' , max3

    print ' h_4? max' , max4
    print ' h_5? max' , max5
    
    
    signalTree.Draw("%s >> h_0" %var1, "%s" %cut)
    signalTree.Draw("%s >> h_1" %var2, "%s" %cut)
    signalTree.Draw("%s >> h_2" %var3, "%s" %cut)
    signalTree.Draw("%s >> h_3" %var4, "%s" %cut)

    signalTree.Draw("%s >> h_4" %var5, "%s" %cut)
    signalTree.Draw("%s >> h_5" %var6, "%s" %cut)

    print ' h_0? mean ' , h_0.GetMean()
    print ' h_1? mean ' , h_1.GetMean()
    print ' h_2? mean' , h_2.GetMean()
    print ' h_3? mean' , h_3.GetMean()

    print ' h_4? mean' , h_4.GetMean()
    print ' h_5? mean' , h_5.GetMean()
    
    
    h_0.SetLineColor(kBlack)
    h_1.SetLineColor(kRed)
    h_2.SetLineColor(kGreen)
    h_3.SetLineColor(kBlue)
    h_4.SetLineColor(7)
    h_5.SetLineColor(6)

    h_0.SetLineWidth(2)
    h_1.SetLineWidth(2)
    h_2.SetLineWidth(2)
    h_3.SetLineWidth(2)

    h_4.SetLineWidth(2)
    h_5.SetLineWidth(2)

    h_0.Draw()
    h_1.Draw("same")
    h_2.Draw("same")
    h_3.Draw("same")
    h_4.Draw("same")
    h_5.Draw("same")

    
    
    
    h_0.SetTitle(axisname)
    h_0.GetXaxis().SetTitleOffset(0.9)
    h_0.GetXaxis().SetTitle("%s" %axisname)
    h_0.GetYaxis().SetTitle(yaxis)
    
    leg = TLegend(0.70,0.35,0.90,0.60)
    leg.SetLineColor(kWhite)
    leg.SetFillColor(kWhite)
    leg.SetTextSize(0.05)
    leg.SetBorderSize(0)

    
    max_height = 0

    
    for hist in [h_0, h_1, h_2, h_3,h_4,h_5]:
        if hist.GetMaximum() > max_height:
            max_height = hist.GetMaximum()

    h_0.GetYaxis().SetRangeUser(0,1.2*max_height)

    leg.AddEntry(h_0,"Disk 0","l")
    leg.AddEntry(h_1,"Disk 1","l")
    leg.AddEntry(h_2,"Disk 2","l")
    leg.AddEntry(h_3,"Disk 3","l")
    leg.AddEntry(h_4,"Disk 4","l")
    leg.AddEntry(h_5,"Disk 5","l")

    #ATLAS_LABEL(0.6,0.8); myText(0.72,0.8,1,"Internal");
    #myText(0.6,0.75,1,"Fully Inclined 4.0");
    #myText(0.6,0.7,1,"Fully Inclined 4.0");

    ATLAS_LABEL(0.3,0.85); myText(0.42,0.85,1,"Simulation Internal");
    myText(0.3,0.80,1,"ITk Inclined");
    myText(0.3,0.75,1,"minimum bias, <#mu> = 200");
    #myText(0.46,0.75,1,"\mathrm{<\mu> = 200}");



    gPad.Print("label.png")
    
    leg.Draw()

    #pdf_string = savename + ".pdf"
    #eps_string = savename + ".eps"
    #hello

    #c1.SaveAs(pdf_string)
    #c1.SaveAs(eps_string)

    '''
    c1.SaveAs("plots/strips/max_barrel/%s.pdf" %(savename))
    c1.SaveAs("plots/strips/max_barrel/%s.eps" %(savename))
    c1.SaveAs("plots/strips/max_barrel/%s.png" %(savename))
    '''






main()

#ATLAS_LABEL(0.2,0.8)
#first argument is where the text starts on the x-axis.
#second argument is where the test starts on the y-axis
#myText(0.2,0.75,1,"Internal")


#c1.SetLogy()
#c1.SaveAs("tester.png")
