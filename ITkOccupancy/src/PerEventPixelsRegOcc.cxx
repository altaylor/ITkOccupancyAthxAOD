// MyPackage includes
#include "PerEventPixelsRegOcc.h"

#include <math.h>

// MyPackage includes
#include "xAODJet/JetContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetAuxContainer.h"

#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "xAODTracking/TrackMeasurementValidationAuxContainer.h"

#include "xAODTracking/TrackStateValidationContainer.h"
#include "xAODTracking/TrackStateValidationAuxContainer.h"

//#include "xAODJet/JetAuxContainer.h"

#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackParticleAuxContainer.h"

#include "TVector.h"

#include "GaudiKernel/ITHistSvc.h"
#include "Counters.h"


#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TVector3.h"
#include "TH2D.h"

#include <math.h>




PerEventPixelsRegOcc::PerEventPixelsRegOcc( const std::string& name, ISvcLocator* pSvcLocator ) : AthAlgorithm( name, pSvcLocator ){

  //declareProperty( "Property", m_nProperty ); //example property declaration

  //hello

}


PerEventPixelsRegOcc::~PerEventPixelsRegOcc() {}


StatusCode PerEventPixelsRegOcc::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");

  ServiceHandle<ITHistSvc> histSvc("THistSvc",name()); 
  CHECK( histSvc.retrieve() );

   myTree = new TTree("myTree","myTree");
  CHECK( histSvc->regTree("/MYSTREAM/myTree",myTree) );

  myTree->Branch("maxHits_chip_0", &maxHits_chip_0);
  myTree->Branch("maxClusters_chip_0", &maxClusters_chip_0);
  myTree->Branch("maxHits_chip_1", &maxHits_chip_1);
  myTree->Branch("maxClusters_chip_1", &maxClusters_chip_1);  
  myTree->Branch("maxHits_chip_2", &maxHits_chip_2);
  myTree->Branch("maxClusters_chip_2", &maxClusters_chip_2);
  myTree->Branch("maxHits_chip_3", &maxHits_chip_3);
  myTree->Branch("maxClusters_chip_3", &maxClusters_chip_3);
  myTree->Branch("maxHits_chip_4", &maxHits_chip_4);
  myTree->Branch("maxClusters_chip_4", &maxClusters_chip_4);


  
  myTree->Branch("maxHits_sensor_0", &maxHits_sensor_0);
  myTree->Branch("maxClusters_sensor_0", &maxClusters_sensor_0);
  myTree->Branch("maxHits_sensor_1", &maxHits_sensor_1);
  myTree->Branch("maxClusters_sensor_1", &maxClusters_sensor_1);  
  myTree->Branch("maxHits_sensor_2", &maxHits_sensor_2);
  myTree->Branch("maxClusters_sensor_2", &maxClusters_sensor_2);
  myTree->Branch("maxHits_sensor_3", &maxHits_sensor_3);
  myTree->Branch("maxClusters_sensor_3", &maxClusters_sensor_3);
  myTree->Branch("maxHits_sensor_4", &maxHits_sensor_4);
  myTree->Branch("maxClusters_sensor_4", &maxClusters_sensor_4);


  

  myHist = new TH1D("myHist","myHist",100,0,100);
  histSvc->regHist("/MYSTREAM/myHist",myHist).ignore();

  // plots for the local X / Y variables
  
  localY_0 = new TH1D("localY_0","localY_0",50,-25.0,25.0);
  localY_0_inclined = new TH1D("localY_0_inclined","localY_0_inclined",50,-25.0,25.0);
  localY_gr0 = new TH1D("localY_gr0","localY_gr0",50,-25.0,25.0);
  localY_gr0_inclined = new TH1D("localY_gr0_inclined","localY_gr0_inclined",50,-25.0,25.0);
  
  localX_0 = new TH1D("localX_0","localX_0",50,-25.0,25.0);
  localX_0_inclined = new TH1D("localX_0_inclined","localX_0_inclined",50,-25.0,25.0);
  localX_gr0 = new TH1D("localX_gr0","localX_gr0",50,-25.0,25.0);
  localX_gr0_inclined = new TH1D("localX_gr0_inclined","localX_gr0_inclined",50,-25.0,25.0);

  // histograms for each different layer..

  h_clus_chip_0 = new TH1D("h_clus_chip_0","h_clus_chip_0",100,0.5,100.5);
  h_clus_sensor_0 = new TH1D("h_clus_sensor_0","h_clus_sensor_0",200,0.5,200.5);    
  h_hits_chip_0 = new TH1D("h_hits_chip_0","h_hits_chip_0",1000,0.5,1000.5);
  h_hits_sensor_0 = new TH1D("h_hits_sensor_0","h_hits_sensor_0",1000,0.5,1000.5);
    
  h_clus_chip_1 = new TH1D("h_clus_chip_1","h_clus_chip_1",100,0.5,100.5);
  h_clus_sensor_1 = new TH1D("h_clus_sensor_1","h_clus_sensor_1",200,0.5,200.5);    
  h_hits_chip_1 = new TH1D("h_hits_chip_1","h_hits_chip_1",1000,0.5,1000.5);
  h_hits_sensor_1 = new TH1D("h_hits_sensor_1","h_hits_sensor_1",1000,0.5,1000.5);
  
  h_clus_chip_2 = new TH1D("h_clus_chip_2","h_clus_chip_2",100,0.5,100.5);
  h_clus_sensor_2 = new TH1D("h_clus_sensor_2","h_clus_sensor_2",200,0.5,200.5);    
  h_hits_chip_2 = new TH1D("h_hits_chip_2","h_hits_chip_2",1000,0.5,1000.5);
  h_hits_sensor_2 = new TH1D("h_hits_sensor_2","h_hits_sensor_2",1000,0.5,1000.5);
  
  h_clus_chip_3 = new TH1D("h_clus_chip_3","h_clus_chip_3",100,0.5,100.5);
  h_clus_sensor_3 = new TH1D("h_clus_sensor_3","h_clus_sensor_3",200,0.5,200.5);    
  h_hits_chip_3 = new TH1D("h_hits_chip_3","h_hits_chip_3",1000,0.5,1000.5);
  h_hits_sensor_3 = new TH1D("h_hits_sensor_3","h_hits_sensor_3",1000,0.5,1000.5);
   
  h_clus_chip_4 = new TH1D("h_clus_chip_4","h_clus_chip_4",100,0.5,100.5);
  h_clus_sensor_4 = new TH1D("h_clus_sensor_4","h_clus_sensor_4",200,0.5,200.5);    
  h_hits_chip_4 = new TH1D("h_hits_chip_4","h_hits_chip_4",1000,0.5,1000.5);
  h_hits_sensor_4 = new TH1D("h_hits_sensor_4","h_hits_sensor_4",1000,0.5,1000.5);

  // nominal regional occupancy histograms
  
  h_region_occ_0 = new TH1D("h_region_occ_0","h_region_occ_0",5,0.0,5.0);
  histSvc->regHist("/MYSTREAM/h_region_occ_0",h_region_occ_0).ignore();
  
  h_region_occ_1 = new TH1D("h_region_occ_1","h_region_occ_1",5,0.0,5.0);
  histSvc->regHist("/MYSTREAM/h_region_occ_1",h_region_occ_1).ignore();
    
  h_region_occ_2 = new TH1D("h_region_occ_2","h_region_occ_2",5,0.0,5.0);
  histSvc->regHist("/MYSTREAM/h_region_occ_2",h_region_occ_2).ignore();
    
  h_region_occ_3 = new TH1D("h_region_occ_3","h_region_occ_3",5,0.0,5.0);
  histSvc->regHist("/MYSTREAM/h_region_occ_3",h_region_occ_3).ignore();
      
  h_region_occ_4 = new TH1D("h_region_occ_4","h_region_occ_4",5,0.0,5.0);
  histSvc->regHist("/MYSTREAM/h_region_occ_4",h_region_occ_4).ignore();

  // histograms for the flat section of the pixel barrel 

  h_region_occ_0_f = new TH1D("h_region_occ_0_f","h_region_occ_0_f",5,0.0,5.0);
  histSvc->regHist("/MYSTREAM/h_region_occ_0_f",h_region_occ_0_f).ignore();
  
  h_region_occ_1_f = new TH1D("h_region_occ_1_f","h_region_occ_1_f",5,0.0,5.0);
  histSvc->regHist("/MYSTREAM/h_region_occ_1_f",h_region_occ_1_f).ignore();
    
  h_region_occ_2_f = new TH1D("h_region_occ_2_f","h_region_occ_2_f",5,0.0,5.0);
  histSvc->regHist("/MYSTREAM/h_region_occ_2_f",h_region_occ_2_f).ignore();
    
  h_region_occ_3_f = new TH1D("h_region_occ_3_f","h_region_occ_3_f",5,0.0,5.0);
  histSvc->regHist("/MYSTREAM/h_region_occ_3_f",h_region_occ_3_f).ignore();
      
  h_region_occ_4_f = new TH1D("h_region_occ_4_f","h_region_occ_4_f",5,0.0,5.0);
  histSvc->regHist("/MYSTREAM/h_region_occ_4_f",h_region_occ_4_f).ignore();

  // histograms for the inclined section of the pixel barrel 
  
  h_region_occ_0_I = new TH1D("h_region_occ_0_I","h_region_occ_0_I",5,0.0,5.0);
  histSvc->regHist("/MYSTREAM/h_region_occ_0_I",h_region_occ_0_I).ignore();
  
  h_region_occ_1_I = new TH1D("h_region_occ_1_I","h_region_occ_1_I",5,0.0,5.0);
  histSvc->regHist("/MYSTREAM/h_region_occ_1_I",h_region_occ_1_I).ignore();
    
  h_region_occ_2_I = new TH1D("h_region_occ_2_I","h_region_occ_2_I",5,0.0,5.0);
  histSvc->regHist("/MYSTREAM/h_region_occ_2_I",h_region_occ_2_I).ignore();
    
  h_region_occ_3_I = new TH1D("h_region_occ_3_I","h_region_occ_3_I",5,0.0,5.0);
  histSvc->regHist("/MYSTREAM/h_region_occ_3_I",h_region_occ_3_I).ignore();
      
  h_region_occ_4_I = new TH1D("h_region_occ_4_I","h_region_occ_4_I",5,0.0,5.0);
  histSvc->regHist("/MYSTREAM/h_region_occ_4_I",h_region_occ_4_I).ignore();
  
  h_bitsPerChip_0 = new TH1D("h_bitsPerChip_0","h_bitsPerChip_0",10000,0.0,10000.0);
  histSvc->regHist("/MYSTREAM/h_bitsPerChip_0",h_bitsPerChip_0).ignore();
  
  h_bitsPerChip_1 = new TH1D("h_bitsPerChip_1","h_bitsPerChip_1",10000,0.0,10000.0);
  histSvc->regHist("/MYSTREAM/h_bitsPerChip_1",h_bitsPerChip_1).ignore();
  
  h_bitsPerChip_2 = new TH1D("h_bitsPerChip_2","h_bitsPerChip_2",10000,0.0,10000.0);
  histSvc->regHist("/MYSTREAM/h_bitsPerChip_2",h_bitsPerChip_2).ignore();
  
  h_bitsPerChip_3 = new TH1D("h_bitsPerChip_3","h_bitsPerChip_3",10000,0.0,10000.0);
  histSvc->regHist("/MYSTREAM/h_bitsPerChip_3",h_bitsPerChip_3).ignore();
  
  h_bitsPerChip_4 = new TH1D("h_bitsPerChip_4","h_bitsPerChip_4",10000,0.0,10000.0);
  histSvc->regHist("/MYSTREAM/h_bitsPerChip_4",h_bitsPerChip_4).ignore();

  //TH1D *h_eventCounter; //!
  
  h_eventCounter = new TH1D("h_eventCounter","h_eventCounter",5,0.0,5.0);
  histSvc->regHist("/MYSTREAM/h_eventCounter",h_eventCounter).ignore();
  
  

  
  
  
  histSvc->regHist("/MYSTREAM/h_clus_chip_0",h_clus_chip_0).ignore();
  histSvc->regHist("/MYSTREAM/h_clus_sensor_0",h_clus_sensor_0).ignore();
  histSvc->regHist("/MYSTREAM/h_hits_chip_0",h_hits_chip_0).ignore();
  histSvc->regHist("/MYSTREAM/h_hits_sensor_0",h_hits_sensor_0).ignore();
  
  histSvc->regHist("/MYSTREAM/h_clus_chip_1",h_clus_chip_1).ignore();
  histSvc->regHist("/MYSTREAM/h_clus_sensor_1",h_clus_sensor_1).ignore();
  histSvc->regHist("/MYSTREAM/h_hits_chip_1",h_hits_chip_1).ignore();
  histSvc->regHist("/MYSTREAM/h_hits_sensor_1",h_hits_sensor_1).ignore();
  
  histSvc->regHist("/MYSTREAM/h_clus_chip_2",h_clus_chip_2).ignore();
  histSvc->regHist("/MYSTREAM/h_clus_sensor_2",h_clus_sensor_2).ignore();
  histSvc->regHist("/MYSTREAM/h_hits_chip_2",h_hits_chip_2).ignore();
  histSvc->regHist("/MYSTREAM/h_hits_sensor_2",h_hits_sensor_2).ignore();
  
  histSvc->regHist("/MYSTREAM/h_clus_chip_3",h_clus_chip_3).ignore();
  histSvc->regHist("/MYSTREAM/h_clus_sensor_3",h_clus_sensor_3).ignore();
  histSvc->regHist("/MYSTREAM/h_hits_chip_3",h_hits_chip_3).ignore();
  histSvc->regHist("/MYSTREAM/h_hits_sensor_3",h_hits_sensor_3).ignore();

  histSvc->regHist("/MYSTREAM/h_clus_chip_4",h_clus_chip_4).ignore();
  histSvc->regHist("/MYSTREAM/h_clus_sensor_4",h_clus_sensor_4).ignore();
  histSvc->regHist("/MYSTREAM/h_hits_chip_4",h_hits_chip_4).ignore();
  histSvc->regHist("/MYSTREAM/h_hits_sensor_4",h_hits_sensor_4).ignore();
  

  histSvc->regHist("/MYSTREAM/localY_0",localY_0).ignore();
  histSvc->regHist("/MYSTREAM/localY_0_inclined",localY_0_inclined).ignore();
  histSvc->regHist("/MYSTREAM/localY_gr0",localY_gr0).ignore();
  histSvc->regHist("/MYSTREAM/localY_gr0_inclined",localY_gr0_inclined).ignore();

  histSvc->regHist("/MYSTREAM/localX_0",localX_0).ignore();
  histSvc->regHist("/MYSTREAM/localX_0_inclined",localX_0_inclined).ignore();
  histSvc->regHist("/MYSTREAM/localX_gr0",localX_gr0).ignore();
  histSvc->regHist("/MYSTREAM/localX_gr0_inclined",localX_gr0_inclined).ignore();

  
   
  return StatusCode::SUCCESS;
}

StatusCode PerEventPixelsRegOcc::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");

  //----------------------------
  // Event information
  //--------------------------- 
  const xAOD::EventInfo* eventInfo = 0; //NOTE: Everything that comes from the storegate direct from the input files is const!

  CHECK( evtStore()->retrieve( eventInfo) );

  // check if data or MC

  /*if(!eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION ) ){
    isMC = false;
  }
  */
   // Pixel Occupancy Studies
  // number of events used in analysis is required for averaging
  
  nEvents += 1.0;

  std::cout << " event number! " << nEvents << std::endl;
  //std::vector<Counters> pixelChips;
  //std::cout << " testing C++ floor function!, also new event! " << floor(2.5) << std::endl;


  float eventCounter = 1.0;
  h_eventCounter->Fill(eventCounter); 
   
    const xAOD::TrackMeasurementValidationContainer* PIXClusters = 0;
    CHECK( evtStore()->retrieve(PIXClusters, "PixelClusters") );
    std::vector<Counters> pixelModules;
    std::vector<Counters> pixelChips;

   for ( const auto* pix : *PIXClusters) {

     // ensure we are in the barrel
     if ( pix->auxdata<int>("bec") == 0 ) {
       
       // some notes on the eta_pixels / phi_pixels variable
       // max eta_pixel returns is 675, max phi_pixel returns is 803
       // chips are ( 336 x 400 ) --> above is consistent with 2 extra pixels? gap between chips?

       //int eta_pix = pix->auxdata<int>("eta_pixel_index"); 
       //int phi_pix = pix->auxdata<int>("phi_pixel_index"); 

       std::vector<int> eta_pixels = pix->auxdata<std::vector<int>>("rdo_eta_pixel_index");
       std::vector<int> phi_pixels = pix->auxdata<std::vector<int>>("rdo_phi_pixel_index");

       if ( eta_pixels.size() != phi_pixels.size() ) { std::cout << "different lengths of pixels!" << std::endl; } 

       // the size of the two vectors above is always the same --> strategy of assigning pixels to boxes should work. 
     
	float clus_size = pix->rdoIdentifierList().size();
	int layer = pix->auxdata<int>("layer");
        int etaModule = pix->auxdata<int>("eta_module");
	int phiModule = pix->auxdata<int>("phi_module");

	float localX = pix->auxdata<float>("localX");
	float localY = pix->auxdata<float>("localY");	
	int inclinedSection = 0;
	
	if ( layer == 0 && abs(etaModule) > 4 ) { inclinedSection = 1; }
	if ( layer == 1 && abs(etaModule) > 5 ) { inclinedSection = 1; }
	if ( layer == 2 && abs(etaModule) > 6 ) { inclinedSection = 1; }
	if ( layer == 3 && abs(etaModule) > 7 ) { inclinedSection = 1; }
	if ( layer == 4 && abs(etaModule) > 8 ) { inclinedSection = 1; }

	// ----------------------------------------------------------------
	// - This function needs changed and code needs re compiled whether we are running on the inclined or extended layout
	// ----------------------------------------------------------------


	// check the chipIndex here
	int chipIndex = getChipInclined(layer, inclinedSection, localX, localY);
	// these need alternated depending on whether we are using the inclined or extended layout.
	//int chipIndex = getChipExtended(layer, localX, localY);
	//int chipIndex = getChipV2(eta_pixels[0], phi_pixels[0]); 

	bool seenChip = false;
	bool seenModule = false;

	// loop over the pixel chips, we assign indices to each chip, layer, etaID, phiID, section and chipIndex.

	for (unsigned int j=0; j < pixelChips.size(); j++ ) {
	    // if all variables match up, we have seen this chip before!
	    if ( layer == pixelChips[j].getLayer() ) {
	      if ( etaModule == pixelChips[j].getEtaID() ) {
		if ( phiModule == pixelChips[j].getPhiID() ) {
		  if ( inclinedSection == pixelChips[j].getInclined() ) {		    
		    if ( chipIndex == pixelChips[j].getChip() ) {
		      
		      seenChip = true;
		      
		      // add the cluster to that chip
		      pixelChips[j].addCluster(clus_size);
		      
		      for (unsigned int l=0; l < eta_pixels.size(); l++ ) {
			//pixelChips[j].assignToBoxFourByOneCreation(eta_pixels[l], phi_pixels[l]);
			pixelChips[j].assignToBoxFourByOne(eta_pixels[l], phi_pixels[l]);
		      }  

		      
		    }
		  }
		}
	      }

	    }
	  }// ends for loop over pixel chips..

	  // if we haven't seen the chip before then create a new counter.
	  if (!seenChip) {

	    // have to pass integer into counstructor to produce boxes

	    int wantBox = 1;
	    Counters chip = Counters(wantBox);
	    // set the indices of chip
	    chip.SetPixelIndices(layer, etaModule, phiModule, inclinedSection, chipIndex);

	    // read out for each chip is...
	    // nRows x nCols
	    float nRows = 336.0;
	    float nCols = 400.0;
	    float chipArea = 20.0*16.8;

	    chip.setReadOut(nRows*nCols);
	    chip.setArea(chipArea);
	    // add cluster
	    chip.addCluster(clus_size);
	    chip.setEvents(1.0);

	    
	    for (unsigned int l=0; l < eta_pixels.size(); l++ ) {
	      //chip.assignToBoxFourByOneCreation(eta_pixels[l], phi_pixels[l]);
	      chip.assignToBoxFourByOne(eta_pixels[l], phi_pixels[l]);
	    }
	    
	    // add on to pixel chips 
	    pixelChips.push_back(chip);
        
	  }


	  // instead of chips we now do the same for pixel sensors --> same game but remove chip index. 
	
	for (unsigned int j=0; j < pixelModules.size(); j++ ) {
	  // if all variables match up, we have seen this chip before!
	  if ( layer == pixelModules[j].getLayer() ) {
	    if ( etaModule == pixelModules[j].getEtaID() ) {
	      if ( phiModule == pixelModules[j].getPhiID() ) {
		if ( inclinedSection == pixelModules[j].getInclined() ) {

		  seenModule = true;
		  // add the cluster to that module
		  pixelModules[j].addCluster(clus_size); 
		 
		}
	      }
	    }
	  }
	}

	  // if we haven't seen the chip before then create a new counter.
	  
	  if (!seenModule) {
	    //std::cout << " new module!" << std::endl;

	    Counters Module = Counters();
	    // layer, etaModule, phiModule, inclined section, ModuleIndex.
	    Module.SetPixelIndices(layer, etaModule, phiModule, inclinedSection, chipIndex);

	    // read out for each chip is...
	    // nRows x nCols

	    // this rows.. cols stuff all needs sorted.. if we want to take results like hit density / occupancy etc etc 
	    // below is for a single chip, not a whole sensor
	    float nRows = 336.0;
	    float nCols = 400.0;
	    float ModuleArea = 20.0*16.8;

	    Module.setReadOut(nRows*nCols);
	    Module.setArea(ModuleArea);
	    // add cluster
	    Module.addCluster(clus_size);
	    Module.setEvents(1.0);
	   
	    // add on 
	    pixelModules.push_back(Module);
        
	  }
	  
     } // ends BEC
   } // ends for loop over clusters

   std::cout << " check on the number of chips hit and the number of modules hit! " << std::endl;
   std::cout << "number of modules hit!  " << pixelModules.size() << "number of chips hit  " << pixelChips.size() << std::endl; 

   // end clusters for loop
   // start filling histograms

   std::vector<float> maxClusters_chip {0.0,0.0,0.0,0.0,0.0};
   std::vector<float> maxHits_chip {0.0,0.0,0.0,0.0,0.0};
   
   std::vector<float> maxClusters_sensor {0.0,0.0,0.0,0.0,0.0};
   std::vector<float> maxHits_sensor {0.0,0.0,0.0,0.0,0.0};

   // Plot for the individual pixel chips
   // fill the histograms with the number of hits / clusters associated to each chip.
   
   for (unsigned int i=0; i < pixelChips.size(); i++ ) {

      int nLayer = pixelChips[i].getLayer();
      int etaID = pixelChips[i].getEtaID(); 

      // ----------
      // checking on the regional occupancy of each chip
      // ----------

      // values used to compute average regional occupancy of chip
      
	float regOccSum = 0.0;
	float regOccCounter = 0.0;

	// get all the boxes associated to the pixel chip
	std::vector<tuple<int,int, int>> boxes = pixelChips[i].getBoxes();
	
	// do for loop over boxes within chip
	for (unsigned int m=0; m < boxes.size(); m++ ) {

	  // boxes only of interest if the box is non-zero
	  float regOccFloat = (float)std::get<2>(boxes[m]);
	  if ( regOccFloat != 0.0 ) {

	    // find the average region occupancy of each chip
	    regOccSum += regOccFloat;
	    regOccCounter += 1.0;
	    
	    // extend this part to the flat and inclined section of the pixel barrel 
	    switch(nLayer) {
	    case 0:
	      
	      h_region_occ_0->Fill(regOccFloat);	      
	      // fill inclined and flat sections. 
	      if ( abs(etaID) > 4 ) { h_region_occ_0_I->Fill(regOccFloat); }
	      else { h_region_occ_0_f->Fill(regOccFloat); }
	      
	    case 1:
	      h_region_occ_1->Fill(regOccFloat);
	      // fill inclined and flat sections. 
	      if ( abs(etaID) > 5 ) { h_region_occ_1_I->Fill(regOccFloat); }
	      else { h_region_occ_1_f->Fill(regOccFloat); }
	      
	    case 2:
	      h_region_occ_2->Fill(regOccFloat);
	      // fill inclined and flat sections. 
	      if ( abs(etaID) > 6 ) { h_region_occ_2_I->Fill(regOccFloat); }
	      else { h_region_occ_2_f->Fill(regOccFloat); }
	      
	    case 3:
	      h_region_occ_3->Fill(regOccFloat);
	      // fill inclined and flat sections. 
	      if ( abs(etaID) > 7 ) { h_region_occ_3_I->Fill(regOccFloat); }
	      else { h_region_occ_3_f->Fill(regOccFloat); }
	      
	    case 4:
	      h_region_occ_4->Fill(regOccFloat);
	      // fill inclined and flat sections. 
	      if ( abs(etaID) > 8 ) { h_region_occ_4_I->Fill(regOccFloat); }
	      else { h_region_occ_4_f->Fill(regOccFloat); }
	      
	    }

	  }
	  

	} // ends for loop over pixel boxs

	if ( regOccCounter == 0 || regOccSum == 0 ) {
	  std::cout << "NaN" << std::endl;
	  std::cout << "reg Occ Counter -- " << regOccCounter << "regOccFloat -- " << regOccSum << std::endl;


	}

	float regAvgOccPerChip = regOccSum / regOccCounter;

	std::cout << " regional occ " << regAvgOccPerChip << std::endl; 

	float bitsPerChip = ( 1.05*32*pixelChips[i].getNHits() ) / regAvgOccPerChip;
	
	// sort with different layers
	switch(nLayer) {
	case 0:
	  h_clus_chip_0->Fill(pixelChips[i].getNClusters());
	  h_hits_chip_0->Fill(pixelChips[i].getNHits());
	  h_bitsPerChip_0->Fill(bitsPerChip); 
        case 1:
	  h_clus_chip_1->Fill(pixelChips[i].getNClusters());
	  h_hits_chip_1->Fill(pixelChips[i].getNHits());
	  h_bitsPerChip_1->Fill(bitsPerChip);
        case 2:
	  h_clus_chip_2->Fill(pixelChips[i].getNClusters());
	  h_hits_chip_2->Fill(pixelChips[i].getNHits());
	  h_bitsPerChip_2->Fill(bitsPerChip);
        case 3:
	  h_clus_chip_3->Fill(pixelChips[i].getNClusters());
	  h_hits_chip_3->Fill(pixelChips[i].getNHits());
	  h_bitsPerChip_3->Fill(bitsPerChip);
        case 4:
	  h_clus_chip_4->Fill(pixelChips[i].getNClusters());
	  h_hits_chip_4->Fill(pixelChips[i].getNHits());
	  h_bitsPerChip_4->Fill(bitsPerChip);

	}


   } // ends for loop over the pixel chips 


      
   // find the maximums
   // Plot for the individual pixel modules
   // fill the histograms with the number of hits / clusters associated to each chip.

    for (unsigned int i=0; i < pixelModules.size(); i++ ) {

      int nLayer = pixelModules[i].getLayer();
              
      if ( pixelModules[i].getNHits() > maxHits_sensor[nLayer]) {	maxHits_sensor[nLayer] = pixelModules[i].getNHits();  }
      if ( pixelModules[i].getNClusters() > maxClusters_sensor[nLayer]) { maxClusters_sensor[nLayer] = pixelModules[i].getNClusters(); }
   	
      
	// sort into the different layers
	switch(nLayer) {
	case 0:
	  h_clus_sensor_0->Fill(pixelModules[i].getNClusters());
	  h_hits_sensor_0->Fill(pixelModules[i].getNHits());
        case 1:
	  h_clus_sensor_1->Fill(pixelModules[i].getNClusters());
	  h_hits_sensor_1->Fill(pixelModules[i].getNHits());
        case 2:
	  h_clus_sensor_2->Fill(pixelModules[i].getNClusters());
	  h_hits_sensor_2->Fill(pixelModules[i].getNHits());
        case 3:
	  h_clus_sensor_3->Fill(pixelModules[i].getNClusters());
	  h_hits_sensor_3->Fill(pixelModules[i].getNHits());
        case 4:
	  h_clus_sensor_4->Fill(pixelModules[i].getNClusters());
	  h_hits_sensor_4->Fill(pixelModules[i].getNHits());

	}
      }

      

    // set to the values that are stored in the TTree..
    
    maxHits_chip_0 = maxHits_chip[0];
    maxClusters_chip_0 = maxClusters_chip[0];

    maxHits_chip_1 = maxHits_chip[1];
    maxClusters_chip_1 = maxClusters_chip[1];
    
    maxHits_chip_2 = maxHits_chip[2];
    maxClusters_chip_2 = maxClusters_chip[2];
      
    maxHits_chip_3 = maxHits_chip[3];
    maxClusters_chip_3 = maxClusters_chip[3];
      
    maxHits_chip_4 = maxHits_chip[4];
    maxClusters_chip_4 = maxClusters_chip[4];



    
        
    maxHits_sensor_0 = maxHits_sensor[0];
    maxClusters_sensor_0 = maxClusters_sensor[0];

    maxHits_sensor_1 = maxHits_sensor[1];
    maxClusters_sensor_1 = maxClusters_sensor[1];
    
    maxHits_sensor_2 = maxHits_sensor[2];
    maxClusters_sensor_2 = maxClusters_sensor[2];
      
    maxHits_sensor_3 = maxHits_sensor[3];
    maxClusters_sensor_3 = maxClusters_sensor[3];
      
    maxHits_sensor_4 = maxHits_sensor[4];
    maxClusters_sensor_4 = maxClusters_sensor[4];
    

    // fill flat ntuple 
    myTree->Fill();
   


   
  return StatusCode::SUCCESS;

}
    




StatusCode PerEventPixelsRegOcc::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");

  return StatusCode::SUCCESS;
}



 int PerEventPixelsRegOcc::getChipInclined(int layer, int inclinedSection, float localX, float localY) {
  



   int chipIndex = 0;
   
   // what layout is being used?
   // in the inclined layout -- switch from double chips to single chips (layer 0), switch from quad chips to double chips.
   
   // in layer 0 we either have one chip (inclined) or two chips (not inclined) 
	
   if (layer == 0) {
     // non-inclined section
     if (inclinedSection == 0) {
       // two chips, check the local Y direction
       if ( localY >= 0.0 ) { chipIndex = 0; }
       if ( localY < 0.0 ) { chipIndex = 1; }
     }

     // for the inclined section 
     if (inclinedSection == 1) {
       chipIndex = 0;
     }
   }

  // in layers > 0 we either have two chips (inclined) or four chips (not inclined) 
   if ( layer > 0 ) {
     // non-inclined section
     if (inclinedSection == 0 ) {
       // four chips, check both the X and Y direction.
       // four opts. neg X / neg Y. neg X / pos Y. pos X / pos Y. pos X / neg Y. 

       if ( localX < 0.0 && localY < 0.0 ) { chipIndex = 0; }
       if ( localX < 0.0 && localY >= 0.0 ) { chipIndex = 1; }
       if ( localX > 0.0 && localY >= 0.0 ) { chipIndex = 2; }
       if ( localX > 0.0 && localY < 0.0 ) { chipIndex = 3; }

     }

     // inclined section..
     if (inclinedSection == 1 ) {
       // two chips. check the local X direction.
       if ( localX >= 0.0 ) { chipIndex = 0; }
       if ( localX < 0.0 ) { chipIndex = 1; }
     }

   }
   return chipIndex; 

 }

 

 int PerEventPixelsRegOcc::getChipExtended(int layer, float localX, float localY) {
  
   int chipIndex = 0;
	
   if (layer == 0) {
       // two chips, check the local Y direction
       if ( localY >= 0.0 ) { chipIndex = 0; }
       if ( localY < 0.0 ) { chipIndex = 1; }
     }

  // in layers > 0 we have four chips (extended) layout 
   if ( layer > 0 ) {

       if ( localX < 0.0 && localY < 0.0 ) { chipIndex = 0; }
       if ( localX < 0.0 && localY >= 0.0 ) { chipIndex = 1; }
       if ( localX > 0.0 && localY >= 0.0 ) { chipIndex = 2; }
       if ( localX > 0.0 && localY < 0.0 ) { chipIndex = 3; }

     }

   return chipIndex; 
   }








 int PerEventPixelsRegOcc::getChipV2(int eta_pixel, int phi_pixel) {

   int chipIndex = 0;

   // both in the first chip
   if ( eta_pixel < 402 && phi_pixel < 338 ) { chipIndex = 0; }
   
   // greater than in eta direction
   if ( eta_pixel > 402 && phi_pixel < 338 ) { chipIndex = 1; }
   
   // greater than in phi direction
   if ( eta_pixel < 402 && phi_pixel > 338 ) { chipIndex = 2; }

   // greater than in eta & phi direction
   if ( eta_pixel > 402 && phi_pixel > 338 ) { chipIndex = 3; }

   return chipIndex; 





 }


 


 
