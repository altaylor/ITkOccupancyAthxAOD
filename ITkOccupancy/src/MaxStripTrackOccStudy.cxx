// MyPackage includes
#include "MaxStripTrackOccStudy.h"

// MyPackage includes
#include "xAODJet/JetContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetAuxContainer.h"

#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "xAODTracking/TrackMeasurementValidationAuxContainer.h"

#include "xAODTracking/TrackStateValidationContainer.h"
#include "xAODTracking/TrackStateValidationAuxContainer.h"

//#include "xAODJet/JetAuxContainer.h"

#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackParticleAuxContainer.h"

#include "TVector.h"

#include "GaudiKernel/ITHistSvc.h"
#include "Counters.h"



#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticle.h"



//#include "AthLinks/ElementLink.h"





MaxStripTrackOccStudy::MaxStripTrackOccStudy( const std::string& name, ISvcLocator* pSvcLocator ) : AthAlgorithm( name, pSvcLocator ){

  //declareProperty( "Property", m_nProperty ); //example property declaration

}


MaxStripTrackOccStudy::~MaxStripTrackOccStudy() {}


StatusCode MaxStripTrackOccStudy::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");


  ServiceHandle<ITHistSvc> histSvc("THistSvc",name()); 
   CHECK( histSvc.retrieve() );

   myTree = new TTree("myTree","myTree");
  CHECK( histSvc->regTree("/MYSTREAM/myTree",myTree) );

  myTree->Branch("maxHits_0", &maxHits_0);
  myTree->Branch("maxClusters_0", &maxClusters_0);
  myTree->Branch("maxOccupancy_0", &maxOccupancy_0);
  myTree->Branch("maxHitDensity_0", &maxHitDensity_0);

  myTree->Branch("maxHits_1", &maxHits_1);
  myTree->Branch("maxClusters_1", &maxClusters_1);
  myTree->Branch("maxOccupancy_1", &maxOccupancy_1);
  myTree->Branch("maxHitDensity_1", &maxHitDensity_1); 
  
  myTree->Branch("maxHits_2", &maxHits_2);
  myTree->Branch("maxClusters_2", &maxClusters_2);
  myTree->Branch("maxOccupancy_2", &maxOccupancy_2);
  myTree->Branch("maxHitDensity_2", &maxHitDensity_2); 

  myTree->Branch("maxHits_3", &maxHits_3);
  myTree->Branch("maxClusters_3", &maxClusters_3);
  myTree->Branch("maxOccupancy_3", &maxOccupancy_3);
  myTree->Branch("maxHitDensity_3", &maxHitDensity_3);
  
  // also find the max hits in the sensor with the max number of clusters
  
  myTree->Branch("nHitsinMaxClusters_0", &nHitsinMaxClusters_0);
  myTree->Branch("nHitsinMaxClusters_1", &nHitsinMaxClusters_1);
  myTree->Branch("nHitsinMaxClusters_2", &nHitsinMaxClusters_2);
  myTree->Branch("nHitsinMaxClusters_3", &nHitsinMaxClusters_3);
   
  myTree->Branch("nClustersinMaxHits_0", &nClustersinMaxHits_0);
  myTree->Branch("nClustersinMaxHits_1", &nClustersinMaxHits_1);
  myTree->Branch("nClustersinMaxHits_2", &nClustersinMaxHits_2);
  myTree->Branch("nClustersinMaxHits_3", &nClustersinMaxHits_3);

  
  myTree->Branch("maxEndCapHits_0", &maxEndCapHits_0);
  myTree->Branch("maxEndCapClusters_0", &maxEndCapClusters_0);
  myTree->Branch("maxEndCapOccupancy_0", &maxEndCapOccupancy_0);
  myTree->Branch("maxEndCapHitDensity_0", &maxEndCapHitDensity_0);

  myTree->Branch("maxEndCapHits_1", &maxEndCapHits_1);
  myTree->Branch("maxEndCapClusters_1", &maxEndCapClusters_1);
  myTree->Branch("maxEndCapOccupancy_1", &maxEndCapOccupancy_1);
  myTree->Branch("maxEndCapHitDensity_1", &maxEndCapHitDensity_1); 
  
  myTree->Branch("maxEndCapHits_2", &maxEndCapHits_2);
  myTree->Branch("maxEndCapClusters_2", &maxEndCapClusters_2);
  myTree->Branch("maxEndCapOccupancy_2", &maxEndCapOccupancy_2);
  myTree->Branch("maxEndCapHitDensity_2", &maxEndCapHitDensity_2); 

  myTree->Branch("maxEndCapHits_3", &maxEndCapHits_3);
  myTree->Branch("maxEndCapClusters_3", &maxEndCapClusters_3);
  myTree->Branch("maxEndCapOccupancy_3", &maxEndCapOccupancy_3);
  myTree->Branch("maxEndCapHitDensity_3", &maxEndCapHitDensity_3);
  
  myTree->Branch("maxEndCapHits_4", &maxEndCapHits_4);
  myTree->Branch("maxEndCapClusters_4", &maxEndCapClusters_4);
  myTree->Branch("maxEndCapOccupancy_4", &maxEndCapOccupancy_4);
  myTree->Branch("maxEndCapHitDensity_4", &maxEndCapHitDensity_4); 

  myTree->Branch("maxEndCapHits_5", &maxEndCapHits_5);
  myTree->Branch("maxEndCapClusters_5", &maxEndCapClusters_5);
  myTree->Branch("maxEndCapOccupancy_5", &maxEndCapOccupancy_5);
  myTree->Branch("maxEndCapHitDensity_5", &maxEndCapHitDensity_5);

  h_hits_sensor_0 = new TH1D("h_hits_sensor_0","h_hits_sensor_0",600,0.5,600.5);
  h_hits_sensor_1 = new TH1D("h_hits_sensor_1","h_hits_sensor_1",600,0.5,600.5);
  h_hits_sensor_2 = new TH1D("h_hits_sensor_2","h_hits_sensor_2",600,0.5,600.5);
  h_hits_sensor_3 = new TH1D("h_hits_sensor_3","h_hits_sensor_3",600,0.5,600.5);
  
  h_clus_sensor_0 = new TH1D("h_clus_sensor_0","h_clus_sensor_0",600,0.5,600.5);
  h_clus_sensor_1 = new TH1D("h_clus_sensor_1","h_clus_sensor_1",600,0.5,600.5);
  h_clus_sensor_2 = new TH1D("h_clus_sensor_2","h_clus_sensor_2",600,0.5,600.5);
  h_clus_sensor_3 = new TH1D("h_clus_sensor_3","h_clus_sensor_3",600,0.5,600.5);
  
  h_hits_row_0 = new TH1D("h_hits_row_0","h_hits_row_0",600,0.5,600.5);
  h_hits_row_1 = new TH1D("h_hits_row_1","h_hits_row_1",600,0.5,600.5);
  h_hits_row_2 = new TH1D("h_hits_row_2","h_hits_row_2",600,0.5,600.5);
  h_hits_row_3 = new TH1D("h_hits_row_3","h_hits_row_3",600,0.5,600.5);
  
  h_clus_row_0 = new TH1D("h_clus_row_0","h_clus_row_0",600,0.5,600.5);
  h_clus_row_1 = new TH1D("h_clus_row_1","h_clus_row_1",600,0.5,600.5);
  h_clus_row_2 = new TH1D("h_clus_row_2","h_clus_row_2",600,0.5,600.5);
  h_clus_row_3 = new TH1D("h_clus_row_3","h_clus_row_3",600,0.5,600.5);

  histSvc->regHist("/MYSTREAM/h_clus_sensor_0",h_clus_sensor_0).ignore();
  histSvc->regHist("/MYSTREAM/h_clus_sensor_1",h_clus_sensor_1).ignore();
  histSvc->regHist("/MYSTREAM/h_clus_sensor_2",h_clus_sensor_2).ignore();
  histSvc->regHist("/MYSTREAM/h_clus_sensor_3",h_clus_sensor_3).ignore();

  histSvc->regHist("/MYSTREAM/h_clus_row_0",h_clus_row_0).ignore();
  histSvc->regHist("/MYSTREAM/h_clus_row_1",h_clus_row_1).ignore();
  histSvc->regHist("/MYSTREAM/h_clus_row_2",h_clus_row_2).ignore();
  histSvc->regHist("/MYSTREAM/h_clus_row_3",h_clus_row_3).ignore();

  histSvc->regHist("/MYSTREAM/h_hits_sensor_0",h_hits_sensor_0).ignore();
  histSvc->regHist("/MYSTREAM/h_hits_sensor_1",h_hits_sensor_1).ignore();
  histSvc->regHist("/MYSTREAM/h_hits_sensor_2",h_hits_sensor_2).ignore();
  histSvc->regHist("/MYSTREAM/h_hits_sensor_3",h_hits_sensor_3).ignore();

  histSvc->regHist("/MYSTREAM/h_hits_row_0",h_hits_row_0).ignore();
  histSvc->regHist("/MYSTREAM/h_hits_row_1",h_hits_row_1).ignore();
  histSvc->regHist("/MYSTREAM/h_hits_row_2",h_hits_row_2).ignore();
  histSvc->regHist("/MYSTREAM/h_hits_row_3",h_hits_row_3).ignore();


  n_events = 0.0; 


  


  


  


  return StatusCode::SUCCESS;
}

StatusCode MaxStripTrackOccStudy::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");

  return StatusCode::SUCCESS;
}

StatusCode MaxStripTrackOccStudy::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  //----------------------------
  // Event information
  //--------------------------- 
  const xAOD::EventInfo* eventInfo = 0; //NOTE: Everything that comes from the storegate direct from the input files is const!

  // ask the event store to retrieve the xAOD EventInfo container
  //CHECK( evtStore()->retrieve( eventInfo, "EventInfo") );  // the second argument ("EventInfo") is the key name
  CHECK( evtStore()->retrieve( eventInfo) );
  // if there is only one container of that type in the xAOD (as with the EventInfo container), you do not need to pass
  // the key name, the default will be taken as the only key name in the xAOD

  n_events += 1.0;
  std::cout << " n events? " << n_events << std::endl; 

  // check if data or MC
  bool isMC = true;
  if(!eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION ) ){
    isMC = false;
  }
   // SCT Occupancy Studies

  // short strips in layers 0 + 1, long strips in layers 2 + 3
  
  float SensorArea = 9316.096;
  float nShortStrips = 5120; 
  float nLongStrips = 2560;

  // set the below
  float area = 0;
  float nStrips = 0;

  // vector of counters for recording the maxmimums
  std::vector<Counters> stripBarrelModules;
  
  const xAOD::TrackMeasurementValidationContainer* SCTClusters = 0;
  CHECK( evtStore()->retrieve(SCTClusters, "SCT_Clusters") );

  

  int barrelModuleIndex = 0; 

   for ( const auto* sct : *SCTClusters) {

     // boolean required to decide if we need new counter
     bool seenSensor = false;

      if ( sct->auxdata<int>("bec") == 0 ) {

	int layer = sct->auxdata<int>("layer");
	int etaModule = sct->auxdata<int>("eta_module");
	int phiModule = sct->auxdata<int>("phi_module");
	int side = sct->auxdata<int>("side");
	float clus_size = sct->rdoIdentifierList().size();

	  // check if we have seen this sensor before, if yes, don't create new counter, if no create new counter
	  for (int j=0; j < stripBarrelModules.size(); j++ ) {

	    // if all variables match up, we have seen this module before!
	    if ( layer == stripBarrelModules[j].getLayer() ) {
	      if ( etaModule == stripBarrelModules[j].getEtaID() ) {
		if ( phiModule == stripBarrelModules[j].getPhiID() ) {
		  if ( side == stripBarrelModules[j].getSide() ) {
		    
		    seenSensor = true;
		    // add the cluster to that module
		    stripBarrelModules[j].addCluster(clus_size);
		    
		  }
		}
	      }
	    }

	  } // ends for loop over modules

	  
	  // if we have not seen the sensor before, create sensor
	  if ( seenSensor == false ) {
	    
	    Counters c = Counters();
	    c.SetIndices(layer,etaModule,phiModule,side);
	    float nStrips;
	    if ( layer > 1 ) { nStrips = nLongStrips; }
	    else { nStrips = nShortStrips; }
	    
	    c.setReadOut(nStrips);
	    c.setArea(SensorArea);
	    // add cluster...
	    c.addCluster(clus_size);
	    // set the number of events used to 1...
	    c.setEvents(1.0);
	    // add on 
	    stripBarrelModules.push_back(c);
	  }
	  

      }
   } // ends for loop over the SCT Clusters for the barrel


   // start track selection for the SCT Barrel
   // try to do track to cluster matching..

   // create vector of tuples for the sensors that have a track associated to them
   std::vector<std::tuple<int,int,int,int>> sensorsWithTracks;
   
   const xAOD::TrackParticleContainer* recoTracks = 0;
   CHECK( evtStore()->retrieve(recoTracks, "InDetTrackParticles") );

   // how many tracks are truth matched
   int nTruthMatchedTracks = 0;
   int nTracks = 0;
   int nFakeTracks = 0;

   int nPrimTrks = 0;
   int nSecTrks = 0;

   // loop over the tracks..
   for ( const auto* track : *recoTracks) {

     // do track --> truth matching
     // note that you can also do truth match probability >= 0.5

     nTracks++;
     std::cout << "d0 of track?" << track->d0() << std::endl;
     std::cout << "pt of track?" << track->pt() << std::endl; 

     const xAOD::TruthParticle* matched_truth_particle=0;
     if(track->isAvailable<ElementLink<xAOD::TruthParticleContainer> >("truthParticleLink")) {
       ElementLink<xAOD::TruthParticleContainer> link = track->auxdata<ElementLink<xAOD::TruthParticleContainer> >("truthParticleLink");
       if(link.isValid()) {
	 
	 matched_truth_particle = *link;

	 int trueBC = matched_truth_particle->barcode();
	   if ((trueBC > 0) && (trueBC < 200000)) {
	     nPrimTrks++;
	   }
	 if ((trueBC > 200000)) {
	   nSecTrks++; 
	 }
       }
     }

     // now do track --> cluster matching 

     typedef std::vector<ElementLink< xAOD::TrackStateValidationContainer > > MeasurementsOnTrack;                                                     
     typedef std::vector<ElementLink< xAOD::TrackStateValidationContainer > >::const_iterator MeasurementsOnTrackIter;

     const MeasurementsOnTrack& measurementsOnTrack = track->auxdata< MeasurementsOnTrack >("msosLink");
     // loop over measurements on track
     for ( auto msos_itr: measurementsOnTrack ) {
       const xAOD::TrackStateValidation *msos = *msos_itr;

       // if it belongs to the strips. det type = 2. 
       if ( msos->detType() == 2 ) {
	 //std::cout << "strip measurement! " << std::endl;
	if(  msos->trackMeasurementValidationLink().isValid() && *(msos->trackMeasurementValidationLink()) ){
	  const xAOD::TrackMeasurementValidation* stripCluster =  *(msos->trackMeasurementValidationLink());

	  // strip cluster that is associated to a track!
	  //std::cout << "msos? " << msos->localTheta() << std::endl;
	  // demand this quantity is in the barrel
	  if ( stripCluster->auxdata<int>("bec") == 0 ) {
	    
	    int layer = stripCluster->auxdata<int>("layer");
	    int etaModule = stripCluster->auxdata<int>("eta_module");
	    int phiModule = stripCluster->auxdata<int>("phi_module");
	    int side = stripCluster->auxdata<int>("side");

	    // check if we have seen this sensor before, if yes, don't create new counter, if no create new counter
	    for (int j=0; j < stripBarrelModules.size(); j++ ) {

	      // if all variables match up, we have seen this module before!
	      if ( layer == stripBarrelModules[j].getLayer() ) {
		if ( etaModule == stripBarrelModules[j].getEtaID() ) {
		  if ( phiModule == stripBarrelModules[j].getPhiID() ) {
		    if ( side == stripBarrelModules[j].getSide() ) {

		      // put function for track associated
		      stripBarrelModules[j].SetTrackAssociation();
		    
		    }
		  }
		}
	      }

	    } // ends for loop over modules
	    
	  }
	}
       }
     }
   }
   
   std::cout << "number of tracks" << nTracks << std::endl; 
   std::cout << "number of primary tracks" << nPrimTrks << std::endl;
   std::cout << "number of secondary tracks?" << nSecTrks << std::endl;

   //*recoTracks)


  std::vector<float> maxClusters {0.0,0.0,0.0,0.0};
  std::vector<float> maxHits {0.0,0.0,0.0,0.0};
  std::vector<float> maxOccupancy {0.0,0.0,0.0,0.0};
  std::vector<float> maxHitDensity {0.0,0.0,0.0,0.0};

  // two other vectors are also defined
  // these save the sensor with the number of clusters in the sensor wi
  std::vector<float> nClustersinMaxHits {0.0,0.0,0.0,0.0};
  std::vector<float> nHitsinMaxClusters {0.0,0.0,0.0,0.0};

   // recording maximums in the SCT Barrel

  int ind_sensorsWithTracks = 0; 
   
    for (int i=0; i < stripBarrelModules.size(); i++ ) {

      int layerIndex = stripBarrelModules[i].getLayer();
      
	// sort with different layers
        // get distributions of all sensors that have been hit
      
	switch(layerIndex) {
	case 0:
	  h_clus_sensor_0->Fill(stripBarrelModules[i].getNClusters());
	  h_hits_sensor_0->Fill(stripBarrelModules[i].getNHits());
        case 1:
	  h_clus_sensor_1->Fill(stripBarrelModules[i].getNClusters());
	  h_hits_sensor_1->Fill(stripBarrelModules[i].getNHits());
        case 2:
	  h_clus_sensor_2->Fill(stripBarrelModules[i].getNClusters());
	  h_hits_sensor_2->Fill(stripBarrelModules[i].getNHits());
        case 3:
	  h_clus_sensor_3->Fill(stripBarrelModules[i].getNClusters());
	  h_hits_sensor_3->Fill(stripBarrelModules[i].getNHits());

	}

	// save maximums 
      if ( stripBarrelModules[i].getNHits() > maxHits[layerIndex]) {
	maxHits[layerIndex] = stripBarrelModules[i].getNHits();
	// find the number of clusters in the sensor with the maximum number of hits 
	nClustersinMaxHits[layerIndex] = stripBarrelModules[i].getNClusters();
      }
      
      if ( stripBarrelModules[i].getNClusters() > maxClusters[layerIndex]) {
	maxClusters[layerIndex] = stripBarrelModules[i].getNClusters();
	// find the number of hits in the sensor with the maximum number of clusters
	nHitsinMaxClusters[layerIndex] = stripBarrelModules[i].getNHits();
      }
      if ( stripBarrelModules[i].occupancy() > maxOccupancy[layerIndex]) { maxOccupancy[layerIndex] = stripBarrelModules[i].occupancy(); }
      if ( stripBarrelModules[i].fluency() > maxHitDensity[layerIndex]) { maxHitDensity[layerIndex] = stripBarrelModules[i].fluency(); }


      // how many sensors have a track associated to them

      if ( stripBarrelModules[i].getTrackAssociation() == 1 ) {
	ind_sensorsWithTracks++;
      }
      
    }


    std::cout << " number of sensors with clusters that are associated to tracks?" << ind_sensorsWithTracks << std::endl;
    std::cout << " number of sensors with hits hits " << stripBarrelModules.size() << std::endl;

    

    // save TTree variables 

    maxHits_0 = maxHits[0];
    maxClusters_0 = maxClusters[0];
    maxOccupancy_0 = maxOccupancy[0]*100;
    maxHitDensity_0 = maxHitDensity[0];

    maxHits_1 = maxHits[1];
    maxClusters_1 = maxClusters[1];
    maxOccupancy_1 = maxOccupancy[1]*100;
    maxHitDensity_1 = maxHitDensity[1];
    
    maxHits_2 = maxHits[2];
    maxClusters_2 = maxClusters[2];
    maxOccupancy_2 = maxOccupancy[2]*100;
    maxHitDensity_2 = maxHitDensity[2];
      
    maxHits_3 = maxHits[3];
    maxClusters_3 = maxClusters[3];
    maxOccupancy_3 = maxOccupancy[3]*100;
    maxHitDensity_3 = maxHitDensity[3];

    // find how many hits are in the sensor with the max number of clusters

    nHitsinMaxClusters_0 = nHitsinMaxClusters[0];
    nHitsinMaxClusters_1 = nHitsinMaxClusters[1];
    nHitsinMaxClusters_2 = nHitsinMaxClusters[2];
    nHitsinMaxClusters_3 = nHitsinMaxClusters[3];

    // find how many clusters are in the sensor with the max number of hits
      
    nClustersinMaxHits_0 = nClustersinMaxHits[0];
    nClustersinMaxHits_1 = nClustersinMaxHits[1];  
    nClustersinMaxHits_2 = nClustersinMaxHits[2];
    nClustersinMaxHits_3 = nClustersinMaxHits[3];
    
    // fill flat ntuple 
   myTree->Fill();


   
  return StatusCode::SUCCESS;
}


