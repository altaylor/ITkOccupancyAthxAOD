import sys

from ROOT import *
gROOT.LoadMacro("AtlasStyle.C")
gROOT.LoadMacro("AtlasLabels.C")
gROOT.LoadMacro("AtlasUtils.C")

SetAtlasStyle()

gStyle.SetOptStat(False)
gStyle.SetMarkerSize(1.5)

# set log scale for y axis...
c1 = TCanvas()

if len(sys.argv) == 1:
    print 'Exiting! please provide .root file as first argument and Inclined or Extended as second arguement!'
    sys.exit()


print(sys.argv)
signal = TFile(sys.argv[1])

#signal = TFile("/afs/cern.ch/work/a/altaylor/itk_occupancy_svn2/ITkOccupancy/ITkOccupancy/ITkOccupancy/run/Plotting/avgPixelStudies_extended_step15_V2.root") 
#signal.ls()
#plotLabel = "Extended"
plotLabel = sys.argv[2]

print(sys.argv)


def main():

    # arguments are the graphs required to produce multigraph, y axis title, y axis max and savename
    
    # plot the channel occupancy for the pix barrel
    # graph names, then y axis maximum
    
    # for the inclined layout, use 1.0 here, for extended use 3.0
    # inclined or extended in as an argument
    #plotLabel = "Extended"

    PlotBarrelOccupancy("occ_z_B0","occ_z_B1","occ_z_B2","occ_z_B3","occ_z_B4",  "Pixel Channel Occupancy in %", 0.3, "pix_occ_vs_z_barrel", plotLabel)
    
    PlotEndCapOccupancy("occ_z_EC0","occ_z_EC1","occ_z_EC2","occ_z_EC3",  "Pixel Channel Occupancy in %", 0.5, "pixel_occ_vs_z_endcap", plotLabel)
    # required to move the legend and the label for pixel hits / mm^{2}
    PlotBarrelOccupancy("hit_dens_z_B0","hit_dens_z_B1","hit_dens_z_B2","hit_dens_z_B3","hit_dens_z_B4",  "Average hit dens [pixels]", 1.0, "pix_hit_dens_vs_z_barrel", plotLabel)
    
    PlotEndCapOccupancy("hit_dens_z_EC0","hit_dens_z_EC1","hit_dens_z_EC2","hit_dens_z_EC3",  "Pixel Hits / mm^{2}", 1.0, "pixel_hits_vs_z_endcap", plotLabel)

    # end cap cluster density
    PlotBarrelOccupancy("clus_dens_z_B0","clus_dens_z_B1","clus_dens_z_B2","clus_dens_z_B3","clus_dens_z_B4",  "Average cluster dens [pixels]", 0.3, "pix_clus_dens_vs_z_barrel", plotLabel)
    
    PlotEndCapOccupancy("clus_dens_z_EC0","clus_dens_z_EC1","clus_dens_z_EC2","clus_dens_z_EC3",  "Pixel Clusters / mm^{2}", 1.0, "pixel_clusters_vs_z_endcap", plotLabel)


    # y max for cluster size should either be set to 32.0 for extended or 12.0 for inclined....
    if plotLabel == "Extended":
        yMax = 32.0 
    else:
        yMax = 12.0
    

    # here we plot the cluster size
    PlotBarrelOccupancy("clus_size_z_B0","clus_size_z_B1","clus_size_z_B2","clus_size_z_B3","clus_size_z_B4",  "Average cluster size [pixels]", yMax, "pix_clus_size_vs_z_barrel", plotLabel)


    PlotEndCapOccupancy("clus_size_z_EC0","clus_size_z_EC1","clus_size_z_EC2","clus_size_z_EC3",  "Average cluster size [pixels]", 4.0, "pix_clus_size_vs_z_endcap", plotLabel)



    
def PlotBarrelOccupancy(graph_0, graph_1, graph_2, graph_3, graph_4, yaxis, yaxismax, saveName, plot_label):

    g_0 = TGraph()
    g_1 = TGraph()
    g_2 = TGraph()
    g_3 = TGraph()
    g_4 = TGraph()
    
    signal.GetObject(graph_0, g_0)
    signal.GetObject(graph_1, g_1)
    signal.GetObject(graph_2, g_2)
    signal.GetObject(graph_3, g_3)
    signal.GetObject(graph_4, g_4)

    g_0.SetMarkerColor(1)
    g_1.SetMarkerColor(2)
    g_2.SetMarkerColor(3)
    g_3.SetMarkerColor(4)
    g_4.SetMarkerColor(6)

    g_0.SetMarkerStyle(20)
    g_1.SetMarkerStyle(21)
    g_2.SetMarkerStyle(22)
    g_3.SetMarkerStyle(23)
    g_4.SetMarkerStyle(34)

    MultiG = TMultiGraph()

    MultiG.Add(g_0)
    MultiG.Add(g_1)
    MultiG.Add(g_2)
    MultiG.Add(g_3)
    MultiG.Add(g_4)

    MultiG.SetMaximum(yaxismax);


    
    MultiG.Draw("AP")

    MultiG.GetXaxis().SetTitle("z [mm]")
    MultiG.GetYaxis().SetTitle(yaxis)
        
    #####MultiG.GetYaxis().SetRangeUser(0,yaxismax)
    ####MultiG.GetXaxis().SetRangeUser(0,1400.0)

    #leg = TLegend(0.32,0.7,0.52,0.90)

    '''
    if saveName == "pix_clus_size_vs_z_barrel":
        leg = TLegend(0.32-0.10,0.7,0.52-0.10,0.90)
    '''
                
    if saveName == "pixel_occ_vs_z_barrel" or saveName == "pixel_hits_vs_z_barrel" or saveName == "pixel_clusters_vs_z_barrel" or saveName == "pix_clus_size_vs_z_barrel":

        leg = TLegend(0.35-0.05,0.72,0.55-0.05,0.92)


        
    else:
        leg = TLegend(0.35,0.72,0.55,0.92)

    # below is for the inclined layout

    '''

    if saveName == "pix_clus_size_vs_z_barrel":
        leg = TLegend(0.70,0.6,0.90,0.85)
    else:
        leg = TLegend(0.35,0.72,0.55,0.92)

    '''

        
    #leg = TLegend(0.3,0.65,0.45,0.85)
    leg.AddEntry(g_0, "Layer 0", "p")
    leg.AddEntry(g_1, "Layer 1", "p")
    leg.AddEntry(g_2, "Layer 2", "p")
    leg.AddEntry(g_3, "Layer 3", "p")
    leg.AddEntry(g_4, "Layer 4", "p")
    leg.SetFillColor(0)
    leg.SetTextSize(0.04)
    leg.SetBorderSize(0)
    leg.Draw()


    ####MultiG.GetXaxis().SetRangeUser(-0.01, 1400)

    # for the extended layout, we put it here!

    itk_plot_label = "ITk "+plot_label

    if saveName == "pix_clus_size_vs_z_barrel":

        ATLAS_LABEL(0.55-0.05,0.85); myText(0.67-0.05,0.85,1,"Simulation Prelim.");
        myText(0.55-0.05,0.80,1,itk_plot_label);
        myText(0.55-0.05,0.75,1,"ttbar, <#mu> = 200");


        ####MultiG.GetYaxis().SetRangeUser(0,yaxismax)

        
    if saveName == "pixel_occ_vs_z_barrel" or saveName == "pixel_hits_vs_z_barrel" or saveName == "pixel_clusters_vs_z_barrel" or saveName == "pix_clus_size_vs_z_barrel":

        ATLAS_LABEL(0.55-0.05,0.85); myText(0.67-0.05,0.85,1,"Simulation Preliminary");
        myText(0.55-0.05,0.80,1,itk_plot_label);
        myText(0.55-0.05,0.75,1,"ttbar, <#mu> = 200");



        

    else:
        ATLAS_LABEL(0.55,0.86); myText(0.67,0.86,1,"Simulation Preliminary");
        myText(0.55,0.81,1,itk_plot_label);
        myText(0.55,0.76,1,"ttbar, <#mu> = 200");





    # for the inclined layout, we put it here!

    '''
    if saveName == "pix_clus_size_vs_z_barrel":
        ATLAS_LABEL(0.31,0.80); myText(0.43,0.80,1,"Simulation Preliminary");                                                                                                                                                                    
        myText(0.31,0.75,1,"ITk Inclined");                                                                                                                                                                                                         
        myText(0.31,0.70,1,"ttbar, <#mu> = 200");
        ####MultiG.GetYaxis().SetRangeUser(0,yaxismax)

        hello

    '''




    c1.Update()

    saveStr_pdf = "plots/"+plot_label+"/%s"
    saveStr_eps = "plots/"+plot_label+"/%s"
    saveStr_png = "plots/"+plot_label+"/%s"

    

        
    c1.SaveAs(saveStr_pdf %(saveName+"_"+plot_label+".pdf"))
    c1.SaveAs(saveStr_eps %(saveName+"_"+plot_label+".eps"))
    c1.SaveAs(saveStr_png %(saveName+"_"+plot_label+".png"))




    
    #c1.SaveAs("plots/variables/%s.pdf" %(saveName))
    #c1.SaveAs("plots/variables/%s.eps" %(saveName))

    

    
def PlotEndCapOccupancy(graph_0, graph_1, graph_2, graph_3,yaxis, yaxismax, saveName, plot_label):

    g_0 = TGraph()
    g_1 = TGraph()
    g_2 = TGraph()
    g_3 = TGraph()
    
    signal.GetObject(graph_0, g_0)
    signal.GetObject(graph_1, g_1)
    signal.GetObject(graph_2, g_2)
    signal.GetObject(graph_3, g_3)

    g_0.SetMarkerColor(1)
    g_1.SetMarkerColor(2)
    g_2.SetMarkerColor(3)
    g_3.SetMarkerColor(4)

    g_0.SetMarkerStyle(20)
    g_1.SetMarkerStyle(21)
    g_2.SetMarkerStyle(22)
    g_3.SetMarkerStyle(23)

    MultiG = TMultiGraph()

    MultiG.Add(g_0)
    MultiG.Add(g_1)
    MultiG.Add(g_2)
    MultiG.Add(g_3)

    #MultiG.SetMaximum(yaxismax);

    
    
    if saveName == "pix_clus_size_vs_z_endcap":
        MultiG.SetMaximum(yaxismax);
    
    MultiG.Draw("AP")

    MultiG.GetXaxis().SetTitle("z [mm]")
    MultiG.GetYaxis().SetTitle(yaxis)
        
    #####MultiG.GetYaxis().SetRangeUser(0,yaxismax)
    ####MultiG.GetXaxis().SetRangeUser(0,3100.0)


    
    if saveName == "pix_clus_size_vs_z_endcap":
        leg = TLegend(0.70,0.70-0.05,0.90,0.90-0.05)
    else:
        leg = TLegend(0.65,0.6,0.85,0.80)

        
    #leg = TLegend(0.35,0.72,0.55,0.92)
    #leg = TLegend(0.3,0.65,0.45,0.85)
    #leg = TLegend(0.65,0.6,0.85,0.80)
    leg.AddEntry(g_0, "Ring Layer 0", "p")
    leg.AddEntry(g_1, "Ring Layer 1", "p")
    leg.AddEntry(g_2, "Ring Layer 2", "p")
    leg.AddEntry(g_3, "Ring Layer 3", "p")
    leg.SetFillColor(0)
    leg.SetTextSize(0.04)
    leg.SetBorderSize(0)
    leg.Draw()


    ####MultiG.GetXaxis().SetRangeUser(-0.01, 3100)

    # nominal!

    '''

    ATLAS_LABEL(0.25-0.05,0.65); myText(0.37-0.05,0.65,1,"Simulation Preliminary");
    myText(0.25-0.05,0.60,1,plot_label);
    myText(0.25-0.05,0.55,1,"ttbar, <#mu> = 200");

    '''


    itk_plot_label = "ITk "+plot_label


    
    if saveName == "pix_clus_size_vs_z_endcap":
        ATLAS_LABEL(0.31,0.80+0.07); myText(0.43,0.80+0.07,1,"Simulation Preliminary");                                  
        myText(0.31,0.75+0.07,1, itk_plot_label);                                                                                                                             myText(0.31,0.70+0.07,1,"ttbar, <#mu> = 200");
    
    if saveName == "pixel_clusters_vs_z_endcap":
    
        ATLAS_LABEL(0.25-0.05,0.65); myText(0.37-0.05,0.65,1,"Simulation Preliminary");
        myText(0.25-0.05,0.60,1,itk_plot_label);
        myText(0.25-0.05,0.55,1,"ttbar, <#mu> = 200");

    if saveName != "pix_clus_size_vs_z_endcap" and saveName != "pix_clus_size_vs_z_endcap":
        ATLAS_LABEL(0.25-0.05,0.65); myText(0.37-0.05,0.65,1,"Simulation Preliminary");
        myText(0.25-0.05,0.60,1,itk_plot_label);
        myText(0.25-0.05,0.55,1,"ttbar, <#mu> = 200");




    c1.Update()

    saveStr_pdf = "plots/"+plot_label+"/%s"
    saveStr_eps = "plots/"+plot_label+"/%s"
    saveStr_png = "plots/"+plot_label+"/%s"

    

        
    c1.SaveAs(saveStr_pdf %(saveName+"_"+plot_label+".pdf"))
    c1.SaveAs(saveStr_eps %(saveName+"_"+plot_label+".eps"))
    c1.SaveAs(saveStr_png %(saveName+"_"+plot_label+".png"))





main()

#ATLAS_LABEL(0.2,0.8)
#first argument is where the text starts on the x-axis.
#second argument is where the test starts on the y-axis
#myText(0.2,0.75,1,"Internal")


#c1.SetLogy()
#c1.SaveAs("tester.png")


