import sys


from ROOT import *
ROOT.gROOT.LoadMacro("AtlasStyle.C")
ROOT.gROOT.LoadMacro("AtlasLabels.C")
ROOT.gROOT.LoadMacro("AtlasUtils.C")

SetAtlasStyle()

ROOT.gStyle.SetOptStat(False)
ROOT.gStyle.SetMarkerSize(1.5)


from array import array
import numpy as np
import scipy
import sys

ROOT.gROOT.SetBatch(kTRUE)

# set log scale for y axis...
c1 = TCanvas()
#c1.SetLogy()

Inclined = TFile("/afs/cern.ch/work/a/altaylor/itk_occupancy_svn2/ITkOccupancy/ITkOccupancy/ITkOccupancy/run/100events_inclined.root")
Extended = TFile("/afs/cern.ch/work/a/altaylor/itk_occupancy_svn2/ITkOccupancy/ITkOccupancy/ITkOccupancy/run/100events_extended_AUW.root")


def main():

    print ' hello world! '

    # plot the inclusive region occupancy
    
    incl_reg_occ_inclusive = PlotPixel(0, "h_region_occ_0", "h_region_occ_1", "h_region_occ_2", "h_region_occ_3", "h_region_occ_4", "Region Occupancy", "Normalised Entries", "inc_reg_occ_per_inclusive_layer")
    extended_hits_per_chip = PlotPixel(1, "h_region_occ_0", "h_region_occ_1", "h_region_occ_2", "h_region_occ_3", "h_region_occ_4", "Region Occupancy", "Normalised Entries", "ext_reg_occ_per_inclusive_layer")

    # get the region occupancy for the flat section of pixel barrel

    incl_reg_occ_flat = PlotPixel(0, "h_region_occ_0_f", "h_region_occ_1_f", "h_region_occ_2_f", "h_region_occ_3_f", "h_region_occ_4_f", "Region Occupancy", "Normalised Entries", "inc_reg_occ_per_flat_layer")
    extended_hits_per_chip = PlotPixel(1, "h_region_occ_0_f", "h_region_occ_1_f", "h_region_occ_2_f", "h_region_occ_3_f", "h_region_occ_4_f", "Region Occupancy", "Normalised Entries", "ext_reg_occ_flat_per_layer")

    # get the region occupancy for the inclined section of pixel barrel
    incl_reg_occ_inclined = PlotPixel(0, "h_region_occ_0_I", "h_region_occ_1_I", "h_region_occ_2_I", "h_region_occ_3_I", "h_region_occ_4_I", "Region Occupancy", "Normalised Entries", "inc_reg_occ_per_inclined_layer")
    extended_hits_per_chip = PlotPixel(1, "h_region_occ_0_I", "h_region_occ_1_I", "h_region_occ_2_I", "h_region_occ_3_I", "h_region_occ_4_I", "Region Occupancy", "Normalised Entries",  "ext_reg_occ_per_inclined_layer")


    # quartiles of the number of bits per chip 

    FindQuartile(0,"h_bitsPerChip_0", 0, 0.95,0.99)
    FindQuartile(1,"h_bitsPerChip_0", 0, 0.95,0.99)
    
    FindQuartile(0,"h_bitsPerChip_1", 1, 0.95,0.99)
    FindQuartile(1,"h_bitsPerChip_1", 1, 0.95,0.99)
    
    FindQuartile(0,"h_bitsPerChip_2", 2, 0.95,0.99)
    FindQuartile(1,"h_bitsPerChip_2", 2, 0.95,0.99)
    
    FindQuartile(0,"h_bitsPerChip_3", 3, 0.95,0.99)
    FindQuartile(1,"h_bitsPerChip_3", 3, 0.95,0.99)
    
    FindQuartile(0,"h_bitsPerChip_4", 4, 0.95,0.99)
    FindQuartile(1,"h_bitsPerChip_4", 4, 0.95,0.99)


    
    incl_bitsPerChip = PlotPixel(0, "h_bitsPerChip_0", "h_bitsPerChip_1", "h_bitsPerChip_2", "h_bitsPerChip_3", "h_bitsPerChip_4", "Bits per chip", "Normalised Entries", "inc_bits_per_chip")
    ext_bitsPerChip = PlotPixel(1, "h_bitsPerChip_0", "h_bitsPerChip_1", "h_bitsPerChip_2", "h_bitsPerChip_3", "h_bitsPerChip_4", "Bits per chip", "Normalised Entries", "ext_bits_per_chip")

    PlotGraphs(incl_bitsPerChip, ext_bitsPerChip, 2500.0, "Mean number of bits per chip", "mean_bits_per_chip")

    # do cumulative histogram...

    cum_incl_bits_per_chip = PlotPixelCumulativeV2(0,"h_bitsPerChip_0", "h_bitsPerChip_1", "h_bitsPerChip_2", "h_bitsPerChip_3", "h_bitsPerChip_4", "Bits per chip", "Cumulative Integral", "cumu_bits_perchip_inclined")
    cum_extended_bits_per_chip = PlotPixelCumulativeV2(1,"h_bitsPerChip_0", "h_bitsPerChip_1", "h_bitsPerChip_2", "h_bitsPerChip_3", "h_bitsPerChip_4", "Bits per chip", "Cumulative Integral", "cumu_bits_perchip_extended")


    print 'Quartiles for the chips!' 

    print 'Hits per chip!' 
    
    FindQuartile(0,"h_hits_chip_0", 0, 0.95,0.99)
    FindQuartile(1,"h_hits_chip_0", 0, 0.95,0.99)

    
    FindQuartile(0,"h_hits_chip_1", 1, 0.95,0.99)
    FindQuartile(1,"h_hits_chip_1", 1, 0.95,0.99)
    
    FindQuartile(0,"h_hits_chip_2", 2, 0.95,0.99)
    FindQuartile(1,"h_hits_chip_2", 2, 0.95,0.99)
    
    FindQuartile(0,"h_hits_chip_3", 3, 0.95,0.99)
    FindQuartile(1,"h_hits_chip_3", 3, 0.95,0.99)
    
    FindQuartile(0,"h_hits_chip_4", 4, 0.95,0.99)
    FindQuartile(1,"h_hits_chip_4", 4, 0.95,0.99)



    print ' Clusters! per chip! '
    
    FindQuartile(0,"h_clus_chip_0", 0, 0.95,0.99)
    FindQuartile(1,"h_clus_chip_0", 0, 0.95,0.99)
    
    FindQuartile(0,"h_clus_chip_1", 1, 0.95,0.99)
    FindQuartile(1,"h_clus_chip_1", 1, 0.95,0.99)
    
    FindQuartile(0,"h_clus_chip_2", 2, 0.95,0.99)
    FindQuartile(1,"h_clus_chip_2", 2, 0.95,0.99)
    
    FindQuartile(0,"h_clus_chip_3", 3, 0.95,0.99)
    FindQuartile(1,"h_clus_chip_3", 3, 0.95,0.99)
    
    FindQuartile(0,"h_clus_chip_4", 4, 0.95,0.99)
    FindQuartile(1,"h_clus_chip_4", 4, 0.95,0.99)


    print 'Quartiles for the sensors! '

    print 'Hits per sensor! ' 
    
    FindQuartile(0,"h_hits_sensor_0", 0, 0.95,0.99)
    FindQuartile(1,"h_hits_sensor_0", 0, 0.95,0.99)
    
    FindQuartile(0,"h_hits_sensor_1", 1, 0.95,0.99)
    FindQuartile(1,"h_hits_sensor_1", 1, 0.95,0.99)

    FindQuartile(0,"h_hits_sensor_2", 2, 0.95,0.99)
    FindQuartile(1,"h_hits_sensor_2", 2, 0.95,0.99)

    FindQuartile(0,"h_hits_sensor_3", 3, 0.95,0.99)
    FindQuartile(1,"h_hits_sensor_3", 3, 0.95,0.99)
    
    FindQuartile(0,"h_hits_sensor_4", 4, 0.95,0.99)
    FindQuartile(1,"h_hits_sensor_4", 4, 0.95,0.99)

    print 'Clusters per sensor! ' 
    
    FindQuartile(0,"h_clus_sensor_0", 0, 0.95,0.99)
    FindQuartile(1,"h_clus_sensor_0", 0, 0.95,0.99)
    
    FindQuartile(0,"h_clus_sensor_1", 1, 0.95,0.99)
    FindQuartile(1,"h_clus_sensor_1", 1, 0.95,0.99)

    FindQuartile(0,"h_clus_sensor_2", 2, 0.95,0.99)
    FindQuartile(1,"h_clus_sensor_2", 2, 0.95,0.99)
    
    FindQuartile(0,"h_clus_sensor_3", 3, 0.95,0.99)
    FindQuartile(1,"h_clus_sensor_3", 3, 0.95,0.99)
    
    FindQuartile(0,"h_clus_sensor_4", 4, 0.95,0.99)
    FindQuartile(1,"h_clus_sensor_4", 4, 0.95,0.99)


    
    inclined_hits_per_chip = PlotPixel(0, "h_hits_chip_0", "h_hits_chip_1", "h_hits_chip_2", "h_hits_chip_3", "h_hits_chip_4", "Hits per chip", "Normalised Entries", "hitsperchip_inclined")
    extended_hits_per_chip = PlotPixel(1, "h_hits_chip_0", "h_hits_chip_1", "h_hits_chip_2", "h_hits_chip_3", "h_hits_chip_4", "Hits per chip", "Normalised Entries", "hitsperchip_extended")

    inclined_clusters_per_chip = PlotPixel(0, "h_clus_chip_0", "h_clus_chip_1", "h_clus_chip_2", "h_clus_chip_3", "h_clus_chip_4", "Clusters per chip", "Normalised Entries", "clustersperchip_inclined")
    extended_clusters_per_chip = PlotPixel(1, "h_clus_chip_0", "h_clus_chip_1", "h_clus_chip_2", "h_clus_chip_3", "h_clus_chip_4", "Clusters per chip", "Normalised Entries", "clustersperchip_extended")
    
    inclined_hits_per_sensor = PlotPixel(0, "h_hits_sensor_0", "h_hits_sensor_1", "h_hits_sensor_2", "h_hits_sensor_3", "h_hits_sensor_4", "Hits per sensor", "Normalised Entries", "hitspersensor_inclined")
    extended_hits_per_sensor = PlotPixel(1, "h_hits_sensor_0", "h_hits_sensor_1", "h_hits_sensor_2", "h_hits_sensor_3", "h_hits_sensor_4", "Hits per sensor", "Normalised Entries", "hitspersensor_extended")
    
    inclined_clusters_per_sensor = PlotPixel(0, "h_clus_sensor_0", "h_clus_sensor_1", "h_clus_sensor_2", "h_clus_sensor_3", "h_clus_sensor_4", "Clusters per sensor", "Normalised Entries", "clusterspersensor_inclined")
    extended_clusters_per_sensor = PlotPixel(1, "h_clus_sensor_0", "h_clus_sensor_1", "h_clus_sensor_2", "h_clus_sensor_3", "h_clus_sensor_4", "Clusters per sensor", "Normalised Entries", "clusterspersensor_extended")

    
    cum_inclined_hits_per_chip = PlotPixelCumulativeV2(0,"h_hits_chip_0", "h_hits_chip_1", "h_hits_chip_2", "h_hits_chip_3", "h_hits_chip_4", "Hits per chip", "Cumulative Integral", "cumu_hits_perchip_inclined")
    cum_extended_hits_per_chip = PlotPixelCumulativeV2(1,"h_hits_chip_0", "h_hits_chip_1", "h_hits_chip_2", "h_hits_chip_3", "h_hits_chip_4", "Hits per chip", "Cumulative Integral", "cumu_hits_perchip_extended")

    cum_inclined_clus_per_chip = PlotPixelCumulativeV2(0,"h_clus_chip_0", "h_clus_chip_1", "h_clus_chip_2", "h_clus_chip_3", "h_clus_chip_4", "Clusters per chip", "Cumulative Integral", "clus_perchip_inclined")
    cum_extended_clus_per_chip = PlotPixelCumulativeV2(1,"h_clus_chip_0", "h_clus_chip_1", "h_clus_chip_2", "h_clus_chip_3", "h_clus_chip_4", "Clusters per chip", "Cumulative Integral", "clus_perchip_extended")

    cum_inclined_hits_per_sensor = PlotPixelCumulativeV2(0,"h_hits_sensor_0", "h_hits_sensor_1", "h_hits_sensor_2", "h_hits_sensor_3", "h_hits_sensor_4", "Hits per sensor", "Cumulative Integral", "cumu_hits_per_sensor_inclined")
    cum_extended_hits_per_sensor = PlotPixelCumulativeV2(1,"h_hits_sensor_0", "h_hits_sensor_1", "h_hits_sensor_2", "h_hits_sensor_3", "h_hits_sensor_4", "Hits per sensor", "Cumulative Integral", "cumu_hits_per_sensor_extended")

    cum_inclined_clus_per_sensor = PlotPixelCumulativeV2(0,"h_clus_sensor_0", "h_clus_sensor_1", "h_clus_sensor_2", "h_clus_sensor_3", "h_clus_sensor_4", "Clusters per sensor", "Cumulative Integral", "clus_per_sensor_inclined")
    cum_extended_clus_per_sensor = PlotPixelCumulativeV2(1,"h_clus_sensor_0", "h_clus_sensor_1", "h_clus_sensor_2", "h_clus_sensor_3", "h_clus_sensor_4", "Clusters per sensor", "Cumulative Integral", "clus_per_sensor_extended")
    


    PlotGraphs(inclined_hits_per_chip, extended_hits_per_chip, 250.0, "Mean number of hits per chip", "mean_hits_per_chip")
    PlotGraphs(inclined_clusters_per_chip, extended_clusters_per_chip, 40.0, "Mean number of clusters per chip", "mean_clusters_per_chip")
    PlotGraphs(inclined_hits_per_sensor, extended_hits_per_sensor, 350.0, "Mean number of hits per sensor", "mean_hits_per_sensor")
    PlotGraphs(inclined_clusters_per_sensor, extended_clusters_per_sensor, 45.0, "Mean number of clusters per sensor", "mean_clusters_per_sensor")




def FindQuartile(inc_or_ext, hist,layer,nQuartileA, nQuartileB):

    h_0 = TH1D()
    #Inclined.GetObject(hist,h_0)

    # 0 -- inclined, 1 -- extended. 

    if inc_or_ext == 0:
        print 'Inclined layout! layer...  ' , layer
        Inclined.GetObject(hist,h_0)

    if inc_or_ext == 1:
        print 'Extended layout! layer.... ' , layer 
        Extended.GetObject(hist,h_0)

    nEntries = h_0.GetEntries()

    percentileA = nQuartileA*h_0.GetEntries()
    percentileB = nQuartileB*h_0.GetEntries()
    
    # do for loop over the bins
    bins = range(h_0.GetNbinsX() + 1)
    testsum_A = 0.0

    for bin in bins:
        testsum_A += h_0.GetBinContent(bin)

        if testsum_A >= percentileA:
            print 'nQuartile, Bin' , nQuartileA, "th", bin
            break

    testsum_B = 0.0

    for bin in bins:
        testsum_B += h_0.GetBinContent(bin)

        if testsum_B >= percentileB:
            print 'nQuartile, Bin' , nQuartileB,"th", bin
            break


def PlotPixelCumulativeV2(inc_or_ext, h_A, h_B, h_C, h_D, h_E, axisname, yaxis, savename):
    
    # inc_or_ext is a 0 or 1 to decide which file to use for grabbing the histograms.

    '''
    if inc_or_ext == 0:
        
        ATLAS_LABEL(0.40,0.85); myText(0.52,0.85,1,"Simulation Internal");
        myText(0.40,0.80,1,"ITk Inclined");
        myText(0.40,0.75,1,"t#bar{t}, <#mu> = 200");
        
    if inc_or_ext == 1:

        ATLAS_LABEL(0.40,0.85); myText(0.52,0.85,1,"Simulation Internal");
        myText(0.40,0.80,1,"ITk Extended");
        myText(0.40,0.75,1,"t#bar{t}, <#mu> = 200");

    '''

       
    
    h_0 = TH1D()
    h_1 = TH1D()
    h_2 = TH1D()
    h_3 = TH1D()
    h_4 = TH1D()

    if inc_or_ext == 0:
        Inclined.GetObject(h_A, h_0)
        Inclined.GetObject(h_B, h_1)
        Inclined.GetObject(h_C, h_2)
        Inclined.GetObject(h_D, h_3)
        Inclined.GetObject(h_E, h_4)

        
    if inc_or_ext == 1:
        Extended.GetObject(h_A, h_0)
        Extended.GetObject(h_B, h_1)
        Extended.GetObject(h_C, h_2)
        Extended.GetObject(h_D, h_3)
        Extended.GetObject(h_E, h_4)


    h_0.SetLineColor(1)
    h_1.SetLineColor(2)
    h_2.SetLineColor(3)
    h_3.SetLineColor(4)
    h_4.SetLineColor(6)

    h_0.SetLineWidth(4)
    h_1.SetLineWidth(4)
    h_2.SetLineWidth(4)
    h_3.SetLineWidth(4)
    h_4.SetLineWidth(4)
        
    h_0.Scale(1.0/h_0.Integral())
    h_1.Scale(1.0/h_1.Integral())
    h_2.Scale(1.0/h_2.Integral())
    h_3.Scale(1.0/h_3.Integral())
    h_4.Scale(1.0/h_4.Integral())

    cumu0 = h_0.GetCumulative()
    cumu1 = h_1.GetCumulative()
    cumu2 = h_2.GetCumulative()
    cumu3 = h_3.GetCumulative()
    cumu4 = h_4.GetCumulative()
    
    cumu0.Draw()
    cumu1.Draw("same")
    cumu2.Draw("same")
    cumu3.Draw("same")
    cumu4.Draw("same")

    

    if inc_or_ext == 0:
        
        ATLAS_LABEL(0.40,0.85-0.4); myText(0.52,0.85-0.4,1,"Simulation Internal");
        myText(0.40,0.80-0.4,1,"ITk Inclined");
        myText(0.40,0.75-0.4,1,"t#bar{t}, <#mu> = 200");
        
    if inc_or_ext == 1:

        ATLAS_LABEL(0.40,0.85-0.4); myText(0.52,0.85-0.4,1,"Simulation Internal");
        myText(0.40,0.80-0.4,1,"ITk Extended");
        myText(0.40,0.75-0.4,1,"t#bar{t}, <#mu> = 200");
    
    cumu0.SetTitle(axisname)
    cumu0.GetXaxis().SetTitleOffset(0.9)
    cumu0.GetXaxis().SetTitle("%s" %axisname)
    cumu0.GetYaxis().SetTitle(yaxis)
    
    leg = TLegend(0.68,0.6,0.83,0.80)
    leg.SetLineColor(kWhite)
    leg.SetFillColor(kWhite)
    leg.SetTextSize(0.05)
    leg.SetBorderSize(0)

    max_height = 0
    
    for hist in [cumu0, cumu1, cumu2, cumu3, cumu4]:
        if hist.GetMaximum() > max_height:
            max_height = hist.GetMaximum()

    cumu0.GetYaxis().SetRangeUser(0.0,1.05*max_height)

    leg.AddEntry(cumu0,"Layer 0","l")
    leg.AddEntry(cumu1,"Layer 1","l")
    leg.AddEntry(cumu2,"Layer 2","l")
    leg.AddEntry(cumu3,"Layer 3","l")
    leg.AddEntry(cumu4,"Layer 4","l")

    leg.Draw()

     
    pdf_string = savename + ".pdf"
    eps_string = savename + ".eps"

    #c1.SaveAs(pdf_string)
    #c1.SaveAs(eps_string)

    c1.SaveAs("plots/pixels/barrel/%s.pdf" %(savename))
    c1.SaveAs("plots/pixels/barrel/%s.eps" %(savename))
    c1.SaveAs("plots/pixels/barrel/%s.png" %(savename))

    



def PlotGraphs(graph_A, graph_B, y_axis_max, y_axis_title, savename):
    
    graph_A.SetMarkerColor(1)
    graph_A.SetLineColor(1)
       
    graph_B.SetMarkerColor(2)
    graph_B.SetLineColor(2)
    graph_B.SetMarkerStyle(22)
    graph_B.SetLineStyle(10)
    
    hits_per_chip = TMultiGraph()
    leg = TLegend(0.70,0.7,0.80,0.80)

    hits_per_chip.Add(graph_A)
    hits_per_chip.Add(graph_B)

    hits_per_chip.Draw("apl")
            
    leg.AddEntry(graph_A, "Inclined", "lp")
    leg.AddEntry(graph_B, "Extended", "lp")

    leg.SetFillColor(0)
    leg.SetTextSize(0.04)
    leg.SetBorderSize(0)

    leg.Draw()

    hits_per_chip.GetYaxis().SetRangeUser(0.0,y_axis_max)
    hits_per_chip.GetYaxis().SetTitle(y_axis_title)
    hits_per_chip.GetXaxis().SetTitle("Pixel Layer")
    

    ATLAS_LABEL(0.40,0.85); myText(0.52,0.85,1,"Simulation Internal");
    myText(0.40,0.80,1,"ITk, Pixel Barrel");
    myText(0.40,0.75,1,"t#bar{t}, <#mu> = 200");

    # save plots as eps and .pdf
    pdf_string = savename + ".pdf"
    eps_string = savename + ".eps"

    #c1.SaveAs(pdf_string)
    #c1.SaveAs(eps_string)


    

    c1.SaveAs("plots/pixels/barrel/%s.pdf" %(savename))
    c1.SaveAs("plots/pixels/barrel/%s.eps" %(savename))
    c1.SaveAs("plots/pixels/barrel/%s.png" %(savename))

    



def PlotPixel(inc_or_ext, h_A, h_B, h_C, h_D, h_E, axisname, yaxis, savename):

    # inc_or_ext is a 0 or 1 to decide which file to use for grabbing the histograms.
    h_0 = TH1D()
    h_1 = TH1D()
    h_2 = TH1D()
    h_3 = TH1D()
    h_4 = TH1D()

    if inc_or_ext == 0:
        Inclined.GetObject(h_A, h_0)
        Inclined.GetObject(h_B, h_1)
        Inclined.GetObject(h_C, h_2)
        Inclined.GetObject(h_D, h_3)
        Inclined.GetObject(h_E, h_4)

        
    if inc_or_ext == 1:
        Extended.GetObject(h_A, h_0)
        Extended.GetObject(h_B, h_1)
        Extended.GetObject(h_C, h_2)
        Extended.GetObject(h_D, h_3)
        Extended.GetObject(h_E, h_4)

        
    h_0.Scale(1.0/h_0.Integral())
    h_1.Scale(1.0/h_1.Integral())
    h_2.Scale(1.0/h_2.Integral())
    h_3.Scale(1.0/h_3.Integral())
    h_4.Scale(1.0/h_4.Integral())

    # change some of the x-axes
    #"h_clus_chip_0"
    

    if h_A == "h_clus_chip_0":
        h_0.GetXaxis().SetRangeUser(0,80.0)
        
    if h_A == "h_clus_sensor_0":
        h_0.GetXaxis().SetRangeUser(0,120.0)

    if h_A == "h_hits_chip_0":
        h_0.GetXaxis().SetRangeUser(0,800.0)

    #incl_bitsPerChip = PlotPixel(0, "h_bitsPerChip_0"

    if h_A == "h_bitsPerChip_0":
        #h_0.GetXaxis().SetRangeUser(0,8000.0)
        # rebin all the histograms with a factor of 2.
        h_0.Rebin(100)
        h_1.Rebin(100)
        h_2.Rebin(100)
        h_3.Rebin(100)
        h_4.Rebin(100)
        h_0.GetXaxis().SetRangeUser(0,8000.0)
        

        

    h_0.SetLineColor(1)
    h_1.SetLineColor(2)
    h_2.SetLineColor(3)
    h_3.SetLineColor(4)
    h_4.SetLineColor(6)

    h_0.SetLineWidth(4)
    h_1.SetLineWidth(4)
    h_2.SetLineWidth(4)
    h_3.SetLineWidth(4)
    h_4.SetLineWidth(4)

    h_0.Draw()
    h_1.Draw("same")
    h_2.Draw("same")
    h_3.Draw("same")
    h_4.Draw("same")
    
    if inc_or_ext == 0:
        
        ATLAS_LABEL(0.40,0.85); myText(0.52,0.85,1,"Simulation Internal");
        myText(0.40,0.80,1,"ITk Inclined");
        myText(0.40,0.75,1,"t#bar{t}, <#mu> = 200");
        
    if inc_or_ext == 1:

        ATLAS_LABEL(0.40,0.85); myText(0.52,0.85,1,"Simulation Internal");
        myText(0.40,0.80,1,"ITk Extended");
        myText(0.40,0.75,1,"t#bar{t}, <#mu> = 200");

        
    
    h_0.SetTitle(axisname)
    h_0.GetXaxis().SetTitleOffset(0.9)
    h_0.GetXaxis().SetTitle("%s" %axisname)
    h_0.GetYaxis().SetTitle(yaxis)
    
    leg = TLegend(0.68,0.6-0.12,0.83,0.80-0.12)
    leg.SetLineColor(kWhite)
    leg.SetFillColor(kWhite)
    leg.SetTextSize(0.05)
    leg.SetBorderSize(0)

    max_height = 0
    
    for hist in [h_0, h_1, h_2, h_3, h_4]:
        if hist.GetMaximum() > max_height:
            max_height = hist.GetMaximum()

    h_0.GetYaxis().SetRangeUser(0.00001,1.05*max_height)

    leg.AddEntry(h_0,"Layer 0","l")
    leg.AddEntry(h_1,"Layer 1","l")
    leg.AddEntry(h_2,"Layer 2","l")
    leg.AddEntry(h_3,"Layer 3","l")
    leg.AddEntry(h_4,"Layer 4","l")

    leg.Draw()

    # save plots as eps and .pdf
    pdf_string = savename + ".pdf"
    eps_string = savename + ".eps"

    #c1.SaveAs(pdf_string)
    #c1.SaveAs(eps_string)

    c1.SaveAs("plots/pixels/barrel/%s.pdf" %(savename))
    c1.SaveAs("plots/pixels/barrel/%s.eps" %(savename))
    c1.SaveAs("plots/pixels/barrel/%s.png" %(savename))

    

    # get the mean value

    print ' h_0? mean ' , h_0.GetMean()
    print ' h_1? mean ' , h_1.GetMean()
    print ' h_2? mean' , h_2.GetMean()
    print ' h_3? mean' , h_3.GetMean()
    print ' h_4? mean' , h_4.GetMean()



    pixel_tgraph = TGraph()

    if inc_or_ext == 0:
            
        pixel_tgraph.SetPoint(0,0,h_0.GetMean())
        pixel_tgraph.SetPoint(1,1,h_1.GetMean())
        pixel_tgraph.SetPoint(2,2,h_2.GetMean())
        pixel_tgraph.SetPoint(3,3,h_3.GetMean())
        pixel_tgraph.SetPoint(4,4,h_4.GetMean())

        
    if inc_or_ext == 1:
            
        pixel_tgraph.SetPoint(0,0,h_0.GetMean())
        pixel_tgraph.SetPoint(1,1,h_1.GetMean())
        pixel_tgraph.SetPoint(2,2,h_2.GetMean())
        pixel_tgraph.SetPoint(3,3,h_3.GetMean())
        pixel_tgraph.SetPoint(4,4,h_4.GetMean())


    return pixel_tgraph



    
def PlotPixelV2(h_A, h_B, h_C, h_D, axisname, yaxis, savename):
    
    h_0 = TH1D()
    h_1 = TH1D()
    h_2 = TH1D()
    h_3 = TH1D()
    
    perEventPixel.GetObject(h_A, h_0)
    perEventPixel.GetObject(h_B, h_1)
    perEventPixel.GetObject(h_C, h_2)
    perEventPixel.GetObject(h_D, h_3)

    h_0.SetLineColor(1)
    h_1.SetLineColor(2)
    h_2.SetLineColor(3)
    h_3.SetLineColor(4)

    h_0.SetLineWidth(4)
    h_1.SetLineWidth(4)
    h_2.SetLineWidth(4)
    h_3.SetLineWidth(4)

    
    h_0.Scale(1.0/h_0.Integral())
    h_1.Scale(1.0/h_1.Integral())
    h_2.Scale(1.0/h_2.Integral())
    h_3.Scale(1.0/h_3.Integral())

    h_0.Draw()
    h_1.Draw("same")
    h_2.Draw("same")
    h_3.Draw("same")
    
    h_0.SetTitle(axisname)
    h_0.GetXaxis().SetTitleOffset(0.9)
    h_0.GetXaxis().SetTitle("%s" %axisname)
    h_0.GetYaxis().SetTitle(yaxis)
    
    leg = TLegend(0.68-0.5,0.6+0.12,0.83-0.5,0.80+0.12)
    leg.SetLineColor(kWhite)
    leg.SetFillColor(kWhite)
    leg.SetTextSize(0.05)
    leg.SetBorderSize(0)

    max_height = 0
    
    for hist in [h_0, h_1, h_2, h_3,h_4]:
        if hist.GetMaximum() > max_height:
            max_height = hist.GetMaximum()

    h_0.GetYaxis().SetRangeUser(0.0001,2.0*max_height)

    leg.AddEntry(h_0,"Layer 0","l")
    leg.AddEntry(h_1,"Layer 0 (Inclined)","l")
    leg.AddEntry(h_2,"Layers >0 ","l")
    leg.AddEntry(h_3,"Layers >0 (Inclined)","l")

    leg.Draw()

    # save plots as eps and .pdf
    pdf_string = savename + ".pdf"
    eps_string = savename + ".eps"

    #c1.SaveAs(pdf_string)
    #c1.SaveAs(eps_string)

    c1.SaveAs("plots/pixels/barrel/%s.pdf" %(savename))
    c1.SaveAs("plots/pixels/barrel/%s.eps" %(savename))
    c1.SaveAs("plots/pixels/barrel/%s.png" %(savename))



    
#first argument decides whether to use inclined or extended layout.
# 0 index -- inclined. 1 index -- extended. 
    
def Plotter(layoutIndex, var0, var1,var2,var3,var4, nbins, lbin, ubin,axisname,savename,cut,yaxis):

    h_0 = TH1F("h_0", "", nbins, lbin, ubin)
    h_1 = TH1F("h_1", "", nbins, lbin, ubin)
    h_2 = TH1F("h_2", "", nbins, lbin, ubin)
    h_3 = TH1F("h_3", "", nbins, lbin, ubin)
    h_4 = TH1F("h_4", "", nbins, lbin, ubin)

    if layoutIndex == 0:

        inclinedTree.Draw("%s >> h_0" %var0, "%s" %cut)
        inclinedTree.Draw("%s >> h_1" %var1, "%s" %cut)
        inclinedTree.Draw("%s >> h_2" %var2, "%s" %cut)
        inclinedTree.Draw("%s >> h_3" %var3, "%s" %cut)
        inclinedTree.Draw("%s >> h_4" %var4, "%s" %cut)

        
        ATLAS_LABEL(0.40,0.85); myText(0.52,0.85,1,"Simulation Internal");
        myText(0.40,0.80,1,"ITk Inclined");
        myText(0.40,0.75,1,"t#bar{t}, <#mu> = 200");
        
    if layoutIndex == 1:

        extendedTree.Draw("%s >> h_0" %var0, "%s" %cut)
        extendedTree.Draw("%s >> h_1" %var1, "%s" %cut)
        extendedTree.Draw("%s >> h_2" %var2, "%s" %cut)
        extendedTree.Draw("%s >> h_3" %var3, "%s" %cut)
        extendedTree.Draw("%s >> h_4" %var4, "%s" %cut)
        

        ATLAS_LABEL(0.40,0.85); myText(0.52,0.85,1,"Simulation Internal");
        myText(0.40,0.80,1,"ITk Extended");
        myText(0.40,0.75,1,"t#bar{t}, <#mu> = 200");

        


            
    h_0.Scale(1.0/h_0.Integral())
    h_1.Scale(1.0/h_1.Integral())
    h_2.Scale(1.0/h_2.Integral())
    h_3.Scale(1.0/h_3.Integral())
    h_4.Scale(1.0/h_4.Integral())
    
    h_0.SetLineColor(1)
    h_1.SetLineColor(2)
    h_2.SetLineColor(3)
    h_3.SetLineColor(4)
    h_4.SetLineColor(6)

    h_0.SetLineWidth(4)
    h_1.SetLineWidth(4)
    h_2.SetLineWidth(4)
    h_3.SetLineWidth(4)
    h_4.SetLineWidth(4)

    h_0.Draw()
    h_1.Draw("same")
    h_2.Draw("same")
    h_3.Draw("same")
    h_4.Draw("same")

    if layoutIndex == 0:

        ATLAS_LABEL(0.4+0.15,0.85); myText(0.52+0.15,0.85,1,"Simulation Internal");
        myText(0.40+0.15,0.80,1,"ITk Inclined");
        myText(0.40+0.15,0.75,1,"t#bar{t}, <#mu> = 200");
        
    if layoutIndex == 1:

        ATLAS_LABEL(0.40+0.15,0.85); myText(0.52+0.15,0.85,1,"Simulation Internal");
        myText(0.40+0.15,0.80,1,"ITk Extended");
        myText(0.40+0.15,0.75,1,"t#bar{t}, <#mu> = 200");

    
    
    h_0.SetTitle(axisname)
    h_0.GetXaxis().SetTitleOffset(0.9)
    h_0.GetXaxis().SetTitle("%s" %axisname)
    h_0.GetYaxis().SetTitle(yaxis)
    
    leg = TLegend(0.68,0.4,0.93,0.60)
    leg.SetLineColor(kWhite)
    leg.SetFillColor(kWhite)
    leg.SetTextSize(0.05)
    leg.SetBorderSize(0)




    
    max_height = 0

    
    for hist in [h_0, h_1, h_2, h_3, h_4]:
        if hist.GetMaximum() > max_height:
            max_height = hist.GetMaximum()

    h_0.GetYaxis().SetRangeUser(0,1.5*max_height)

    leg.AddEntry(h_0,"Layer 0","l")
    leg.AddEntry(h_1,"Layer 1","l")
    leg.AddEntry(h_2,"Layer 2","l")
    leg.AddEntry(h_3,"Layer 3","l")
    leg.AddEntry(h_4,"Layer 4","l")

    gPad.Print("label.png")
    leg.Draw()

    
    # save plots as eps and .pdf
    pdf_string = savename + ".pdf"
    eps_string = savename + ".eps"

    #c1.SaveAs(pdf_string)
    #c1.SaveAs(eps_string)

    c1.SaveAs("plots/pixels/barrel/%s.pdf" %(savename))
    c1.SaveAs("plots/pixels/barrel/%s.eps" %(savename))
    c1.SaveAs("plots/pixels/barrel/%s.png" %(savename))


       # get the mean value


    print ' h_0? mean ' , h_0.GetMean()
    print ' h_1? mean ' , h_1.GetMean()
    print ' h_2? mean' , h_2.GetMean()
    print ' h_3? mean' , h_3.GetMean()
    print ' h_4? mean' , h_4.GetMean()

    # pixel tgraph
    #pixel_tgraph = TGraph()

    #plot mean max vs layour

    mean_max = TGraph()

                  
    mean_max.SetPoint(0,0,h_0.GetMean())
    mean_max.SetPoint(1,1,h_1.GetMean())
    mean_max.SetPoint(2,2,h_2.GetMean())
    mean_max.SetPoint(3,3,h_3.GetMean())
    mean_max.SetPoint(4,4,h_4.GetMean())

    return mean_max


main()


