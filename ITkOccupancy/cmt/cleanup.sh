# echo "cleanup ITkOccupancy ITkOccupancy-00-00-00 in /afs/cern.ch/work/a/altaylor/git_test/ITkOccupancyAthxAOD"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc49-opt/2.4.27/CMT/v1r25p20160527; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtITkOccupancytempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtITkOccupancytempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=ITkOccupancy -version=ITkOccupancy-00-00-00 -path=/afs/cern.ch/work/a/altaylor/git_test/ITkOccupancyAthxAOD  $* >${cmtITkOccupancytempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=ITkOccupancy -version=ITkOccupancy-00-00-00 -path=/afs/cern.ch/work/a/altaylor/git_test/ITkOccupancyAthxAOD  $* >${cmtITkOccupancytempfile}"
  cmtcleanupstatus=2
  /bin/rm -f ${cmtITkOccupancytempfile}
  unset cmtITkOccupancytempfile
  return $cmtcleanupstatus
fi
cmtcleanupstatus=0
. ${cmtITkOccupancytempfile}
if test $? != 0 ; then
  cmtcleanupstatus=2
fi
/bin/rm -f ${cmtITkOccupancytempfile}
unset cmtITkOccupancytempfile
return $cmtcleanupstatus

