# echo "cleanup ITkOccupancy ITkOccupancy-00-00-00 in /afs/cern.ch/work/a/altaylor/git_test/ITkOccupancyAthxAOD"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc49-opt/2.4.27/CMT/v1r25p20160527
endif
source ${CMTROOT}/mgr/setup.csh
set cmtITkOccupancytempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmtITkOccupancytempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=ITkOccupancy -version=ITkOccupancy-00-00-00 -path=/afs/cern.ch/work/a/altaylor/git_test/ITkOccupancyAthxAOD  $* >${cmtITkOccupancytempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=ITkOccupancy -version=ITkOccupancy-00-00-00 -path=/afs/cern.ch/work/a/altaylor/git_test/ITkOccupancyAthxAOD  $* >${cmtITkOccupancytempfile}"
  set cmtcleanupstatus=2
  /bin/rm -f ${cmtITkOccupancytempfile}
  unset cmtITkOccupancytempfile
  exit $cmtcleanupstatus
endif
set cmtcleanupstatus=0
source ${cmtITkOccupancytempfile}
if ( $status != 0 ) then
  set cmtcleanupstatus=2
endif
/bin/rm -f ${cmtITkOccupancytempfile}
unset cmtITkOccupancytempfile
exit $cmtcleanupstatus

